package com.main.mailtester.models;

public class MailtesterGsonModel {
	private String displayedMark;
	private Blacklists blacklists;
	private SpamAssassin spamAssassin;
	private Signature signature;
	private Body body;
	private Links links;
	

	public Blacklists getBlacklists () {
		return blacklists;
	}

	public void setBlacklists (Blacklists blacklists) {
		this.blacklists = blacklists;
	}

	public SpamAssassin getSpamAssassin () {
		return spamAssassin;
	}

	public void setSpamAssassin (SpamAssassin spamAssassin) {
		this.spamAssassin = spamAssassin;
	}

	public Signature getSignature () {
		return signature;
	}

	public void setSignature (Signature signature) {
		this.signature = signature;
	}

	public Body getBody () {
		return body;
	}

	public void setBody (Body body) {
		this.body = body;
	}

	public Links getLinks () {
		return links;
	}

	public void setLinks (Links links) {
		this.links = links;
	}

	public String getDisplayedMark() {
		return displayedMark;
	}

	public void setDisplayedMark(String displayedMark) {
		this.displayedMark = displayedMark;
	}

}
