package com.main.model;

import java.util.List;

public class Category {

	String cateGoryName;

	public String getCateGoryName() {
		return cateGoryName;
	}

	public void setCateGoryName(String cateGoryName) {
		this.cateGoryName = cateGoryName;
	}

	List<whitelabels.model.FinalGroupObject> finalTemplateGroupList;

	public List<whitelabels.model.FinalGroupObject> getFinalTemplateGroupList() {
		return finalTemplateGroupList;
	}

	public void setFinalTemplateGroupList(List<whitelabels.model.FinalGroupObject> finalTemplateGroupList) {
		this.finalTemplateGroupList = finalTemplateGroupList;
	}

}
