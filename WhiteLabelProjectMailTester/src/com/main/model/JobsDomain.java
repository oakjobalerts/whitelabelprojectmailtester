package com.main.model;

import java.util.List;

public class JobsDomain {

	String name;
	String date;
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	List<JobSourceCountModel> jobSourceCountModelList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<JobSourceCountModel> getJobSourceCountModelList() {
		return jobSourceCountModelList;
	}

	public void setJobSourceCountModelList(
			List<JobSourceCountModel> jobSourceCountModelList) {
		this.jobSourceCountModelList = jobSourceCountModelList;
	}

}
