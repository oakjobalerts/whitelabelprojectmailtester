package com.main.model;

public class MailTesterDomainModel {

	public String domainName;
	public String emailClient;
	public String dashBoardFile;
	public String FromEmailAddress;
	public String SubjectsFileName;
	public String SendGridCategoryName;
	public String SendgridUsername , SendgridPassword , whiteLableName;
	public String PostalAddress;
	public String sparkpost_key;
	public String sparkpost_bounce_domain;

	public boolean equals (Object obj) {
		if (obj instanceof MailTesterDomainModel) {
			MailTesterDomainModel model = (MailTesterDomainModel) obj;
			return (model.domainName.equalsIgnoreCase(this.domainName));
		}
		else {
			return false;
		}
	}

	public int hashCode () {
		int hashcode = 0;
		hashcode = 1;
		hashcode += domainName.hashCode();
		return hashcode;
	}

}
