package com.main.model;

import java.util.List;

public class Providers {

	String providerName;
	List<Domains> domains;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public List<Domains> getDomains() {
		return domains;
	}

	public void setDomains(List<Domains> domains) {
		this.domains = domains;
	}

}
