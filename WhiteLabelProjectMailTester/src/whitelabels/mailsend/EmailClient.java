package whitelabels.mailsend;

public class EmailClient {

	SendGridClass sendGridClassObject = null;
	MailGunClass mailGunClassObject = null;
	SesClass sesClassObject = null;
	SparkPostClass sparkPostClassObject = null;

	public EmailClient() {

		sendGridClassObject = new SendGridClass();
		mailGunClassObject = new MailGunClass();
		sesClassObject = new SesClass();
		sparkPostClassObject = new SparkPostClass();

	}

	public MailSendInterface getEmailClientInterface(String emailClient) {

		if (emailClient.equalsIgnoreCase("sendgrid")) {
			if (sendGridClassObject == null) {
				sendGridClassObject = new SendGridClass();
			}
			return sendGridClassObject;
		} else if (emailClient.equalsIgnoreCase("mailgun")) {
			if (mailGunClassObject == null) {
				mailGunClassObject = new MailGunClass();
			}
			return mailGunClassObject;
		} else if (emailClient.equalsIgnoreCase("ses")) {
			if (sesClassObject == null) {
				sesClassObject = new SesClass();
			}
			return sesClassObject;
		} else if (emailClient.equalsIgnoreCase("sparkpost")) {

			if (sparkPostClassObject == null) {
				sparkPostClassObject = new SparkPostClass();
			}
			return sparkPostClassObject;
		}
		return mailGunClassObject;

	}
}
