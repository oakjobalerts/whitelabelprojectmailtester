package whitelabels.mailsend;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.zip.Deflater;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import whitelabels.mainpackage.MainWhiteLabelClass;
import whitelabels.mainpackage.SettingsClass;
import whitelabels.model.Jobs;
import whitelabels.model.ParameterModelClass;
import whitelabels.model.UsersData;

import com.amazonaws.util.json.JSONObject;

public class EmailTemplates {

	public String emailTemplate1(UsersData userDataObject, List<Jobs> jobsArray, String groupId,
			String templateId) {

		// String topResumeCriteria = "";
		String postal_Address = SettingsClass.child_postal_addresses
				.get(userDataObject.child_process_key);
		String image_postalAddress = "<img src='http://" + userDataObject.domainName
				+ "/postal_address_img/"
				+ MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() + ".png"
				+ "' alt='" + MainWhiteLabelClass.parameterModelClassObject.getDomainUrl()
				+ "' style = width: 357px;height: 57px; />";

		if (postal_Address == null) {
			System.out.println("getting null from hash map");
			postal_Address = MainWhiteLabelClass.parameterModelClassObject.getPostalAddress();
		}
		if (MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() != "") {
			postal_Address = image_postalAddress;
		}

		String htmlHead = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>"
				+ "<meta http-equiv='Content-type' content='text/html; charset=utf-8' />"
				+ "<title>"
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ "</title></head>";

		String unsubString = "http://" + userDataObject.domainName
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.id)// $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		String htmlBodyStartTag = ""
				// +
				// "<body style='padding:0; margin:0; background:#fff; line-height:18px;'> <p></p>"
				+ addOpenpixels(userDataObject, groupId, templateId)
				+ " <table width='614' border='0'cellspacing='0' cellpadding='10' bgcolor='#ffffff' style='margin:0 auto; font-family:Arial, Helvetica, sans-serif;'>"
				+ " <tr> <td align='center' colspan='2' valign='middle'>"
				+ "<div style='text-align:center'>If you wish to turn off this alert <a "
				+ "href='"
				+ unsubString
				+ "' target='_blank'> click here</a></div>"

				+ " </td> </tr><tr> <td align='center' colspan='2' valign='middle'>"
				+ "<a href='http://"
				+ userDataObject.domainName
				+ "' style='color:#383838; line-height:30px;'><img src='"
				+ MainWhiteLabelClass.parameterModelClassObject.getLogoUrl()
				+ "' alt='At "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ " we help people find a new job and employers hire the right candidates through our strict job search algorithm. We are both a job aggregator and a job search engine but we like to think of ourselves as a facilitator in the recruitment industry.' /></a></td></tr>"
				+ " <tr><td style='color:"
				+ MainWhiteLabelClass.parameterModelClassObject.getLearnMoreColorCode()
				+ "; font-size:26px;' colspan='2'>Daily Job Alert</td></tr>"
				+ "  <tr> <td style='padding-bottom:10px;'><a style='color:#383838; font-size:14px; text-decoration:none;' href='http://"
				+ userDataObject.domainName
				+ "/jobs.php?q="
				+ userDataObject.keyword
				+ "&l="
				+ userDataObject.locationString
				+ "&r=20' target='_blank'>"
				+ userDataObject.keyword// +ucwords($searchKeyword)
				+ " Jobs Near "
				+ userDataObject.locationString// ucwords($subjectLocation).
				+ "</a><br/><br/>"
				+ "<a style='background: none repeat scroll 0px 0px "
				+ MainWhiteLabelClass.parameterModelClassObject.getLearnMoreColorCode()
				+ "; border-radius: 3px; color: rgb(255, 255, 255); font-size: 14px; padding: 5px 8px; text-decoration: none;' href='http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id) // $row['EmailGroup']
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "' target='_blank'>Edit this alert</a>"
				+ "</td><td style='vertical-align: top;' align='right'>"
				+ "<a style='color:#0066cc; font-size:13px; font-weight:bold; text-decoration:none;white-space:nowrap;' target='_blank' href='http://"
				+ userDataObject.domainName
				+ "/jobs.php?q="
				+ userDataObject.keyword
				+ "&l="
				+ userDataObject.locationString
				+ "&r=20' >See all matching jobs >"
				+ " </a>  </td>  </tr> <tr> <td colspan='2'> <table cellspacing='0' cellpadding='0' width='100%' border='0'><tbody><tr>"
				+ "<td style='padding:0 10px 0 0'> <hr style='border:0;background-color:#ddd;min-height:1px'/> </td> </tr> </tbody></table> </td></tr>";

		// JOBS Message START
		String jobMessage = "";
		boolean onceNewjob = true;
		for (int i = 0; i < jobsArray.size(); i++) {

			// make redirection through url only for all queues
			String redirectionString = "";

			// passing usersData,Job,Job Position starting from 1, Subject line
			// (Group id)
			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setUserJobUrl(
					"http://"
							+ userDataObject.domainName
							+ "/"
							+ MainWhiteLabelClass.parameterModelClassObject
									.getRedirectionFileName() + ".php?q=" + redirectionString);
			String dateString = "";
			if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
				dateString += "<span style='color:#F30E0E;'><b>New</b></span> | ";
				if (onceNewjob) {
					SettingsClass.numberOfNewJobMails.getAndIncrement();
					onceNewjob = false;
				}
			}

			String cityStateString = "";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity() + "," + jobsArray.get(i).getState();
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity();
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getState();
			}

			String empolyerLoctionDateString = "<h5  style='color:#383838; font-size:14px; text-decoration:none; margin:0;'>";

			String employer = jobsArray.get(i).getEmployer();

			String empolyerString = "";

			if (employer != null && !employer.equalsIgnoreCase("")
					&& !employer.toLowerCase().contains("http"))
				empolyerString = employer + " | ";

			empolyerLoctionDateString += dateString + empolyerString + cityStateString + "</h5>";

			jobMessage = jobMessage

					+ "<tr><td colspan='2' style='border-bottom:1px solid #CCC;'>"
					+ "<a style='text-decoration:none;' href='"
					+ jobsArray.get(i).getUserJobUrl()
					+ "'>"

					+ "<div style='display:table;width:100%'>"
					+ "<div style='display:table-cell;padding-bottom:10px;'>"
					+ "<h5 style='color:"
					+ MainWhiteLabelClass.parameterModelClassObject.getJobTitleColorCode()
					+ "; font-size:18px; text-decoration:none; font-weight:bold; margin:0;'>"
					+ jobsArray.get(i).getTitle()
					+ "</h5>"
					+ empolyerLoctionDateString
					+ "</div>"
					+ "<div style='display:table-cell;vertical-align: middle;width:110px;padding:5px 8px;' align='right'> <span style='color:#fff; display:block; font-size:14px; background:"
					+ MainWhiteLabelClass.parameterModelClassObject.getLearnMoreColorCode()
					+ "; padding:5px 8px; border-radius:3px; width: 92px; text-align:center; height:20px;'>Learn More ></span> </div>"
					+ "</div>" + "</a>" + "</td>" + "</tr>";

		}

		// JOBS Message END

		String htmlBodyEndTag = "<tr><td bgcolor='#eeeeee'><ul style='font-size:14px; margin:0; padding:0 0 0 10px'> <li>"
				+ "<a href='http://"
				+ userDataObject.domainName
				+ "/create_alert.php?source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "&email="
				+ baseEncode64(userDataObject.email)
				+ "' target='_blank'>Create another alert"
				+ "</a></li><li>"
				+ "<a href='http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id) // /$row['EmailGroup']
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "' target='_blank'>Edit this alert"
				+ "</a></li><li>"
				+ "<a "
				+ " href='http://"
				+ userDataObject.domainName
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.id)// $row['EmailGroup']
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "&email="
				+ baseEncode64(userDataObject.email)
				+ "' target='_blank'>Turn Off This Alert"
				+ "</a></li> </ul> </td><td align='right'  bgcolor='#eeeeee'>"
				+ "<a style='font-size:13px; font-weight:bold; color:#0066cc; text-decoration:none; white-space:nowrap; ' href='http://"
				+ userDataObject.domainName
				+ "/jobs.php?q="
				+ userDataObject.keyword
				+ "&l="
				+ userDataObject.locationString
				+ "&r=20' >See all matching jobs ></a>"
				+ " </td></tr><tr>"

				+ "<td colspan='2' style='font-size:14px; white-space:nowrap;border-bottom:1px solid #CCC;'><span style='padding-bottom:15px;'>See what else "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ " has to offer:</span>"
				+ "<a href='http://"
				+ userDataObject.domainName
				+ "/sign_up.php?source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "&email="
				+ baseEncode64(userDataObject.email)
				+ "' style='color:#fff; white-space:nowrap;font-size:14px; text-decoration:none; background:#15c; padding:5px 8px; border-radius:3px; margin-left:10px;' target='_blank'>Sign Up for an account</a>"
				+ "</td></tr>"

				+ "<tr><td colspan='2' align='center'> <p style='font-size:12px;'>This message was sent to <a style='color:#0066cc;'"
				+ " href='mailto:"
				+ userDataObject.email
				+ "' >"
				+ userDataObject.email
				+ "</a> by "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ ".</p>"
				+ "<p style='font-size:12px'>To keep these emails coming, add <a style='color:#0066cc;' href='mailto:"
				+ "alerts@"
				+ userDataObject.domainName
				+ ""
				+ "'>"
				+ userDataObject.fromDomainName
				+ "</a> to your address book.</p>"
				+ "<p style='font-size:12px;'>"
				//

				+ postal_Address
				+ "</p>"
				+ "</td></tr>"

				+ "</table>"

				+ addArborPixel_with_internal(MainWhiteLabelClass.parameterModelClassObject,
						userDataObject)
				+ "<div style='text-align:center'>If you wish to turn off this alert "
				+ "<a "
				+ "href='"
				// +
				// addTraverse_pixel(MainWhiteLabelClass.parameterModelClassObject,
				// userDataObject) +
				// "<div style='text-align:center'>If you wish to turn off this alert "
				// + "<a " + "href='"
				+ unsubString
				+ "' target='_blank'> click here</a> , or to edit this alert or modify your notification frequency <a href='http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id)
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "' target='_blank'> click here</a></div>"
		// + "</body></html>"
		;

		if (userDataObject.sendGridCategory.contains("default value")) {
		} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
		} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
			htmlBodyEndTag += "<a href='%unsubscribe_url%' ></a>";
		} else {
			htmlBodyEndTag += "<a href='<UNSUBSCRIBE>' ></a>";
		}

		//
		// if
		// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
		// {
		// htmlBodyEndTag += "<a href='%unsubscribe_url%' ></a>";
		// }
		// else if
		// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
		// {
		// htmlBodyEndTag += "<a href='<UNSUBSCRIBE>' ></a>";
		// }

		String powerInbox = "";
		String topResume = "";
		if (MainWhiteLabelClass.parameterModelClassObject.isPowerInbox())
			powerInbox = powerInboxAdd(userDataObject, templateId);
		if (MainWhiteLabelClass.parameterModelClassObject.isTopResume())
			topResume = topResumeAddNew(userDataObject, templateId);

		String fullHtml = htmlBodyStartTag + topResume + jobMessage
				+ addToEmail(userDataObject, MainWhiteLabelClass.parameterModelClassObject)
				+ powerInbox + htmlBodyEndTag;

		return fullHtml;

	}

	public String emailTemplate4(UsersData userDataObject, List<Jobs> jobsArray, String groupId,
			String templateId) {

		boolean newOnceJob = false;

		String postal_Address = SettingsClass.child_postal_addresses
				.get(userDataObject.child_process_key);
		String image_postalAddress = "<img src='http://" + userDataObject.domainName
				+ "/postal_address_img/"
				+ MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() + ".png"
				+ "' alt='" + MainWhiteLabelClass.parameterModelClassObject.getDomainUrl()
				+ "' style = width: 357px;height: 57px; />";
		if (postal_Address == null) {
			System.out.println("getting null from hash map");
			postal_Address = MainWhiteLabelClass.parameterModelClassObject.getPostalAddress();
		}

		if (MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() != "") {
			postal_Address = image_postalAddress;
		}

		String htmlHead = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>";

		String unsubString = "http://" + userDataObject.domainName + "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.id) + "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		String todayDate = SettingsClass.todayDate;
		if (MainWhiteLabelClass.parameterModelClassObject.getDashboardName().toLowerCase()
				.contains("linkus"))
			todayDate = "";

		String htmlBodyStartTag = ""
				// + "<body style=\"margin:0;\">"
				+ addOpenpixels(userDataObject, groupId, templateId)
				+ "<table  style=\"border-collapse: collapse; margin:0 auto; width:620px; padding:0 13px; font-family:Arial, Helvetica, sans-serif; font-size:14px;\" width=\"620\" >"
				+ "<tr>"
				+ "<td style=\"border-bottom:1px solid #dcdcdc; padding:12px 0;\">"
				+ "<table style=\"margin:0; width:100%;\">"
				+ "<tr>"
				+ " <td style=\"text-align:left;\">"
				+ "<a href=\"http://"
				+ userDataObject.domainName
				+ "\" style=\"display: block;\"><img alt=\"At "
				+ userDataObject.domainName
				+ " we help people find a new job and employers hire the right candidates through our strict job search algorithm. We are both a job aggregator and a job search engine but we like to think of ourselves as a facilitator in the recruitment industry.\" style=\"border:none;display: block;\" src=\""
				+ MainWhiteLabelClass.parameterModelClassObject.getLogoUrl() + "\" /></a>"
				+ "</td>" + "<td style=\"text-align:right;font-size:14px; color:#666666;\">"
				+ todayDate + "</td>" + " </tr>" + "</table>" + "</td>" + "</tr>" + "<tr>"
				+ " <td style=\"padding:14px 0;border-bottom:1px solid #dcdcdc;\">"
				+ "<strong style=\"display:block; margin:0;font-size:15px;\">"
				+ userDataObject.getKeyword() + " jobs near " + userDataObject.getLocationString()
				+ "</strong>" + " <a href=\"http://" + userDataObject.domainName
				+ "/update_alert.php?id=" + baseEncode64(userDataObject.getId()) + "&amp;source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "\" style=\"color:#124fcf; font-size:13px;\">Edit this Alert</a>" + " </td>"
				+ "</tr>";
		String jobMessage = "";

		for (int i = 0; i < jobsArray.size(); i++) {
			// make redirection through url only for all queues
			String redirectionString = "";

			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setUserJobUrl(
					"http://"
							+ userDataObject.domainName
							+ "/"
							+ MainWhiteLabelClass.parameterModelClassObject
									.getRedirectionFileName() + ".php?q=" + redirectionString);

			String dateString = "";
			try {
				if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
					if (!newOnceJob) {
						SettingsClass.numberOfNewJobMails.incrementAndGet();
						newOnceJob = true;
					}
					dateString = "<span style='color: #F30E0E '><b>New </b></span> | ";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			String cityStateString = "";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity() + ", " + jobsArray.get(i).getState();
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity();
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getState();
			}

			String empolyerLoctionDateString = "";

			String employer = jobsArray.get(i).getEmployer();

			String empolyerString = "";

			if (employer != null && !employer.equalsIgnoreCase("")
					&& !employer.toLowerCase().contains("http"))
				empolyerString = "<strong style=\"color:#535353;\">" + employer + "</strong> - ";

			empolyerLoctionDateString += dateString + empolyerString + cityStateString;

			jobMessage += "<tr>"
					+ "<td colspan='2'>"
					+ "<div style='display:table; border-bottom:1px solid #ececec; width:100%'>"
					+ " <a style='text-decoration:none;' href=\""
					+ jobsArray.get(i).getUserJobUrl()
					+ "\" >"
					+ "<div style=\"display:table-cell; color:#434448; padding:10px 0;width:100%\">"
					+ "<strong style=\"font-size:16px;margin: 0 0 5px 0;display:block;color:#124fcf;\">"

					+ jobsArray.get(i).getTitle()

					+ "</strong>"
					+ " <p style=\"margin:0 0 5px 0; color:#898989;font-size:14px;\">"

					+ empolyerLoctionDateString

					// + " <span style='color: #F30E0E '> <b>" +
					// checkDate(jobsArray.get(i).getPostingdate()) +
					// "</b> </span>"
					// + (((checkDate(jobsArray.get(i).getPostingdate()) ==
					// "New") &&
					// !jobsArray.get(i).getEmployer().equalsIgnoreCase("")) ?
					// " | " : "") + "<strong style=\"color:#535353;\">"
					// + jobsArray.get(i).getEmployer()
					// + "</strong>" + pipeString

					+ " </p>"
					+ "<p style=\"display:inline-block;margin:0px; color:#124fcf;\">Easily apply </p>"
					+ "</div></a>" + " </div>" + "</td>" + "</tr>";

		}

		String htmlBodyEndTag = " <tr>"
				+ "<td style=\"text-align:center; padding:10px 0;border-bottom:1px solid #dcdcdc;\">"

				+ "<a href=\"http://"
				+ userDataObject.domainName
				+ "/jobs.php?q="
				+ userDataObject.getKeyword()
				+ "&amp;l="
				+ userDataObject.getLocationString()
				+ "\" style=\"text-decoration: none; color:#124fcf\"><strong>VIEW MORE JOBS</strong></a>"
				+ "</td>"
				+ " </tr>"
				+ "<tr>"
				+ "<td style=\"color:#666666; text-align:center; padding:20px 15px; line-height:1.4;\">"
				+ "  This message was sent to <a href=\"mailto:"
				+ userDataObject.getEmail()
				+ "\" style=\"text-decoration: none;color:#124fcf\">"

				+ userDataObject.getEmail()

				+ "</a> "

				+ "<br>"

				+ "To keep these emails coming, add <a href=\"mailto:"
				+ userDataObject.fromDomainName
				+ "\" style=\"text-decoration: none;color:#124fcf\">"
				+ userDataObject.fromDomainName
				+ "</a> to your address book<br>"
				+ ""
				+ postal_Address
				+ ""
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td style=\"font-size:14px; text-align:center; padding: 20px 0; border-top:1px solid #dcdcdc;\">"
				+ " <a href=\"http://"
				+ userDataObject.domainName
				+ "/alert_list.php\" style=\"color:#656565; display:inline-block; padding:0 5px;\">Manage your job alerts</a> |"
				+ " <a "
				+ "href=\"http://"
				+ userDataObject.domainName
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.getId() + "")
				+ "&amp;source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "&amp;email="

				+ baseEncode64(userDataObject.getEmail())

				+ "\" style=\"color:#656565; display:inline-block; padding:0 5px;\">Turn Off This Alert </a> |"
				+ " <a href=\"http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id) // $row['EmailGroup']
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "\" style=\"color:#656565; display:inline-block; padding:0 5px;;\"> Modify Notification Frequency</a>"
				+ "</td>"
				+ " </tr>"
				+ "</table>"
				+ addArborPixel_with_internal(MainWhiteLabelClass.parameterModelClassObject,
						userDataObject)
				// +
				// addTraverse_pixel(MainWhiteLabelClass.parameterModelClassObject,
				// userDataObject)
				+ "<a" + " href='" + unsubString + "' target='_blank'></a>"
		// + "</body>" + "</html>	"
		;

		if (userDataObject.sendGridCategory.contains("default value")) {
		} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
		} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
			htmlBodyEndTag += "<a href='%unsubscribe_url%' ></a>";
		} else {
			htmlBodyEndTag += "<a href='<UNSUBSCRIBE>' ></a>";
		}

		// if
		// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
		// {
		// htmlBodyEndTag += "<a href='%unsubscribe_url%' ></a>";
		// }
		// else if
		// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
		// {
		// htmlBodyEndTag += "<a href='<UNSUBSCRIBE>' ></a>";
		// }

		String powerInbox = "";
		String topResume = "";
		if (MainWhiteLabelClass.parameterModelClassObject.isPowerInbox())
			powerInbox = powerInboxAdd(userDataObject, templateId);

		if (MainWhiteLabelClass.parameterModelClassObject.isTopResume())
			topResume = topResumeAddNew(userDataObject, templateId);

		String fullHtml = htmlBodyStartTag + topResume + jobMessage
				+ addToEmail(userDataObject, MainWhiteLabelClass.parameterModelClassObject)
				+ powerInbox + htmlBodyEndTag;

		return fullHtml;
	}

	public String emailTemplate7(UsersData userDataObject, List<Jobs> jobsArray, String groupId,
			String templateId) {

		String postal_Address = SettingsClass.child_postal_addresses
				.get(userDataObject.child_process_key);
		String image_postalAddress = "<img src='http://" + userDataObject.domainName
				+ "/postal_address_img/"
				+ MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() + ".png"
				+ "' alt='" + MainWhiteLabelClass.parameterModelClassObject.getDomainUrl()
				+ "' style = width: 357px;height: 57px; />";

		if (postal_Address == null) {
			System.out.println("getting null from hash map");
			postal_Address = MainWhiteLabelClass.parameterModelClassObject.getPostalAddress();
		}
		if (MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress() != "") {
			postal_Address = image_postalAddress;
		}
		String unsubString = "http://" + userDataObject.domainName + "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.id) + "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email) + "";

		String powerInbox = "";
		String topResume = topResumeAddNew(userDataObject, templateId);

		String htmlBodyStartTag = ""
				+ addOpenpixels(userDataObject, groupId, templateId)
				// + "<body>"
				+ "<div style='width:746px;background-color:#f3f3f3; float:none; margin:auto; padding:0 10px 10px 10px;font-family:Arial, Helvetica, sans-serif;'>"
				+ "<table style='width:700px;padding:0px 0;'>"
				+ "<tr>"
				+ "<td>"
				+ "<span> "
				+ "<a href='"
				+ userDataObject.domainName
				+ "' title = '"
				+ userDataObject.domainName
				+ "' target='_blank'> <img src='"
				+ MainWhiteLabelClass.parameterModelClassObject.getLogoUrl()
				+ "' alt='At "
				+ userDataObject.domainName
				+ " we help people find a new job and employers hire the right candidates through our strict job search algorithm."
				+ " We are both a job aggregator and a job search engine but we like to think of ourselves as a facilitator in the recruitment industry.'/> "
				+ "</a>"
				+ " </span>"
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>"
				+ "<span style='font-size:19px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; color:#000'>"
				+ userDataObject.keyword
				+ " Jobs in "
				+ userDataObject.locationString
				+ "</span>"
				+ "</td>"
				+ "</tr>"
				+ "<tr>"
				+ " <td style='padding:5px 0 10px;'> <span style='font-size:11px; font-weight:bold;'>"
				+ " Not the right jobs for you? "
				+ "</span>   <a title='"
				+ userDataObject.domainName
				+ "' href='"
				+ "http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id)
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "' target='_blank' style='background-color:#f7604e; color:#fff; font-size:8px; padding:5px; text-decoration:none; text-transform:uppercase; font-weight:700; margin:0 10px;'>"
				+ "Update Your Alert"
				+ "</a>"
				+ "<span style='float: right;'>"
				+ "<a style='color:#0066cc; font-size:13px; font-weight:bold; text-decoration:none;white-space:nowrap;'"
				+ " href='http://"
				+ userDataObject.domainName
				+ "/jobs.php?q="
				+ userDataObject.keyword.replace(" ", "%20")
				+ "&l="
				+ userDataObject.locationString.replace(" ", "%20")
				+ "' target='_blank' title='This external link will open in a new window'>See all matching jobs &gt; </a>"
				+ "</span> </td>"
				+ "</tr>"
				+ "</table>"
				+ "<table style='width:700px;border:solid 1px #ddd;font-family:Arial, Helvetica, sans-serif;padding:20px;background:#fff'>"
				+ topResume + " <tr>" + "<td style='border-bottom:solid 1px #ddd; '>" + "</td>"
				+ " </tr>";

		// JOBS Message START
		String jobMessage = "";
		boolean onceNewjob = true;
		for (int i = 0; i < jobsArray.size(); i++) {

			// make redirection through url only for all queues
			String redirectionString = "";

			// passing usersData,Job,Job Position starting from 1, Subject line
			// (Group id)
			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setUserJobUrl(
					"http://" + userDataObject.domainName + "/RedirectAOL_CFU.php?q="
							+ redirectionString);
			String dateString = "";
			String newString = "";
			dateString += checkDateDaysAgo(jobsArray.get(i).getPostingdate());
			if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
				if (onceNewjob) {
					SettingsClass.numberOfNewJobMails.getAndIncrement();
					onceNewjob = false;
					newString = "<span style='color:#f15f4c;font-style:italic'> - new </span>";
				}
			}

			if (!dateString.equalsIgnoreCase("")) {
				dateString = " | " + dateString;
			}

			String cityStateString = "";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity() + "," + jobsArray.get(i).getState();
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity();
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getState();
			}

			String empolyerLoctionDateString = "<span style='display:block; padding:5px 0;'> ";

			String employer = jobsArray.get(i).getEmployer();

			String empolyerString = "";

			if (employer != null && !employer.equalsIgnoreCase("")
					&& !employer.toLowerCase().contains("http"))
				empolyerString = "<strong>" + employer + "</strong>" + " - ";

			empolyerLoctionDateString += empolyerString + cityStateString + dateString + "</span>";
			String jobBgStyle = "";

			if (i % 2 == 1) {
				jobBgStyle = "background-color:#f3f3f3;";
			}

			jobMessage = jobMessage

			+ "<tr>" + "<td style='padding:10px 20px;" + jobBgStyle + " font-size:12px;'>"
					+ "<p style='font-size:12px;padding:5px 0;font-weight:700; margin:0'>"
					+ "<a title='" + userDataObject.domainName + "' href='"
					+ jobsArray.get(i).getUserJobUrl() + "' style='color:#1380c2; '>"
					+ jobsArray.get(i).getTitle() + "</a>" + newString + "</p>"
					+ empolyerLoctionDateString + "</td>" + "</tr>";

		}

		jobMessage += addToEmail(userDataObject, MainWhiteLabelClass.parameterModelClassObject);
		// JOBS Message END

		String htmlBodyEndTag = "<table width='700' align='center' style='border:solid 1px #ddd; padding:0; background:#fff;text-align:center; margin:20px auto;'>"
				+ "<tr>"
				+ " <td colspan= '2' style='padding:20px 0 20px;'>"
				+ " <strong style='font-size:14px; display:block; padding:0 0 20px 0;'>Not getting the results you're looking for? </strong><br />"
				+ " <a title='"
				+ userDataObject.domainName
				+ "' href='http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id)
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "' target='_blank' style='background-color:#f7604e; color:#fff; font-size:15px; padding:8px; letter-spacing:1px; text-decoration:none; text-transform:uppercase; font-weight:700; margin:10px;'>Update Your Alert</a>"
				+ "</td>"
				+ " </tr>"
				+ "</table>"
				+ "<div style='font-size:12px; color:#888; width:700px; margin:0px auto; display:block; text-align:center;'>"
				+ " <a title='"
				+ userDataObject.domainName
				+ "' href='"
				+ unsubString
				+ "' target='_blank' style='color:#000;'> Unsubscribe </a> | <a title='"
				+ userDataObject.domainName
				+ "' href='http://"
				+ userDataObject.domainName
				+ "/alert_list.php' style='color:#000;'> Manage your alerts</a>"
				+ " <p style='color:#000;'>"
				+ "This message was sent to <a style='color:#0066cc;'"
				+ " href='mailto:"
				+ userDataObject.email
				+ "' >"
				+ userDataObject.email
				+ "</a> <br/> "
				+ "To keep these emails coming, add <a style='color:#0066cc;' href='mailto:"
				+ userDataObject.fromDomainName
				+ "'>"
				+ userDataObject.fromDomainName
				+ "</a> to your address book. "
				+ "<br/> "
				+ postal_Address
				+ " <br/>Powered by "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ ".</p></div>"
				+ addArborPixel_with_internal(MainWhiteLabelClass.parameterModelClassObject,
						userDataObject) + "</div>";

		htmlBodyEndTag += "<a title='" + userDataObject.domainName + "' href='<UNSUBSCRIBE>' ></a>";

		String fullHtml = htmlBodyStartTag + jobMessage + htmlBodyEndTag;

		return fullHtml;

	}

	private String checkDateDaysAgo(String postedDate) {
		// new code for old code see below
		if (postedDate != null && !postedDate.equalsIgnoreCase("null")
				&& !postedDate.equalsIgnoreCase("")) {
			Date date1 = null, date2 = null;
			GregorianCalendar calendar = new GregorianCalendar();
			if (postedDate.contains("/")) {
				String postedDateArray[] = postedDate.split("/");
				postedDate = postedDateArray[2] + "-" + postedDateArray[1] + "-"
						+ postedDateArray[0];
			}
			String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-"
					+ String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-"
					+ String.valueOf(calendar.get(Calendar.DATE));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date1 = sdf.parse(currentdate);
				date2 = sdf.parse(postedDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (date1.equals(date2)) {
				return "Posted today";
			} else if (date1.after(date2)) {
				LocalDate localDate1 = new LocalDate(date1.getTime());
				LocalDate localDate2 = new LocalDate(date2.getTime());
				int days = Days.daysBetween(localDate2, localDate1).getDays();
				String daysDiff = "";
				if (days == 1) {
					daysDiff = "Posted 1 day ago";
				} else {
					daysDiff = "Posted " + days + " days ago";
				}
				return daysDiff;
			}
		}

		return "";

	}

	public String addToEmail(UsersData userDataObject, ParameterModelClass parameterModelClassObject) {
		String jobMessage = "";
		String keywordsColumn = "";
		try {

			JSONObject jObject = new JSONObject();
			try {
				jObject.put("email", userDataObject.email);
				jObject.put("provider", userDataObject.providerName);
				jObject.put("source", userDataObject.getAdvertisement_id());
				if (parameterModelClassObject.isForEveningAlerts())
					jObject.put("emailtype", "evening");
				else
					jObject.put("emailtype", "morning");

				jObject.put("keyword", userDataObject.getKeyword());

			} catch (Exception e) {
				e.printStackTrace();
			}
			String base64 = baseEncode64(jObject.toString());

			Collections.shuffle(SettingsClass.keywordList);

			List<String> keywordList = SettingsClass.keywordList.subList(0, 9);
			keywordsColumn = "<tr><td colspan='2' style='border-bottom:1px solid #CCC;background-color:#F7F7F7;padding-left: 0px;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;' >"
					+ "<span style='text-align:center;background:#D8D8D8;padding:10px 20px;display:block;font-size:24px; font-weight:700; "
					+ "color:#262626;'>You may also be interested in these jobs:</span>"
					+ " <table style='width:100%;text-align: center;'>" + "<tr>";

			int insertTr = 0;
			for (String keyword : keywordList) {
				insertTr++;
				String url = "http://" + userDataObject.domainName + "/jobs.php?q="
						+ keyword.replace(" ", "%20") + "&l="
						+ userDataObject.getLocationString().replace(" ", "%20") + "&addtoemail="
						+ base64;

				// System.out.println(url);
				keywordsColumn = keywordsColumn
						+ "<td style='width:33.3%;padding:5px 10px;'><a style='display:inline-block; text-decoration:none;font-weight:bold;font-size: 14px;'"
						+ "href=" + url + ">" + keyword + "</a></td>";
				// Need to insert tr after 3 keywords
				if (insertTr == 3 || insertTr == 6) {
					keywordsColumn = keywordsColumn + "</tr><tr>";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		keywordsColumn = keywordsColumn + "</tr></table></td></tr>";
		jobMessage = jobMessage + keywordsColumn;

		return jobMessage;
	}

	public String emailTemplateplaintext(UsersData userDataObject, List<Jobs> jobsArray,
			String groupId, String templateId) {
		//
		// String postal_address =
		// "1 Store Hill Road, Suite 86, Old Westbury, NY 11568";
		String domainUrl = userDataObject.domainName;
		// String redirectFile = "RedirectAOL_CJU.php";

		String postal_Address = SettingsClass.child_postal_addresses
				.get(userDataObject.child_process_key);
		if (postal_Address == null) {
			System.out.println("getting null from hash map");
			postal_Address = MainWhiteLabelClass.parameterModelClassObject.getPostalAddress();
		}
		String image_postaladdress = MainWhiteLabelClass.parameterModelClassObject.getImage_postaladdress();

		if (image_postaladdress != null && !image_postaladdress.equalsIgnoreCase("")) {
			postal_Address = "";
		}

		String edit_this_alerts = "http://" + domainUrl + "/update_alert.php?id="
				+ baseEncode64(userDataObject.id) // $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml);

		String unsubscrib_alerts = "http://" + domainUrl
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.getId())// $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		String create_another_alerts = "http://" + domainUrl + "/create_alert.php?source="
				+ baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		String see_all_matching_jobs = "http://" + userDataObject.domainName + "/jobs.php?q="
				+ URLEncoder.encode(userDataObject.keyword) + "&l="
				+ URLEncoder.encode(userDataObject.locationString) + "&r=20";

		String sign_up = "http://" + domainUrl + "/sign_up.php?source="
				+ baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		String htmlStart = ""
				// + "\n\n" + "" + userDataObject.userSourceNameForHtml
				+ "\n\n" + "\n\n" + "\n\nDaily Job Alert\n\n" + userDataObject.getKeyword()
				+ " jobs near " + userDataObject.getLocationString() + "\n\n\nEdit this alert "
				+ edit_this_alerts + "\n\nSee all matching jobs " + "\n\n" + see_all_matching_jobs

				+ "\n\n";
		String jobMessage = "";

		for (int i = 0; i < jobsArray.size(); i++) {

			String redirectionString = "";
			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setJoburl(
					"http://"
							+ domainUrl
							+ "/"
							+ MainWhiteLabelClass.parameterModelClassObject
									.getRedirectionFileName() + ".php?q=" + redirectionString);

			String dateString = "";
			try {
				if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
					dateString = "New | ";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			String cityStateString = "";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity() + "," + jobsArray.get(i).getState();
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getCity();
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				cityStateString += jobsArray.get(i).getState();
			}

			String employer = jobsArray.get(i).getEmployer();

			String empolyerString = "";

			if (employer != null && !employer.equalsIgnoreCase("")
					&& !employer.toLowerCase().contains("http"))
				empolyerString = employer + " | ";

			String empolyerLoctionDateString = "";

			empolyerLoctionDateString += dateString + empolyerString + cityStateString;

			jobMessage = jobMessage + jobsArray.get(i).getTitle() + "\n\n"

			+ empolyerLoctionDateString

			+ "  Learn More \n\n" + jobsArray.get(i).getJoburl() + "\n"

			+ "\n";

		}

		// JOBS Message END

		String htmlBodyEndTag = "\n\n"

		+ "\n\n" + "Create another alert" + "\n\n"
				+ create_another_alerts
				// + "\n\n"

				+ "\n\nTurn Off This Alert"
				+ "\n\n"
				+ unsubscrib_alerts
				+ unsubscrib_alerts
				// + "\n\n"
				+ "\n\n See all matching jobs " + "\n\n" + see_all_matching_jobs
				+ "\n\n See what else "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ " has to offer:"

				+ "Sign Up for an account" + "\n\n" + sign_up + "\n\n"

				+ "\n\n  This message was sent to "

				+ userDataObject.getEmail() + "by "
				+ MainWhiteLabelClass.parameterModelClassObject.getHostDomainName()
				+ "\n\n To keep these emails coming, add " + userDataObject.fromDomainName
				+ " to your address book."

				+ postal_Address + "\n\n"
				// + ", or "
				+ "To Modify Notification Frequency" + " click here" + "\n\n" + edit_this_alerts;
		String fullPlainTextMail = htmlStart + jobMessage + htmlBodyEndTag;

		return fullPlainTextMail;

	}

	public String jobvitalTemplate(UsersData userDataObject, List<Jobs> jobsArray, String groupId,
			String templateId) {

		String unsubString = "http://" + userDataObject.domainName
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.id)// $row['EmailGroup']
				+ "&source=" + baseEncode64(userDataObject.userSourceNameForHtml) + "&email="
				+ baseEncode64(userDataObject.email);

		boolean newOnceJob = true;
		String companyList = "";
		int companyCount = 0;
		// String topResumeCriteria = "";

		for (int i = 0; i < jobsArray.size(); i++) {

			try {
				if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
					if (newOnceJob) {
						SettingsClass.numberOfNewJobMails.incrementAndGet();
						newOnceJob = false;
						break;
					}
				}
			} catch (Exception e) {
			}
		}

		String htmlHead = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/><title></title><meta name='viewport' content='width=device-width'/></head>";
		for (int i = 0; i < jobsArray.size(); i++) {

			if (!companyList.contains(jobsArray.get(i).getEmployer())) {

				companyList = companyList + jobsArray.get(i).getEmployer();
				companyCount++;

				if (companyCount == 10)
					break;
				companyList = companyList + ", ";

			}
		}

		String editThisAlertWithSililarJobs = "Similar <b>" + userDataObject.keyword
				+ "</b> Jobs Near " + userDataObject.getLocationString()
				+ "&nbsp;&nbsp;<a href='http://" + userDataObject.domainName
				+ "/update_alert.php?id=" + baseEncode64(userDataObject.id) + "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml) + "' >Edit this alert</a>";

		String htmlbody = "<body bgcolor='#ececeb' style='margin:0px; padding:0px;'>"
				+ addOpenpixels(userDataObject, groupId, templateId)
				+ "<table cellpadding='0' cellspacing='0' border='0' style='display:none;height:0px;margin:0px;padding:0px;'>"
				+ "<tr><td height='0' style='font-size:0px;font-family:Arial,sans-serif;display:none;height:0px;mso-line-height-rule:exactly;line-height:0px;margin:0px;padding:0px;text-indent:-9999px;'>"
				+ "Today\'s job matches from "
				+ companyList
				+ "</td></tr></table>"
				+ "<table width='100%' bgcolor='#47443e' cellpadding='0' cellspacing='0' border='0'><tr>"
				+ "<td align='center' valign='middle' style='padding:10px 10px 10px 10px;'>"
				+ "<table align='center' width='100%' cellpadding='0' cellspacing='0' border='0'>"
				+ "<tr><td width='600' align='left' valign='middle' style='max-width:600px;'><center>"
				+ "<div align='center' style='max-width:600px;text-align:left;margin:0 auto 0 auto;font-size:12px;color:#ececeb;font-family:arial,sans-serif;mso-line-height-rule:exactly;line-height:15px;-webkit-text-adjust:none;'>"
				+ "<strong>VitalMatch&trade;</strong> by&nbsp;&nbsp;<a href='http://"
				+ userDataObject.domainName
				+ "'>"
				+ "<img src='http://"
				+ userDataObject.domainName
				+ "/jobvitals_img/logo.gif' width='99' height='15' border='0' alt='At "
				+ userDataObject.domainName
				+ " we help people find a new job and employers hire the right candidates through our strict job search algorithm. We are both a job aggregator and a job search engine but we like to think of ourselves as a facilitator in the recruitment industry.' style='outline:none;border-style:none;text-decoration:none;width:99px;height:15px;' /></a>, a CareerBliss company.</div>"
				+ "</center></td>"
				// +editThisAlertLinkInTopHeader
				+ "</tr></table></td></tr></table>"

				+ "<table align='center' width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ececeb'>"
				+ "<tr>"
				+ "<td align='left' valign='top'></td>"
				+ "<td width='630' align='center' valign='top' style='max-width:630px;padding:0px 0px 30px 0px;'>"
				+ "<center>"
				+ "<table width='100%' cellpadding='0' cellspacing='0' border='0'>"
				+ "<tr>"
				+ "<td height='34' align='center' valign='middle' style='text-align:center;color:#6c6965;font-family:arial,sans-serif;font-size:12px;line-height:14px;mso-line-height-rule:exactly;-webkit-text-adjust:none;'>"
				+ "If you wish to turn off this alert "
				+ "<span style='color:#045cff;text-decoration:underline;'>"
				+ "<a href='"
				+ unsubString
				+ "' target='_blank' style='color:#045cff;text-decoration:underline;'>click here</a>"
				+ "</span>"
				// + editThisAlertLinkWithUnsub
				+ "</td>" + "</tr>" + "</table>";

		String redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(0), 1,
				groupId, templateId);
		jobsArray.get(0).setUserJobUrl(
				"http://" + userDataObject.domainName + "/"
						+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
						+ ".php?q=" + redirectionString);

		String pipeString1 = "";
		int j = 0;
		if (!jobsArray.get(j).getCity().equalsIgnoreCase("")
				&& !jobsArray.get(j).getState().equalsIgnoreCase("")) {
			pipeString1 += " - " + jobsArray.get(j).getCity() + "," + jobsArray.get(j).getState();
		} else if (!jobsArray.get(j).getCity().equalsIgnoreCase("")) {
			pipeString1 += " - " + jobsArray.get(j).getCity();
		} else if (!jobsArray.get(j).getState().equalsIgnoreCase("")) {
			pipeString1 += " - " + jobsArray.get(j).getState();
		}

		String firstJob = "<table width='100%' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' style='height:100%;max-width:600px;border:1px solid #e0dfde;-moz-box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);-webkit-box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);'>"
				+ "<tr><td align='left' valign='top' style='padding:15px 15px 10px 15px;text-align:left;'>"
				+ "<div style='font-family:arial,sans-serif;font-size:30px;mso-line-height-rule:exactly;line-height:40px;-webkit-text-adjust:none;'>"
				+ "<a href='"
				+ jobsArray.get(0).getUserJobUrl()
				+ "' style='display:block;color:#045cff;text-decoration:none;'><span style='color:#045cff;text-decoration:none;'>"
				+ "<strong>"
				+ jobsArray.get(0).getTitle()
				+ "</strong></span>"
				// + "</a>"
				+ "</div>"
				+ "<div style='font-family:arial,sans-serif;font-size:16px;mso-line-height-rule:exactly;line-height:40px;-webkit-text-adjust:none;'>"
				// +
				// "<a href='#' style='display:block;color:#000000;text-decoration:none;'><span style='color:#000000;text-decoration:none;'>"
				+ "<strong>"

				+ pipeString1

				// + jobsArray.get(0).getEmployer()
				// + "</strong> - "
				// + jobsArray.get(0).getCity()
				// + ", "
				// + jobsArray.get(0).getState()
				+ "</span></a>"
				+ "</div></td></tr>"
				+ "<tr><td align='left' valign='top' style='padding:0px 15px 20px 15px;text-align:left;'>"
				+ "<table cellspacing='0' cellpadding='0' border='0' style='display:inline-block;'><tr>"
				+ "<td align='center' valign='middle' height='45' bgcolor='#0ecc58' style='width:280px; height:45px; -moz-border-radius:3px; -webkit-border-radius:3px; border-radius:3px; color:#ffffff;'>"
				+ "<a style='display:block;width:100%;max-width:280px;height:45px;font-size:20px;mso-line-height-rule:exactly;line-height:45px;color:white;font-weight:bold;text-decoration:none;font-family:arial,sans-serif;' "
				+ "href='"
				+ jobsArray.get(0).getUserJobUrl()
				+ "'><span style='color:#ffffff'>View this Job</span></a></td></tr></table>"
				+ "</td></tr></table>";

		if (jobsArray.size() > 1) {
			firstJob = firstJob
					+ "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:630px;'>"
					+ "<tr><td align='left' valign='bottom' height='50' style='padding:0px 15px 0px 15px;font-family:arial,sans-serif;color:#84807a;font-size:14px;mso-line-height-rule:exactly;line-height:30px;-webkit-text-adjust:none;'>"
					+ editThisAlertWithSililarJobs + "</td></tr></table>";
		}

		String allJobs = "<table width='100%' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' style='max-width:600px;border-top:1px solid #e0dfde;border-right:1px solid #e0dfde;border-bottom:1px solid #e0dfde;-moz-box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);-webkit-box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);box-shadow: 2.121px 2.121px 6px rgba(1, 2, 2, 0.2);'>";

		String topResume = "";
		if (MainWhiteLabelClass.parameterModelClassObject.isTopResume())
			topResume = topResumeAddNew(userDataObject, templateId);

		allJobs += topResume;

		// allJobs += goodHireAdd(userDataObject, templateId);

		// for loop for all Jobs(start from 2nd job)
		for (int i = 1; i < jobsArray.size(); i++) {

			// make redirection through url only for all queues
			redirectionString = "";

			// passing usersData,Job,Job Position starting from 1, Subject line
			// (Group id)
			// http://"+userDataObject.domainName+"/RedirectAOLF_JVJ.php
			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setUserJobUrl(
					"http://"
							+ userDataObject.domainName
							+ "/"
							+ MainWhiteLabelClass.parameterModelClassObject
									.getRedirectionFileName() + ".php?q=" + redirectionString);

			String dateString = "";
			if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
				dateString += "<span style='color:#F30E0E;'><b>New</b></span> | ";
			}

			String pipeString = "";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				pipeString += " - " + jobsArray.get(i).getCity() + ","
						+ jobsArray.get(i).getState();
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				pipeString += " - " + jobsArray.get(i).getCity();
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				pipeString += " - " + jobsArray.get(i).getState();
			}

			String empolyerLoctionDateString = dateString
					+ "<span style='color:#333333;text-decoration:none;'><strong>"
					+ jobsArray.get(i).getEmployer() + "</strong> " + pipeString;

			allJobs = allJobs
					+ "<tr>"
					+ "<td align='left' valign='top' style='padding:15px 15px 15px 15px;font-size:14px;mso-line-height-rule:exactly;line-height:20px;text-align:left;font-family:arial,sans-serif;'>"
					+ "<a href='"
					+ jobsArray.get(i).getUserJobUrl()
					+ "' style='display:block;font-size:16px;color:#045cff;text-decoration:none;'><div>"
					+ "<span style='color:#045cff;text-decoration:none;'>"
					+ "<strong>"
					+ jobsArray.get(i).getTitle()
					+ "</strong>"
					+ "</span>"
					// + "</a>"
					+ "</div>"
					+ "<div>"
					+ empolyerLoctionDateString

					// + "<span style='color:#333333;text-decoration:none;'>" +
					// "<strong>" + jobsArray.get(i).getEmployer() +
					// "</strong> " + pipeString
					// + "</span>"

					+ "</div></a></td></tr>"

					+ "<tr><td height='1' bgcolor='#e0dfde' style='font-size:1px;mso-line-height-rule:exactly;line-height:1px;-webkit-text-adjust:none;'>&nbsp;</td>"
					+ "</tr>";

		}
		String powerInbox = "";
		if (MainWhiteLabelClass.parameterModelClassObject.isPowerInbox())
			powerInbox = powerInboxAdd(userDataObject, templateId);
		allJobs = allJobs

		+ powerInbox + "</table>";

		String htmlEnd = "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:630px;'>"
				+ "<tr><td align='left' valign='top' style='padding:10px 15px 0px 15px;color:#6c6965;font-size:14px;mso-line-height-rule:exactly;line-height:20px;-webkit-text-adjust:none;padding-top:8px;text-align:left;font-family:arial,sans-serif;'>"
				+ "<strong>Job Vitals&trade;</strong>, your personal job matching engine. Powered by CareerBliss.<br/>"
				+ "3420 Bristol St., Costa Mesa, CA 92626.<br/>"
				+ " <a style='color:#6c6965;text-decoration:none;' href='http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="
				+ baseEncode64(userDataObject.id) // /$row['EmailGroup']
				+ "&source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "'><span style='color:#6c6965;'>Edit this alert</span></a>"
				+ " | <a style='color:#6c6965;text-decoration:none;' href='"
				+ unsubString
				+ "'><span style='color:#6c6965;'>Turn Off This Alert</span></a>"
				+ "</td></tr></table></center></td><td align='left' valign='top'></td></tr></table>"
				// + addPixels(userDataObject)
				+ ""
		// + "</body></html>"
		;

		String fullHtml =
		// htmlHead +
		htmlbody + firstJob + allJobs + htmlEnd;

		return fullHtml;

	}

	public String paperroseTemplate(UsersData userDataObject, List<Jobs> jobsArray, String groupId,
			String templateId) {
		boolean newOnceJob = false;

		String htmlHead = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>"
				+ "<meta http-equiv='Content-type' content='text/html; charset=utf-8' /><title>PAPERROSE ALERTS | </title> <meta content=\"width=device-width, "
				+ "initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\"/></head>";

		String htmlBodyStartTag =

		"<body style=\"margin:0;\"> "
				+ addOpenpixels(userDataObject, groupId, templateId)
				+ "<table  style=\"border-collapse: initial; margin:0 auto; width:700px; font-family:Arial, Helvetica, sans-serif; font-size:14px;border:1px solid #d8d8d8;\" > "
				+ "<tr style='border-right:solid 2px #d8d8d8';> <td style=\"border-bottom:4px solid #ec4449; padding:10px 15px;\"> "
				+ "<table style=\"margin:0; width:100%;\"> <tr> <td style=\"text-align:left;\"> "
				+ "<a href=\"http://"
				+ userDataObject.domainName
				+ "\" style=\"display: block;\">"
				+ "<img alt=\"\" style=\"border:none;display: block;\" "
				+ "src=\"http://"
				+ userDataObject.domainName
				+ "/paperrosejobs_img/logo1_trans.png?refreshed=true\" /></a> "
				+ "</td> <td style=\"text-align:right;font-size:15px;\"> "

				+ SettingsClass.todayDate

				+ "</td> </tr> </table> </td> </tr>"

				+ "<tr>"
				+ "<td style=\"padding:10px 0;\">"
				+ "<table style=\"margin:0; width:100%;\">"
				+ "<tbody><tr>"
				+ "<td width='75%' style=\"font-size:16px; padding:9px 15px; color: #666666; text-align:left;\">"
				+ "<span style=\"color:#333333;\">" + userDataObject.getKeyword()
				+ "</span> within 30 miles of " + userDataObject.locationString + "</td>"
				+ "<td width='25%' style=\"text-align:right;font-size:15px;padding:9px 15px\">"
				+ "<a href=\"http://" + userDataObject.domainName + "/update_alert.php?id="

				// base encode alert_id
				+ baseEncode64(userDataObject.getId() + "")

				+ "&amp;source="

				// base encode web
				+ baseEncode64(userDataObject.userSourceNameForHtml)

				+ "\" style=\"color:#1155cc;\">Edit this Alert</a>"
				+ "</td></tr></tbody></table></td></tr>";

		// <!-- Start: One job HTML. Please put this in a loop -->
		String jobMessage = "";

		for (int i = 0; i < jobsArray.size(); i++) {

			// make redirection through url only for all queues
			String redirectionString = "";

			// passing usersData,Job,Job Position starting from 1, Subject line
			// (Group id)
			redirectionString = createCompressEncodeString(userDataObject, jobsArray.get(i), i + 1,
					groupId, templateId);
			jobsArray.get(i).setUserJobUrl(
					"http://"
							+ userDataObject.domainName
							+ "/"
							+ MainWhiteLabelClass.parameterModelClassObject
									.getRedirectionFileName() + ".php?q=" + redirectionString);

			String dateString = "";

			try {
				if (checkDate(jobsArray.get(i).getPostingdate()).equalsIgnoreCase("New")) {
					if (!newOnceJob) {
						SettingsClass.numberOfNewJobMails.incrementAndGet();
						newOnceJob = true;
					}
					dateString = "<span style='color: #F30E0E '><b>New </b></span> | ";

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// new

			String pipeString = "<span style=\"color:#1155cc;\">" + jobsArray.get(i).getEmployer()
					+ "</span>";
			if (!jobsArray.get(i).getCity().equalsIgnoreCase("")
					&& !jobsArray.get(i).getState().equalsIgnoreCase("")) {
				pipeString += " ( " + jobsArray.get(i).getCity() + ","
						+ jobsArray.get(i).getState() + ")";
			} else if (!jobsArray.get(i).getCity().equalsIgnoreCase("")) {
				pipeString += " ( " + jobsArray.get(i).getCity() + ")";
			} else if (!jobsArray.get(i).getState().equalsIgnoreCase("")) {
				pipeString += " ( " + jobsArray.get(i).getState() + ")";
			}

			String empolyerLoctionDateString = dateString + pipeString;

			jobMessage += "<tr> <td style=\"background-color:#f5f7f5; color:#434448; padding:15px; border-top:1px solid #d8d8d8;\"> "

					+ "<a style='text-decoration:none; font-size : 16px;' href='"
					+ jobsArray.get(i).getUserJobUrl()
					+ "' >"
					+ " <div style='display:table;width:100%;'> <div style='display:table-cell;'> <strong>"
					+ jobsArray.get(i).getTitle()
					+ "</strong><p style=\"margin:5px 0 2px 0; color:#5f605d;\"> "

					+ empolyerLoctionDateString

					// + " <span style='color: #F30E0E '> <b>" +
					// checkDate(jobsArray.get(i).getPostingdate()) +
					// "</b> </span>"
					// + (((checkDate(jobsArray.get(i).getPostingdate()) ==
					// "New") &&
					// !jobsArray.get(i).getEmployer().equalsIgnoreCase("")) ?
					// " | " : "")
					// + "<span style=\"color:#1155cc;\">"
					// + jobsArray.get(i).getEmployer()
					// + "</span>"
					// + pipeString

					+ "<br>"
					+ " ("
					+ formatJobDate(jobsArray.get(i).getPostingdate())
					+ ")"
					+ "</p> </div> <div style=\" display:table-cell;text-align:center;width:32px;\">"

					+ " <img style=\"border:none;\" "
					+ "src=\"http://"
					+ userDataObject.domainName
					+ "/img/right-arrow.png\" alt=\"\" align='top'> </div> </div> "

					+ "</a>"

					+ " </td> </tr>";

		}

		String htmlBodyEndTag = "<tr> <td style=\"color:#333333; text-align:center; padding:20px 15px; border-top:1px solid #d8d8d8; line-height:1.4;\"> "
				+ "This message was sent to " + "<a href=\"mailto:"
				+ userDataObject.getEmail()
				+ "\" style=\"text-decoration: none;\">"
				+ userDataObject.getEmail()
				+ "</a> by <strong>PaperRoseJobs</strong>.<br /> To keep these emails coming, add "
				+ "<a href=\"mailto:"
				+ userDataObject.fromDomainName
				+ "\" style=\"text-decoration: none;\">"
				+ userDataObject.fromDomainName

				+ "</a> to your address book<br /> "

				+ "1 Store Hill Road, Suite 86, Old Westbury, NY 11568 "

				+ "</td> </tr> <tr> "
				+ "<td style=\"background-color:#f1f0f0;color:#3563c0; font-size:15px; padding: 10px 15px; border-top:2px solid #ec4449;\"> "
				+ "<table style=\"margin:0; width:100%;\"> <tr> <td> "

				+ " <a href=\"http://"
				+ userDataObject.domainName
				+ "/alert_list.php\" style=\"color:#3563c0;\">Manage your job alerts</a><br />"
				+ "<a "
				+ "href=\"http://"
				+ userDataObject.domainName
				+ "/unsubscribe.php?alert_id="
				+ baseEncode64(userDataObject.getId())
				+ "&amp;source="
				+ baseEncode64(userDataObject.userSourceNameForHtml)
				+ "&amp;email="
				+ baseEncode64(userDataObject.getEmail())
				+ "\" style=\"color:#3563c0;\">Turn Off This Alert </a><br />"
				+ " <a href=\"http://"
				+ userDataObject.domainName
				+ "/update_alert.php?id="

				// base encode alert_id
				+ baseEncode64(userDataObject.getId() + "")

				+ "&amp;source="

				// base encode web
				+ baseEncode64(userDataObject.userSourceNameForHtml)

				+ "\" style=\"color:#3563c0;\">Modify Notification Frequency </a>"
				+ " </td> <td style=\"text-align:right;\"> "
				+ "<a href=\"http://"
				+ userDataObject.domainName
				+ "\" style=\"display: block;\">"
				+ " <img style=\"border:none;\" "
				+ "src="
				+ "\"http://"
				+ userDataObject.domainName
				+ "/paperrosejobs_img/logo1_trans.png\" "
				+ "alt=\"\"/> </a> </td> </tr> </table>  </td> </tr> </table> "
				// + addPixels(userDataObject)
				+ "</body> </html>";
		String powerInbox = "";
		String topResume = "";
		if (MainWhiteLabelClass.parameterModelClassObject.isPowerInbox())
			powerInbox = powerInboxAdd(userDataObject, templateId);
		if (MainWhiteLabelClass.parameterModelClassObject.isTopResume())
			topResume = topResumeAddNew(userDataObject, templateId);

		String fullHtml = htmlHead + htmlBodyStartTag + topResume + jobMessage
				+ addToEmail(userDataObject, MainWhiteLabelClass.parameterModelClassObject)
				+ powerInbox + htmlBodyEndTag;

		return fullHtml;

	}

	static String formatJobDate(String date) {

		// date="2015-09-17T00:00:00Z";
		Date date1 = null;
		String convertedDate = "";
		if (date != null && !date.equalsIgnoreCase("null") && !date.equalsIgnoreCase("")) {
			if (date.contains("T")) {
				String date_new[] = date.split("T");
				date = date_new[0];
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yy");
			try {
				date1 = sdf.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				convertedDate = sdf1.format(date1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// System.out.println(sdf1.format(new Date()));

		return convertedDate;
	}

	public String addOpenpixels(UsersData userDataObject, String groupId, String templateId) {

		String pixels = "<img alt='Find your next job at " + userDataObject.domainName
				+ "' width='0' height='0' style='visibility:hidden;'" + "src='" + "http://"
				+ userDataObject.domainName + "/opencapture.php?e="
				+ baseEncode64(userDataObject.email) + "&tid=" + templateId + "&gid=" + groupId
				+ "&d=" + SettingsClass.dateFormat.format(new Date()) + "&c="
				+ userDataObject.categoryProviderForOpenPixels + "'/>";

		if (userDataObject.sendGridCategory.contains("default value")) {
			pixels += "<img alt=\"\" src='http://www."
					+ userDataObject.domainName
					+ "/backend/track_ses.php?e="
					+ baseEncode64(userDataObject.email.toLowerCase().trim())
					+ "&p="
					+ baseEncode64(userDataObject.categoryProviderForOpenPixels)
					+ "&sid="
					+ groupId
					+ "&template_id="
					+ templateId
					+ "' style='max-width: 0px; display: none; max-height: 0px; font-size: 0px; overflow: hidden; mso-hide: all''/>";
		}
		return pixels;
	}

	public String addPixels(UsersData userDataObject) {
		String pixels = "<img alt=\"\" border='0' hspace='0' vspace='0' width='1' height='1' src='http://images."
				+ userDataObject.domainName
				+ "/"
				+ userDataObject.liveRamp
				+ ".gif?s="
				+ sha1Converter(userDataObject.email.toLowerCase().trim())
				+ "&n=1'/>"
				+ "<img alt=\"\" border='0' hspace='0' vspace='0' width='1' height='1' src='http://images."
				+ userDataObject.domainName
				+ "/"
				+ userDataObject.liveRamp
				+ ".gif?s="
				+ sha1Converter(userDataObject.email.toLowerCase().trim())
				+ "&n=2'/>"
				+ "<img alt=\"\" border='0' hspace='0' vspace='0' width='1' height='1' src='http://images."
				+ userDataObject.domainName
				+ "/"
				+ userDataObject.liveRamp
				+ ".gif?s="
				+ sha1Converter(userDataObject.email.toLowerCase().trim())
				+ "&n=3'/>"
				+ "<img alt=\"\" border='0' hspace='0' vspace='0' width='1' height='1' src='http://images."
				+ userDataObject.domainName
				+ "/"
				+ userDataObject.liveRamp
				+ ".gif?s="
				+ sha1Converter(userDataObject.email.toLowerCase().trim())
				+ "&n=4'/>"
				+ "<img alt=\"\" border='0' hspace='0' vspace='0' width='1' height='1' src='http://images."
				+ userDataObject.domainName
				+ "/"
				+ userDataObject.liveRamp
				+ ".gif?s="
				+ sha1Converter(userDataObject.email.toLowerCase().trim()) + "&n=5'/>";

		return pixels;

	}

	// Adds Block================
	public String powerInboxAdd(UsersData userDataObject, String templateId) {
		String powerInbox = "<tr><td colspan='2' style='border-bottom:1px solid #ccc;padding-bottom:10px'><!--POWERINBOX--><div class='powerinbox'><!-- domain: rs-1098-a.com -->"
				+ "<table width='594' border='0' cellpadding='0' cellspacing='0'><tbody><tr>"
				+ "<td align='left' valign='middle' style='color: #383838; font-family: Arial, sans-serif; font-size: 18px; font-weight: bold;'>You Might Like</td>"
				+ "<td align='right'>" + "<a href='" + "http://"
				+ userDataObject.domainName
				+ "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(
						userDataObject,
						"http://info.revenuestripe.com/?utm_source=contentstripe&utm_medium=email&utm_campaign=firebrickgroup&utm_content=animatedlogo",
						"powerinbox", templateId)
				+ "' style='display: inline-block; border: 0; outline:none; text-decoration:none;' target='_blank'>"
				+ "<img src='http://branding.revenuestripe.com/recommend/transparent.gif' style='width: 143px; height: 40px;' width='143' height='40' border='0' alt='Learn more about RevenueStripe...' /></a></td>"
				+ "</tr></tbody></table>"
				+ "<table width='594' class='fallback'  border='0' cellspacing='0' cellpadding='0'>"
				+ "<tbody><tr>"
				+ "<td width='144' style='border-collapse:collapse; padding-right: 6px;'>"
				+ "<a href='"
				+ "http://"
				+ userDataObject.domainName
				+ "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject,
						"http://stripe.rs-1098-a.com/stripe/redirect?cs_email="
								+ userDataObject.email
								+ "&cs_esp=amazonses&cs_offset=0&cs_stripeid=2425", "powerinbox",
						templateId)
				+ "' style='border-style: none;outline: none;text-decoration: none;' target='_blank'>"
				+ "<img alt='' height='176' src='http://stripe.rs-1098-a.com/stripe/image?cs_email="
				+ userDataObject.email
				+ "&cs_esp=amazonses&cs_offset=0&cs_stripeid=2425' style='display: block;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;' width='144'></a></td>"
				+ "<td width='144' style='border-collapse:collapse; padding-right: 6px;'>"
				+ "<a href='"
				+ "http://"
				+ userDataObject.domainName
				+ "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject,
						"http://stripe.rs-1098-a.com/stripe/redirect?cs_email="
								+ userDataObject.email
								+ "&cs_esp=amazonses&cs_offset=1&cs_stripeid=2425", "powerinbox",
						templateId)
				+ "' style='border-style: none;outline: none;text-decoration: none;' target='_blank'>"
				+ "<img alt='' height='176' src='http://stripe.rs-1098-a.com/stripe/image?cs_email="
				+ userDataObject.email
				+ "&cs_esp=amazonses&cs_offset=1&cs_stripeid=2425' style='display: block;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;' width='144'></a></td>"
				+ "<td width='144' style='border-collapse:collapse; padding-right: 6px;'>"
				+ "<a href='"
				+ "http://"
				+ userDataObject.domainName
				+ "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject,
						"http://stripe.rs-1098-a.com/stripe/redirect?cs_email="
								+ userDataObject.email
								+ "&cs_esp=amazonses&cs_offset=2&cs_stripeid=2425", "powerinbox",
						templateId)
				+ "' style='border-style: none;outline: none;text-decoration: none;' target='_blank'>"
				+ "<img alt='' height='176' src='http://stripe.rs-1098-a.com/stripe/image?cs_email="
				+ userDataObject.email
				+ "&cs_esp=amazonses&cs_offset=2&cs_stripeid=2425' style='display: block;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;' width='144'></a></td>"
				+ "<td width='144' style='border-collapse:collapse;'>"
				+ "<a href='"
				+ "http://"
				+ userDataObject.domainName
				+ "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject,
						"http://stripe.rs-1098-a.com/stripe/redirect?cs_email="
								+ userDataObject.email
								+ "&cs_esp=amazonses&cs_offset=3&cs_stripeid=2425", "powerinbox",
						templateId)
				+ "' style='border-style: none;outline: none;text-decoration: none;' target='_blank'>"
				+ "<img alt='' height='176' src='http://stripe.rs-1098-a.com/stripe/image?cs_email="
				+ userDataObject.email
				+ "&cs_esp=amazonses&cs_offset=3&cs_stripeid=2425' style='display: block;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;' width='144'></a></td>"
				+ "</tr></tbody></table></div><!--POWERINBOX--></td></tr>";

		return powerInbox;

	}

	// public String topResumeAdd(UsersData userDataObject, String templateId) {
	// String resumeRedirctionString = "http://" +
	// userDataObject.domainName + "/" +
	// MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
	// + ".php?q=" + createCompressStringForTopResumeAdd(userDataObject,
	// "https://www.topresume.com/?action=newindex&pt=2wZFs2rHeRyTl&utm_source=jser",
	// "TopResume", templateId);
	//
	// String topResumeCriteria =
	// "<tr><td colspan='2' style='padding:0'><table border='0' cellspacing='0' cellpadding='0'style='width:100%;border-collapse:collapse;border:1px solid #e5e5e5;border-width: 0px 0px 1px 0px;' id='alertsTable'><tr><td width='50' align='center' valign='top'style='padding:10px;background: #f8ffe6;'>"
	// + "<a href='"
	// + resumeRedirctionString
	// +
	// "' style='display:block;text-decoration:none;color:#777;' target='_blank'>"
	// + "<img alt='TopResume' src='http://"
	// + userDataObject.domainName
	// +
	// "/img/logo-topresume-cb.png' border='0' width='50' height='50'style='display:block;'/></a></td><td width=''align='left' valign='middle'style='font-family:Arial,sans-serif;font-size:15px;color:#333;padding:10px;font-weight:100;background: #f8ffe6;'>"
	// + "<a href='"
	// + resumeRedirctionString
	// +
	// "' target='_blank' style='color: #0066cc; font-size: 16px; text-decoration: none;'>"
	// +
	// "Does your resume pass the 30-second test ? Find out for free!</a><br/><p style='font-size: 13px; margin: 8px 0;'>See what employers think of your resume. Click here for your free resume evaluation for a trusted resume expert.</p>"
	// +
	// "</td><td width='50' align='center' valign='top'style='background: #f8ffe6;vertical-align:middle;'><a href='"
	// + resumeRedirctionString
	// +
	// "' style='display:block;text-decoration:none;color:#777;'target='_blank'>"
	// + "<img alt='View Job' src='http://"
	// + userDataObject.domainName
	// +
	// "/img/arrow-topresume-cb.png' border='0' width='40' height='40'style='display:block;margin-right:8px;'/></a></td>"
	// + "</tr></table></td></tr>";
	//
	// return topResumeCriteria;
	//
	// }

	public String topResumeAddNew(UsersData userDataObject, String templateId) {

		// if
		// (MainWhiteLabelClass.parameterModelClassObject.getDashboardName().toLowerCase().contains("linkus"))
		// {
		// return linkusAdd(userDataObject, templateId);
		// }

		// NEW ADD OF ISF BANNER
		// name = "isf_banner_latest.jpg";
		// url =
		// "http://interviewsuccessformula.ontraport.net/t?orid=269619&opid=91";
		// provider = "ISF BANNER 1";

		// String name = "isf_banner_new.jpg";
		// String url =
		// "http://interviewsuccessformula.ontraport.net/t?orid=240417&opid=89";
		// String provider = "ISF BANNER";
		//
		String name = "topresume_add1.jpg";
		String url = "https://www.topresume.com/?action=newindex&pt=2wZFs2rHeRyTl&utm_source=jser";
		String provider = "TopResume1";

		synchronized (SettingsClass.topResumeCounter) {
			if (SettingsClass.topResumeCounter.get() % 6 == 0) {
				if ((SettingsClass.topResumeCounter.get() / 6) == 1) {
					name = "topresume_add1.jpg";
					url = "https://www.topresume.com/?action=newindex&pt=2wZFs2rHeRyTl&utm_source=jser";
					provider = "TopResume1";
				} else if ((SettingsClass.topResumeCounter.get() / 6) == 2) {
					name = "topresume_add2.jpg";
					url = "https://www.topresume.com/?action=newindex&pt=2wZFs2rHeRyTl&utm_source=jser";
					provider = "TopResume2";
				} else if ((SettingsClass.topResumeCounter.get() / 6) == 3) {
					name = "topresume_add3.jpg";
					url = "https://www.topresume.com/?action=newindex&pt=2wZFs2rHeRyTl&utm_source=jser";
					provider = "TopResume3";
				}
			}
			if (SettingsClass.topResumeCounter.get() >= 20)
				SettingsClass.topResumeCounter.set(0);

			SettingsClass.topResumeCounter.incrementAndGet();
		}

		String resumeRedirctionString = "http://" + userDataObject.domainName + "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject, url, provider, templateId);
		String add = "<tr><td colspan='2'>" + "<a href='" + resumeRedirctionString
				+ "' target='_blank'>" + "<img width='100%' height='100' alt='' " + "src='http://"
				+ userDataObject.domainName + "/topresume_add_img/" + name + "'/> " + "</a>"
				+ "</td></tr>";

		return add;
		// return "";

	}

	private String linkusAdd(UsersData userDataObject, String templateId) {

		String name = "linkus_banner.jpg";
		String url = "http://www.linkushr.com/blog/";
		String provider = "LINKUS BANNER";

		if (SettingsClass.topResumeCounter.get() == 1) {
			name = "linkus_banner.jpg";
			provider = "LINKUS BANNER";
			url = "http://www.linkushr.com/blog/";
		} else if (SettingsClass.topResumeCounter.get() >= 2) {
			name = "isf_banner_new.jpg";
			url = "http://interviewsuccessformula.ontraport.net/t?orid=240417&opid=89";
			provider = "ISF BANNER";
			SettingsClass.topResumeCounter.set(0);
		}
		SettingsClass.topResumeCounter.incrementAndGet();

		String resumeRedirctionString = "http://" + userDataObject.domainName + "/"
				+ MainWhiteLabelClass.parameterModelClassObject.getRedirectionFileName()
				+ ".php?q="
				+ createCompressStringForTopResumeAdd(userDataObject, url, provider, templateId);
		String add = "<tr><td colspan='2'>" + "<a href='" + resumeRedirctionString
				+ "' target='_blank'>" + "<img width='100%' height='100' alt='' " + "src='http://"
				+ userDataObject.domainName + "/topresume_add_img/" + name + "'/> " + "</a>"
				+ "</td></tr>";

		return add;

	}

	private String createCompressStringForTopResumeAdd(UsersData userDataObject, String resumeUrl,
			String providername, String templateId) {

		String currentDate = SettingsClass.dateFormat.format(new Date());
		String grossCpc = "0.16";
		String Cpc = "0.16";
		if (providername.contains("ISF BANNER")) {
			grossCpc = "0.20";
			Cpc = "0.20";
		} else if (providername.contains("LINKUS BANNER")) {
			grossCpc = "0";
			Cpc = "0";
		} else {
			grossCpc = "0.16";
			Cpc = "0.16";
		}
		String redirectRecords = userDataObject.email + "|#|" + "|#|" + "|#|" + "|#|" + "|#|"
				+ userDataObject.keyword + "|#|" + userDataObject.zipcode + "|#|" + resumeUrl
				+ "|#|" + providername + "|#|" + userDataObject.providerName + "|#|" + currentDate
				+ "|#|" + "|#|" + "|#|" + templateId + "|#|" + "|#|" + "|#|" + userDataObject.id
				+ "|#|" + userDataObject.radius + "|#|" + userDataObject.firstName + "|#|"
				+ grossCpc + "|#|" + Cpc + "|#|" + userDataObject.campgain_category_mapping_key;

		try {
			redirectRecords = Base64.encodeBase64String(compressString(redirectRecords));
		} catch (IOException e) {

			e.printStackTrace();
		}

		return redirectRecords;

	}

	// end of ads block

	private String baseEncode64(String strToEncode) {
		// encoding byte array into base 64
		byte[] encoded = Base64.encodeBase64(strToEncode.getBytes());
		return new String(encoded);

	}

	private String checkDate(String postedDate) {
		// new code for old code see below
		if (postedDate != null && !postedDate.equalsIgnoreCase("null")
				&& !postedDate.equalsIgnoreCase("")) {

			Date date1 = null, date2 = null;
			GregorianCalendar calendar = new GregorianCalendar();

			if (postedDate.contains("/")) {

				String postedDateArray[] = postedDate.split("/");
				// year-mm-date
				postedDate = postedDateArray[2] + "-" + postedDateArray[1] + "-"
						+ postedDateArray[0];
			}

			String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-"
					+ String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-"
					+ String.valueOf(calendar.get(Calendar.DATE));

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			try {

				date1 = sdf.parse(currentdate);

				date2 = sdf.parse(postedDate);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (date1.equals(date2)) {
				return "New";
			} else {
				return "";
			}
		}

		return "";

	}

	private String sha1Converter(String email) {

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(email.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();

	}

	private String createCompressEncodeString(UsersData userDataObject, Jobs job, int job_position,
			String group_id, String templateId) {

		String currentDate = SettingsClass.dateFormat.format(new Date());
		String redirectRecords = "";
		String input = "";

		// EMAILID|#|CLICKED JOB TITLE|#|CLICKED JOB CITY|#|
		// CLICKED JOB STATE|#|CLICKED ZIP|#|SEARCHED KEYWPORD|#|
		// SEARCHED ZIP|#|JOB URL|#|
		// JOB SOURCE|#|PROVIDER|#|CURRENT DATE
		// |#|JOB POSITION|#|GROUP ID|#|TEMPLATE ID|#|EXECT MATCH OR
		// SYNONYM|#|API OR CS

		if (job.getSourcename().contains("Jobs2Careers")
				|| job.getSourcename().contains("J2C APIDE")
				|| job.getSourcename().contains("J2C Trucking")
				|| job.getSourcename().contains("J2C Boolean")) {
			job.setJoburl(job.getJoburl() + userDataObject.t2Value);
		}

		// if (job.getSourcename().contains("Jobs2Careers") ||
		// job.getSourcename().contains("J2C APIDE") ||
		// job.getSourcename().contains("J2C Trucking") ||
		// job.getSourcename().contains("J2C Boolean")) {
		// input = userDataObject.email + "|#|" + job.getTitle() + "|#|" +
		// job.getCity() + "|#|" + job.getState() + "|#|" + job.getZipcode() +
		// "|#|" + userDataObject.keyword + "|#|"
		// + userDataObject.zipcode + "|#|" + job.getJoburl() + "&t2=" +
		// MainWhiteLabelClass.parameterModelClassObject.getT2ValueJ2c() + "|#|"
		// + job.getSourcename() + "|#|"
		// + userDataObject.providerName + userDataObject.affiliatCode + "|#|" +
		// currentDate + "|#|" + job_position + "|#|" + group_id + "|#|" +
		// templateId + "|#|" + job.getExactOrSynonym()
		// + "|#|" + job.getApiOrCS();
		// } else {

		String base64EncodedJobUrl = job.getJoburl();

		if (MainWhiteLabelClass.isEncodeJobUrl)
			base64EncodedJobUrl = baseEncode64(job.getJoburl());

		input = userDataObject.email + "|#|" + job.getTitle() + "|#|" + job.getCity() + "|#|"
				+ job.getState() + "|#|" + job.getZipcode() + "|#|" + userDataObject.keyword
				+ "|#|" + userDataObject.zipcode + "|#|" + base64EncodedJobUrl + "|#|"
				+ job.getSourcename() + "|#|" + userDataObject.providerName
				// + userDataObject.affiliatCode
				+ "|#|" + currentDate + "|#|" + job_position + "|#|" + group_id + "|#|"
				+ templateId + "|#|" + job.getExactOrSynonym() + "|#|" + job.getApiOrCS();

		input += "|#|" + userDataObject.id + "|#|" + userDataObject.radius + "|#|"
				+ userDataObject.firstName + "|#|" + job.getGross_cpc() + "|#|" + job.getCpc()
				+ "|#|" + userDataObject.campgain_category_mapping_key;

		try {
			redirectRecords = Base64.encodeBase64String(compressString(input));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return redirectRecords;

	}

	public static byte[] compressString(String data) throws IOException {
		byte[] compressed = null;
		byte[] byteData = data.getBytes();
		ByteArrayOutputStream bos = new ByteArrayOutputStream(byteData.length);
		Deflater compressor = new Deflater();
		compressor.setLevel(Deflater.BEST_COMPRESSION);
		compressor.setInput(byteData, 0, byteData.length);
		compressor.finish();

		// Compress the data
		final byte[] buf = new byte[1024];
		while (!compressor.finished()) {
			int count = compressor.deflate(buf);
			bos.write(buf, 0, count);
		}
		compressor.end();
		compressed = bos.toByteArray();
		bos.close();
		return compressed;
	}

	private String addArborPixel_with_internal(ParameterModelClass paraMeterModel,
			UsersData userDataObject) {
		// http://images1.domain_name?pid=1234,1235,1254&email=bse64{email}
		String arbor_pixel = SettingsClass.child_process_arbor_pixels
				.get(userDataObject.child_process_key);
		if (arbor_pixel == null) {
			arbor_pixel = paraMeterModel.getArbor_pixel();
		}
		String pixels = "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=1"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=2"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=3"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=4"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=5"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=6"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=7"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=8"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=9"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail())
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://images1."
				+ userDataObject.domainName
				+ "?pid="
				+ arbor_pixel
				+ "&number=10"
				+ "&email="
				+ baseEncode64(userDataObject.getEmail()) + "'/>";

		if (MainWhiteLabelClass.parameterModelClassObject.getArbor_pixel().equalsIgnoreCase(""))
			pixels = "";

		return pixels;
	}

	private String addTraverse_pixel(ParameterModelClass paraMeterModel, UsersData userDataObject) {
		// http://images1.domain_name?pid=1234,1235,1254&email=bse64{email}
		String arbor_pixel = SettingsClass.child_process_arbor_pixels
				.get(userDataObject.child_process_key);
		if (arbor_pixel == null) {
			arbor_pixel = paraMeterModel.getArbor_pixel();
		}
		String md5EmailString = createMd5String(userDataObject.email);
		String sha1EmailString = sha1Converter(userDataObject.email);

		String pixels = "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/0.gif?emailMd5Lower="
				+ md5EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/1.gif?emailMd5Lower="
				+ md5EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/2.gif?emailMd5Lower="
				+ md5EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/3.gif?emailMd5Lower="
				+ md5EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/4.gif?emailMd5Lower="
				+ md5EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/0.gif?emailSha1Lower="
				+ sha1EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/1.gif?emailSha1Lower="
				+ sha1EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/2.gif?emailSha1Lower="
				+ sha1EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/3.gif?emailSha1Lower="
				+ sha1EmailString
				+ "'/>"
				+ "<img alt=\"\" style='border:none;' width='1' height='1' src='http://traverse."
				+ userDataObject.domainName
				+ "/v1/d6b07d75-d064-44ea-a437-0851513729fd/4.gif?emailSha1Lower="
				+ sha1EmailString + "'/>" + "";

		if (MainWhiteLabelClass.parameterModelClassObject.getArbor_pixel().equalsIgnoreCase(""))
			pixels = "";

		return pixels;
	}

	public String createMd5String(String email) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(email.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}

		System.out.println("original:" + email);
		System.out.println("digested(hex):" + sb.toString());
		return sb.toString();
	}
}
