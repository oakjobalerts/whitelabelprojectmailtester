package whitelabels.mailsend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.core.MediaType;

import net.spy.memcached.internal.OperationFuture;
import whitelabels.mainpackage.MainWhiteLabelClass;
import whitelabels.mainpackage.MainWhiteLableMailtesterMain;
import whitelabels.mainpackage.SettingsClass;
import whitelabels.mainpackage.Utility;
import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.impl.MultiPartWriter;

public class MailGunClass implements MailSendInterface {

	GregorianCalendar calendar;

	static ArrayList<String> fromNameList = new ArrayList<String>();
	Calendar calcurrentDate;
	SimpleDateFormat formatter;
	String baseName = "";
	public List<String> bccMailList;
	EmailTemplates emailTemplateObject = null;

	public MailGunClass() {

		emailTemplateObject = new EmailTemplates();

		calcurrentDate = Calendar.getInstance();
		formatter = new SimpleDateFormat("yyyyMMdd");
		// basefolder
		baseName = formatter.format(calcurrentDate.getTime());

		// genrate random file name
		//
		// fromNameList.add("Grace Palmer");
		// fromNameList.add("Jobs for You");
		// fromNameList.add("Job Alerts - Alumni Recruiter.com");
		// fromNameList.add("Job Alerts");

		// set bcc mail
		bccMailList = new ArrayList<String>();
		// if (SettingsClass.runOrignalTest) {
		// bccMailList.clear();
		// bccMailList.add("pawan@mailtesting.us");
		// bccMailList.add("gagan@mailtesting.us");
		// bccMailList.add("rajinder.signity@aol.com");
		// bccMailList.add("duamit@aol.com");

		// } else {
		bccMailList.clear();
		bccMailList.add("mangesh@mailtesting.us");
		bccMailList.add("gaurav@mailtesting.us");
		bccMailList.add("pawan@mailtesting.us");
		// bccMailList.add("jason@mailtesting.us");
		bccMailList.add("amit@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("raj@mailtesting.us");
		// }
	}

	public void sendEmail(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {

		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(MultiPartWriter.class);
		Client client = Client.create(cc);
		client.addFilter(new HTTPBasicAuthFilter("api", "key-b8f6462082ffa67a3a56352b14f91523"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + userDataObject.domainName + "/messages");
		// MultivaluedMapImpl formData = new MultivaluedMapImpl();
		webResource.setProperty("domain", "" + userDataObject.domainName + "");
		FormDataMultiPart form = new FormDataMultiPart();
		// List<String> dataList = composeClearfitEmail(userDataObject,
		// jobsArray);

		String html = "";

		// GroupObject groupObject =
		// Settings.groupObjectList.get(Settings.currentGroupCount.get());

		// ============================new code add
		// here===============================

		// String subjectLine_key = userDataObject.domainName.replace(" ",
		// "").trim() + "_" + userDataObject.compainId.replace(" ", "").trim();

		String subjectLine_key = userDataObject.child_process_key;

		ArrayList<GroupObject> groupList = SettingsClass.child_Grouplist_map.get(subjectLine_key);

		if (groupList == null) {
			groupList = SettingsClass.groupObjectList;

		}
		if (localGroupCount >= groupList.size())
			localGroupCount = 0;

		GroupObject groupObject = new GroupObject(groupList.get(localGroupCount).getGroupId(), groupList.get(localGroupCount).getSubject(), groupList.get(localGroupCount).getFromName());

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);

		// ========================================================================================

		// GroupObject groupObject = new
		// GroupObject(SettingsClass.groupObjectList.get(localGroupCount).getGroupId(),
		// SettingsClass.groupObjectList.get(localGroupCount).getSubject(),
		// SettingsClass.groupObjectList.get(localGroupCount).getFromName());

		// TemplateObject templateObject =
		// SettingsClass.templateObjectList.get(localTemplateCount);
		// TemplateObject templateObject =
		// SettingsClass.templateObjectList.get(4);

		// set city ,state or zipcode based on data availability for subject of
		// email
		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null")
				&& !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			userDataObject.locationString = " " + userDataObject.city + ", " + userDataObject.state;
		} else {
			userDataObject.locationString = " " + userDataObject.zipcode;
		}

		// replace both city and state if they both occur in subject line
		groupObject.setSubject(groupObject.getSubject().replace("CITY, STATE", userDataObject.locationString));

		// replace city if they only city in subject line
		if (userDataObject.city != null && !userDataObject.city.equalsIgnoreCase("") && !userDataObject.city.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.city));
		else
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.locationString));

		// replace state if they only state in subject line
		if (userDataObject.state != null && !userDataObject.state.equalsIgnoreCase("") && !userDataObject.state.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.state));
		else
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.locationString));

		// change radius ,job title , no.of jobs in subject line

		groupObject.setSubject(groupObject.getSubject().replace("RADIUS", " 30 Miles "));

		groupObject.setSubject(groupObject.getSubject().replace("JOB TITLE", userDataObject.getKeyword()));

		groupObject.setSubject(groupObject.getSubject().replace("NUMBER OF", String.valueOf(jobsList.size())));

		// replace first name on basis of it's presence
		if (userDataObject.getFirstName() != null && !userDataObject.getFirstName().toLowerCase().contains("null") && !userDataObject.getFirstName().equalsIgnoreCase(""))
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME", userDataObject.getFirstName()));
		else
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME ,", ""));

		groupObject.setSubject(groupObject.getSubject().replace("(current date)", SettingsClass.dateFormetForSubjectLine.format(new Date())));
		// if (SettingsClass.thanxGiving) {
		// groupObject.setFromName("David B.");
		// groupObject.setGroupId("50");
		// templateObject.setTemplateId(50);
		// groupObject.setSubject("Happy Thanksgiving From ClearFit Job Alerts");
		// html = thanksGivingTemplate(userDataObject);
		// }
		//
		// else

		// call different templates for email based on template id
		if (templateObject.getTemplateId() == 1) {

			if (MainWhiteLabelClass.isJobvitals)
				html = emailTemplateObject.jobvitalTemplate(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			else
				html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 2) {

			// html = emailTemplate2(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 3) {

			// html = emailTemplate3(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 4) {

			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 5) {

			// html = emailTemplate5(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 6) {

			// html = emailTemplate6(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 7) {

			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		}

		// html = emailTemplate3(userDataObject, jobsList,
		// String.valueOf(groupObject.getGroupId()),String.valueOf(templateObject.getTemplateId()));

		try {
			// increment template count

			// create key combination of group id and template id for hash map
			// to store counts of template

			// new code for subject line
			// stats=======================================
			String fileName = SettingsClass.child_subject_From_File_Names.get(subjectLine_key);
			String key = fileName + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();
			// =================================//==========================================

			// String key = groupObject.getGroupId() + "_" +
			// templateObject.getTemplateId();

			Integer templateCountObject = SettingsClass.templateCountHashMap.get(key);

			if (templateCountObject != null)
				SettingsClass.templateCountHashMap.put(key, templateCountObject + 1);
			else
				SettingsClass.templateCountHashMap.put(key, 1);

		} catch (Exception e) {
			// TODO: handle exception
		}

		// new code

		try {

			// increment template count
			// Settings.currentTemplateCount.getAndIncrement();

			// create key combination of group id and template id for hash map
			// to store counts of template

			// String tempKey = "";
			// if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
			// {
			// tempKey = "_" + userDataObject.compainId.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }

			String key = userDataObject.providerName + "_" + userDataObject.child_process_key + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();

			Integer templateCountObject = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);

			// System.out.println("seting map value" + key + "   " +
			// templateCountObject);
			if (templateCountObject != null)
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, templateCountObject + 1);
			else
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, 1);

		} catch (Exception e) {

		}

		String emailStr = "";

		// Construct an object to contain the recipient address.
		// Destination destination = new Destination();
		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				form.field("to", userDataObject.email);
				emailStr = userDataObject.email;

			} else {
				form.field("to", SettingsClass.testingEmailId);

				emailStr = SettingsClass.testingEmailId;
			}
		} else {

			form.field("to", userDataObject.email);
			emailStr = userDataObject.email;
			if (SettingsClass.firstEmailFlag) {

				for (int i = 0; i < bccMailList.size(); i++) {
					form.field("bcc", bccMailList.get(i));
				}

				System.out.println("bcc sending.....");
				SettingsClass.firstEmailFlag = false;

			}
		}

		if (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails) {

			SettingsClass.emailCount.set(0);
			SettingsClass.BccMails.getAndIncrement();

			System.out.println("bcc sending.....");
			for (int i = 0; i < bccMailList.size(); i++) {
				form.field("bcc", bccMailList.get(i));
			}

		}
		// String campaign_id = "";

		// if (SettingsClass.isWeekly)
		// campaign_id = "104";
		// else
		// FOR CLAR FIT..............
		// campaign_id = "106";

		JSONObject jObj = new JSONObject();

		try {
			jObj.put("group_id", groupObject.getGroupId());
			jObj.put("template_id", templateObject.getTemplateId());
			jObj.put("SENTDATE", SettingsClass.dateFormat.format(new Date()));
			jObj.put("REGDATE", userDataObject.registration_date);
			if (userDataObject.providerName.toLowerCase().contains("web"))
				jObj.put("IS_WEB_ALERTS", userDataObject.id);

			// THIS IS FOR EVENING ALERTS USERS
			if (MainWhiteLabelClass.parameterModelClassObject.isForEveningAlerts()) {
				jObj.put("EVENING_ALERTS", "y");
			}
		} catch (JSONException e3) {

			e3.printStackTrace();
		}
		form.field("from", groupObject.getFromName() + "<" + userDataObject.fromDomainName + ">");
		form.field("subject", groupObject.getSubject());

		form.field("v:my-custom-data", jObj.toString());// json value group_id
		form.field("o:campaign", userDataObject.compainId);
		form.field("html", html);
		// form.field("text", html);

		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		// new tags
		form.field("text", textHtml);
		form.field("stripped-html", html);
		form.field("body-html", html);
		form.field("body-plain", textHtml);
		form.field("content-type", MediaType.TEXT_HTML);

		// adding schedule send parameter in the mailgun only in live process
		if (!SettingsClass.isThisTestRun && !SettingsClass.runOrignalTest)
			form.field("o:deliverytime", userDataObject.scheduleSendForMailgun);

		try {
			SettingsClass.cal = Calendar.getInstance();
			if (SettingsClass.cal.get(Calendar.HOUR_OF_DAY) >= SettingsClass.exitTime && SettingsClass.cal.get(Calendar.MINUTE) >= 30) {
				SettingsClass.processesIsRunning = 1;
				SettingsClass.currentTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
				Utility.updateMemcacheStatsInEnd();
				Utility.killProcess();
			}
			ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, form);

			if (response.getStatus() == 200) {

				try {
					System.out.println("Email sent to " + emailStr + " via mailgun");
					SettingsClass.emailCount.incrementAndGet();
					SettingsClass.totalEmailCount.incrementAndGet();
					SettingsClass.newTotalEmailCount.incrementAndGet();

					if (!SettingsClass.isThisTestRun && MainWhiteLabelClass.parameterModelClassObject.isCylconSubProvider() && !MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
						Object duplicateUserEmailUPTo4MailSentObject = null;
						try {
							duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon");

						} catch (Exception e) {

							e.printStackTrace();
						}

						if (duplicateUserEmailUPTo4MailSentObject != null) {
							try {
								int count = Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString());
								OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, ++count);
							} catch (Exception e) {
							}
						} else {
							try {
								OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, 1);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					if (SettingsClass.isForMailtester) {
						// userDataObject.mail_tester_test_email =
						// userDataObject.getEmail();
						userDataObject.mail_tester_domain = userDataObject.domainName;
						userDataObject.mail_tester_api_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0] + "&format=json";
						userDataObject.mail_tester_debug_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0];
						MainWhiteLableMailtesterMain.mailtesterDomain_users_List.add(userDataObject);
						// MainWhiteLableMailtesterMain.mailtester_confirmation_map.put(userDataObject.domainName,
						// 1);
						// insertDataInTheMailtester_table(mailtester);

					}

					try {
						if (SettingsClass.isThisTestRun && !SettingsClass.isForMailtester) {
							if (SettingsClass.newTotalEmailCount.get() >= SettingsClass.numberOfTestingMails) {
								// Utility.updateMemcacheStatsInEnd();
								System.exit(0);
							}
						} else {
							SettingsClass.memcacheObj.set(
									userDataObject.email.replaceAll(" ", "") + "_" + userDataObject.keyword.replaceAll(" ", "") + "_" + userDataObject.zipcode.replaceAll(" ", "") + "_"
											+ SettingsClass.queuName, 0, String.valueOf("1")); //
						}
					} catch (Exception e1) {

						e1.printStackTrace();
					}
					try {
						SettingsClass.memcacheObj.set(SettingsClass.processCurrentExecutionTime, 0,
								String.valueOf(SettingsClass.cal.get(Calendar.HOUR_OF_DAY) + ":" + SettingsClass.cal.get(Calendar.MINUTE) + ":" + SettingsClass.cal.get(Calendar.SECOND)));
					} catch (Exception e1) {

						e1.printStackTrace();
					}

					try {
						SettingsClass.memcacheObj.set(SettingsClass.emailSend, 0, String.valueOf(SettingsClass.totalEmailCount)); //
					} catch (Exception e1) {

						e1.printStackTrace();
					}

					synchronized (SettingsClass.child_Process_stats_hashMap) {

						try {

							Integer childEmailSend = null;

							// String tempKey = "";
							// if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
							// {
							// tempKey = "_" +
							// userDataObject.sendGridCategory.replaceAll(" ",
							// "").trim();
							// } else if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
							// {
							// tempKey = "_" +
							// userDataObject.compainId.replaceAll(" ",
							// "").trim();
							// } else if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
							// {
							// tempKey = "_" +
							// userDataObject.sendGridCategory.replaceAll(" ",
							// "").trim();
							// }

							String childKeyStr = userDataObject.child_process_key + "_" + SettingsClass.emailSend;

							// for child domain emailSend processed count...

							try {

								childEmailSend = SettingsClass.child_Process_stats_hashMap.get(childKeyStr);
								if (childEmailSend == null) {
									childEmailSend = 0;

								}
							} catch (Exception e) {
								// TODO: handle exception
								childEmailSend = 0;
							}

							childEmailSend++;

							SettingsClass.memcacheObj.set(childKeyStr, 0, String.valueOf(childEmailSend));

							// setting it in the hash map

							SettingsClass.child_Process_stats_hashMap.put(childKeyStr, childEmailSend);

						} catch (Exception e) {
							// TODO: handle exception
						}
					}

				} catch (Exception ex) {
					System.out.println("The email was not sent.");
					System.out.println("Error message: " + ex.getMessage());
				}

			} else {

				// userDataObject.errorMessage = "Email Not Sent ===>" +
				// response.getStatus() + response.getResponseStatus();
				SettingsClass.userNotProcessedList.add(userDataObject);
				// System.out.println("Error in email sending" +
				// response.getStatus() + response.getResponseStatus());

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public void sendStartEndEmail(String startSendMsg, String end) {

		// ==============================mail
		// gn================================================
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(MultiPartWriter.class);
		Client client = Client.create(cc);
		client.addFilter(new HTTPBasicAuthFilter("api", "key-b8f6462082ffa67a3a56352b14f91523"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + MainWhiteLabelClass.parameterModelClassObject.getMailgunDomainName() + "/messages");
		// MultivaluedMapImpl formData = new MultivaluedMapImpl();
		webResource.setProperty("domain", MainWhiteLabelClass.parameterModelClassObject.getMailgunDomainName());
		FormDataMultiPart form = new FormDataMultiPart();

		String[] processStartsEndBccList = new String[5];

		processStartsEndBccList[0] = "mangesh@mailtesting.us";
		processStartsEndBccList[1] = "gaurav@mailtesting.us";
		processStartsEndBccList[2] = "pawan@mailtesting.us";
		processStartsEndBccList[3] = "amit@mailtesting.us";
		processStartsEndBccList[4] = "raj@mailtesting.us";

		// processStartsEndBccList[3] = "jason@mailtesting.us";
		// ===========================adding bcc to start and stop
		// mail==========================================
		for (int i = 0; i < processStartsEndBccList.length; i++) {
			form.field("bcc", processStartsEndBccList[i]);
		}

		form.field("to", "gagan@mailtesting.us");

		form.field("from", "<" + MainWhiteLabelClass.parameterModelClassObject.getFromEmailAddress() + ">");

		// ===========================adding bcc to start and stop
		// mail==========================================// sending mail from
		// email id..
		if (end.equalsIgnoreCase("settings")) {
			form.field("subject", "Api settings change in " + SettingsClass.dashboradFileName + " process");
		} else {
			form.field("subject", startSendMsg);

		}
		String bodyMessage = "";

		if (end.equalsIgnoreCase("stop")) {

			String name = SettingsClass.queuName;
			String startTime = new Date().toString();
			String endTime = new Date().toString();
			String totalUsers = "";
			String userProcessed = "";
			String mailSent = "";
			String perSecondMail = "";
			String cloudSearchPer = "";
			String mailCloudPer = "";
			String duplicateUsers = "";
			String perSecondUsers = "";
			String newJobsMails = "";
			String newJobPercentage = "";

			try {
				startTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processStartTime));
				if (startTime == null || startTime.equalsIgnoreCase("null")) {
					startTime = "";
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				endTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processCurrentExecutionTime));
				if (endTime == null || endTime.equalsIgnoreCase("null")) {
					endTime = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				totalUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.totalUserKey));
				if (totalUsers == null || totalUsers.equalsIgnoreCase("null")) {
					totalUsers = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				userProcessed = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.userProcessed));
				if (userProcessed == null || userProcessed.equalsIgnoreCase("null")) {
					userProcessed = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				mailSent = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.emailSend));
				if (mailSent == null || mailSent.equalsIgnoreCase("null")) {
					mailSent = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				perSecondUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondUsers));
				if (perSecondUsers == null || perSecondUsers.equalsIgnoreCase("null")) {
					perSecondUsers = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				perSecondMail = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondsMails));
				if (perSecondMail == null || perSecondMail.equalsIgnoreCase("null")) {
					perSecondMail = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				cloudSearchPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.cloudSearchUsersPer));
				if (cloudSearchPer == null || cloudSearchPer.equalsIgnoreCase("null")) {
					cloudSearchPer = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				mailCloudPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.mailCloudJobsPer));
				if (mailCloudPer == null || mailCloudPer.equalsIgnoreCase("null")) {
					mailCloudPer = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				duplicateUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.duplicateUserKey));
				if (duplicateUsers == null || duplicateUsers.equalsIgnoreCase("null")) {
					duplicateUsers = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				newJobPercentage = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.average_percentage_NewJobsMailsKey));
				if (newJobPercentage == null || newJobPercentage.equalsIgnoreCase("null")) {
					newJobPercentage = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				newJobsMails = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.numberofNewJobMailsKey));
				if (newJobsMails == null || newJobsMails.equalsIgnoreCase("null")) {
					newJobsMails = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				// perSecondUsers =
				// String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.per));

			} catch (Exception e) {

				e.printStackTrace();
			}
			String duplicateUserOakAndCylcon = "";
			try {
				duplicateUserOakAndCylcon = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey));
				if (duplicateUserOakAndCylcon == null || duplicateUserOakAndCylcon.equalsIgnoreCase("null")) {
					duplicateUserOakAndCylcon = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			String body = "<body>" + "<div ><h3 >" + name + "</h3><div>" + "<span>Total Users : </span>" + totalUsers + "<br><span>Total number of thread : </span>" + SettingsClass.TOTAL_N0_THREAD
					+ "<br><span>User Processed : </span>" + userProcessed + "<br><span>User Processed Per Second: </span>" + perSecondUsers + "<br><span>Duplicate Users : </span>" + duplicateUsers
					+ "<br><span>Mails Sent : </span>" + mailSent + "<br><span>Mails Per Second : </span>" + perSecondMail + "<br><span>Start Time : </span>" + startTime
					+ "<br><span>End Time : </span>" + endTime + "<br><span>Cloud Search Percentage : </span>" + cloudSearchPer + "<br><span>Mail Cloud Percentage : </span>" + mailCloudPer
					+ "<br><span>Number of mails with new jobs : </span>" + newJobsMails + "<br><span>Percentage of Number of mails with new jobs : </span>" + newJobPercentage
					+ "<br><span>Elastic cache duplicate : </span>" + duplicateUserOakAndCylcon + "</body></html>";

			bodyMessage = body;

		} else
			bodyMessage = startSendMsg;

		String htmlHead = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" + "Hi all  " + "<br/><br/>" + "Clearfit Process "
				+ startSendMsg;
		// + "has been started to process!") ;
		form.field("html", bodyMessage);

		try {
			ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, form);

			// SendGrid.Response response = sendgrid.send(email);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
