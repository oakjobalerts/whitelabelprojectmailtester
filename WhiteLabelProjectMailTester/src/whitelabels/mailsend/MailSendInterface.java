package whitelabels.mailsend;

import java.util.List;

import whitelabels.model.Jobs;
import whitelabels.model.UsersData;

public interface MailSendInterface {

	public void sendEmail (UsersData userDataObject , List<Jobs> jobsList , int localTemplateCount , int localGroupCount);

	public void sendStartEndEmail (String startSendMsg , String end);
}
