package whitelabels.mailsend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import net.spy.memcached.internal.OperationFuture;
import whitelabels.mainpackage.MainWhiteLabelClass;
import whitelabels.mainpackage.MainWhiteLableMailtesterMain;
import whitelabels.mainpackage.SettingsClass;
import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

import com.sendgrid.SendGrid;

public class SendGridClass implements MailSendInterface {

	GregorianCalendar calendar;

	static ArrayList<String> fromNameList = new ArrayList<String>();
	Calendar calcurrentDate;
	SimpleDateFormat formatter;
	String baseName = "";
	public List<String> bccMailList;
	EmailTemplates emailTemplateObject = null;

	public SendGridClass() {

		emailTemplateObject = new EmailTemplates();

		calcurrentDate = Calendar.getInstance();
		formatter = new SimpleDateFormat("yyyyMMdd");
		// basefolder
		baseName = formatter.format(calcurrentDate.getTime());

		// genrate random file name

		// fromNameList.add("Grace Palmer");
		// fromNameList.add("Jobs for You");
		// fromNameList.add("Job Alerts - Alumni Recruiter.com");
		// fromNameList.add("Job Alerts");

		// set bcc mail
		bccMailList = new ArrayList<String>();
		// if (SettingsClass.runOrignalTest) {
		// bccMailList.clear();
		// bccMailList.add("pawan@mailtesting.us");
		// bccMailList.add("gagan@mailtesting.us");
		// bccMailList.add("rajinder.signity@aol.com");
		// bccMailList.add("duamit@aol.com");

		// } else {
		bccMailList.clear();
		bccMailList.add("mangesh@mailtesting.us");
		bccMailList.add("gaurav@mailtesting.us");
		bccMailList.add("pawan@mailtesting.us");
		bccMailList.add("jason@mailtesting.us");
		bccMailList.add("amit@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("raj@mailtesting.us");
		// }
	}

	public void sendEmail(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {

		// List<String> dataList = composeEmailFormat(userDataObject, jobsList);
		// create global sendgrid obj

		String html = "";

		// GroupObject groupObject =
		// Settings.groupObjectList.get(Settings.currentGroupCount.get());

		// String subjectLine_key = userDataObject.domainName.replace(" ",
		// "").trim() + "_" + userDataObject.sendGridCategory.replace(" ",
		// "").trim();
		String subjectLine_key = userDataObject.child_process_key;

		ArrayList<GroupObject> groupList = SettingsClass.child_Grouplist_map.get(subjectLine_key);

		if (groupList == null) {
			groupList = SettingsClass.groupObjectList;

		}

		if (localGroupCount >= groupList.size())
			localGroupCount = 0;

		GroupObject groupObject = new GroupObject(groupList.get(localGroupCount).getGroupId(), groupList.get(localGroupCount).getSubject(), groupList.get(localGroupCount).getFromName());

		// GroupObject groupObject = new
		// GroupObject(SettingsClass.groupObjectList.get(localGroupCount).getGroupId(),
		// SettingsClass.groupObjectList.get(localGroupCount).getSubject(),
		// SettingsClass.groupObjectList.get(localGroupCount).getFromName());

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		// TemplateObject templateObject =
		// SettingsClass.templateObjectList.get(4);

		// set city ,state or zipcode based on data availability for subject
		// of
		// email
		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null")
				&& !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			userDataObject.locationString = userDataObject.city + ", " + userDataObject.state;
		} else {
			userDataObject.locationString = userDataObject.zipcode;
		}

		// replace both city and state if they both occur in subject line
		groupObject.setSubject(groupObject.getSubject().replace("CITY, STATE", userDataObject.locationString));

		// replace city if they only city in subject line
		if (userDataObject.city != null && !userDataObject.city.equalsIgnoreCase("") && !userDataObject.city.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.city));
		else
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.locationString));

		// replace state if they only state in subject line
		if (userDataObject.state != null && !userDataObject.state.equalsIgnoreCase("") && !userDataObject.state.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.state));
		else
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.locationString));

		// change radius ,job title , no.of jobs in subject line

		groupObject.setSubject(groupObject.getSubject().replace("RADIUS", " 30 Miles "));

		groupObject.setSubject(groupObject.getSubject().replace("JOB TITLE", userDataObject.getKeyword()));

		groupObject.setSubject(groupObject.getSubject().replace("NUMBER OF", String.valueOf(jobsList.size())));

		// replace first name on basis of it's presence
		if (userDataObject.getFirstName() != null && !userDataObject.getFirstName().toLowerCase().contains("null") && !userDataObject.getFirstName().equalsIgnoreCase(""))
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME", userDataObject.getFirstName()));
		else
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME ,", ""));

		// call different templates for email based on template id
		groupObject.setSubject(groupObject.getSubject().replace("(current date)", SettingsClass.dateFormetForSubjectLine.format(new Date())));

		if (templateObject.getTemplateId() == 1) {

			html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 2) {

		} else if (templateObject.getTemplateId() == 3) {

			// html = emailTemplate3(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 4) {

			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 5) {

			// html = emailTemplate5(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 6) {

			// html = emailTemplate6(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 7) {

			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		}

		// html = emailTemplate3(userDataObject, jobsList,
		// String.valueOf(groupObject.getGroupId()),String.valueOf(templateObject.getTemplateId()));

		try {
			// increment template count

			// create key combination of group id and template id for hash
			// map
			// to store counts of template
			// new code for subject line
			// stats=======================================
			String fileName = SettingsClass.child_subject_From_File_Names.get(subjectLine_key);
			String key = fileName + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();
			// =================================//==========================================

			// String key = groupObject.getGroupId() + "_" +
			// templateObject.getTemplateId();
			Integer templateCountObject = SettingsClass.templateCountHashMap.get(key);

			if (templateCountObject != null)
				SettingsClass.templateCountHashMap.put(key, templateCountObject + 1);
			else
				SettingsClass.templateCountHashMap.put(key, 1);

		} catch (Exception e) {
			// TODO: handle exception
		}

		// new code

		try {

			// increment template count
			// Settings.currentTemplateCount.getAndIncrement();

			// create key combination of group id and template id for hash map
			// to store counts of template

			// String tempKey = "";
			// if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
			// {
			// tempKey = "_" + userDataObject.compainId.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }

			String key = userDataObject.providerName + "_" + userDataObject.child_process_key + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();

			Integer templateCountObject = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);

			// System.out.println("seting map value" + key + "   " +
			// templateCountObject);
			if (templateCountObject != null)
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, templateCountObject + 1);
			else
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, 1);

		} catch (Exception e) {

		}

		SendGrid sendgrid = new SendGrid(userDataObject.sendGridUserName, userDataObject.sendGridUserPassword);
		SendGrid.Email email = new SendGrid.Email();

		email.setFrom(userDataObject.fromDomainName);
		try {
			email.addUniqueArg("GROUPID", groupObject.getGroupId());
			email.addUniqueArg("TEMPLATEID", String.valueOf(templateObject.getTemplateId()));
			email.addUniqueArg("SENTDATE", String.valueOf(SettingsClass.dateFormat.format(new Date())));
			email.addUniqueArg("REGDATE", String.valueOf(userDataObject.registration_date));
			email.addCategory(userDataObject.sendGridCategory);
			email.addUniqueArg("DOMAIN", userDataObject.domainName);

			if (userDataObject.providerName.toLowerCase().contains("web"))
				email.addUniqueArg("IS_WEB_ALERTS", userDataObject.getId());

			if (MainWhiteLabelClass.parameterModelClassObject.isForEveningAlerts()) {
				email.addUniqueArg("EVENING_ALERTS", "y");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		email.setSubject(groupObject.getSubject());
		email.setFromName(groupObject.getFromName());

		email.setHtml(html);

		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		email.setText(textHtml);

		String emailStr = "";

		// sending mail to email id..
		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				email.addTo(userDataObject.email);
				emailStr = userDataObject.email;
			} else {
				email.addTo(SettingsClass.testingEmailId);
				emailStr = SettingsClass.testingEmailId;
			}
		} else {

			// real user processing..

			// user email id..//set sender mail id...for to

			long scheduledTime = 0;
			try {
				String time = userDataObject.openTime;

				if (!time.equalsIgnoreCase(""))
					scheduledTime = Long.parseLong(time);
			} catch (Exception e) {
			}

			// adding schedule send parameter to the sendrid oakkobject
			if (!SettingsClass.isThisTestRun && !SettingsClass.runOrignalTest)
				email.setSendAt((int) (scheduledTime));

			email.addTo(userDataObject.getEmail());
			emailStr = userDataObject.getEmail();
			// sending mail subject..

			// send bcc mail for real user...that will be call only one..
			if (SettingsClass.firstEmailFlag) {
				String[] bccArray = new String[bccMailList.size()];

				System.out.println("bcc sending.....");

				for (int i = 0; i < bccMailList.size(); i++) {
					bccArray[i] = bccMailList.get(i);
				}

				email.setBcc(bccArray);

				SettingsClass.firstEmailFlag = false;

			}

		}

		// bcc mail for real user..now check for 50,000 && max bcc mail sent
		// will be 100

		if (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails) {

			String[] bccArray = new String[bccMailList.size()];

			for (int i = 0; i < bccMailList.size(); i++) {
				bccArray[i] = bccMailList.get(i);
			}
			System.out.println("bcc sending.....");
			email.setBcc(bccArray);

			SettingsClass.BccMails.getAndIncrement();

			SettingsClass.emailCount.set(0);
		}

		try {

			// sending mail...
			SendGrid.Response response = sendgrid.send(email);
			// System.out.println("response" + response.getStatus()+"");

			sendgrid = null;
			email = null;

			if (response.getStatus()) {

				// Adding the user email id to memcache to avoid sending
				// duplicate
				// email and set value to check email has sent or not
				// 0 for not sent and 1 for sent mail
				try {

					SettingsClass.memcacheObj.set(userDataObject.email.replaceAll(" ", "") + "_" + userDataObject.keyword.replaceAll(" ", "") + "_" + userDataObject.zipcode.replaceAll(" ", "") + "_"
							+ SettingsClass.queuName, 0, String.valueOf("1"));
					// SettingsClass.memcacheObj.set("sandeepk@signitysolutions.com",
					// 0, "1");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {

					System.out.println("Email sent to " + emailStr + " via sendgrid");
					SettingsClass.emailCount.incrementAndGet();
					SettingsClass.totalEmailCount.getAndIncrement();
					SettingsClass.newTotalEmailCount.getAndIncrement();

					if (!SettingsClass.isThisTestRun && MainWhiteLabelClass.parameterModelClassObject.isCylconSubProvider() && !MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
						Object duplicateUserEmailUPTo4MailSentObject = null;
						try {
							duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon");

						} catch (Exception e) {

							e.printStackTrace();
						}

						if (duplicateUserEmailUPTo4MailSentObject != null) {
							try {
								int count = Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString());
								OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, ++count);
							} catch (Exception e) {
							}
						} else {
							try {
								OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, 1);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					if (SettingsClass.isThisTestRun && !SettingsClass.isForMailtester) {
						if (SettingsClass.newTotalEmailCount.get() >= SettingsClass.numberOfTestingMails) {
							// Utility.updateMemcacheStatsInEnd();
							System.exit(0);
						}
					}

					SettingsClass.cal = Calendar.getInstance();

					try {
						SettingsClass.memcacheObj.set(SettingsClass.processCurrentExecutionTime, 0,
								String.valueOf(SettingsClass.cal.get(Calendar.HOUR_OF_DAY) + ":" + SettingsClass.cal.get(Calendar.MINUTE) + ":" + SettingsClass.cal.get(Calendar.SECOND)));
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					try {
						SettingsClass.memcacheObj.set(SettingsClass.emailSend, 0, String.valueOf(SettingsClass.totalEmailCount));
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					synchronized (SettingsClass.child_Process_stats_hashMap) {

						try {

							Integer childEmailSend = null;

							// String tempKey = "";
							// if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
							// {
							// tempKey = "_" +
							// userDataObject.sendGridCategory.replaceAll(" ",
							// "").trim();
							// }
							// else if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
							// {
							// tempKey = "_" +
							// userDataObject.compainId.replaceAll(" ",
							// "").trim();
							// }
							// else if
							// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
							// {
							// tempKey = "_" +
							// userDataObject.sendGridCategory.replaceAll(" ",
							// "").trim();
							// }

							String childKeyStr = userDataObject.child_process_key + "_" + SettingsClass.emailSend;
							// for child domain emailSend processed count...

							// for child domain emailSend processed count...

							try {

								childEmailSend = SettingsClass.child_Process_stats_hashMap.get(childKeyStr);
								if (childEmailSend == null) {
									childEmailSend = 0;

								}
							} catch (Exception e) {
								// TODO: handle exception
								childEmailSend = 0;
							}

							childEmailSend++;

							SettingsClass.memcacheObj.set(childKeyStr, 0, String.valueOf(childEmailSend));

							// setting it in the hash map

							SettingsClass.child_Process_stats_hashMap.put(childKeyStr, childEmailSend);

						} catch (Exception e) {
							// TODO: handle exception
						}
					}

				} catch (Exception ex) {

					System.out.println("Error message: " + ex.getMessage());
				}

				if (SettingsClass.isForMailtester) {
					// userDataObject.mail_tester_test_email =
					// userDataObject.getEmail();
					userDataObject.mail_tester_domain = userDataObject.domainName;
					userDataObject.mail_tester_api_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0] + "&format=json";
					userDataObject.mail_tester_debug_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0];
					MainWhiteLableMailtesterMain.mailtesterDomain_users_List.add(userDataObject);
					// MainWhiteLableMailtesterMain.mailtester_confirmation_map.put(userDataObject.domainName,
					// 1);
					// insertDataInTheMailtester_table(mailtester);

				}

			} else {

				System.out.println("The email was not sent.");
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void sendStartEndEmail(String startSendMsg, String end) {

		String[] processStartsEndBccList = new String[5];

		processStartsEndBccList[0] = "mangesh@mailtesting.us";
		processStartsEndBccList[1] = "gaurav@mailtesting.us";
		processStartsEndBccList[2] = "pawan@mailtesting.us";
		processStartsEndBccList[3] = "amit@mailtesting.us";
		processStartsEndBccList[4] = "raj@mailtesting.us";

		// processStartsEndBccList[3] = "jason@mailtesting.us";
		SendGrid sendgrid;
		SendGrid.Email email;

		// create global sendgrid obj
		sendgrid = new SendGrid(MainWhiteLabelClass.parameterModelClassObject.getSendgridUsername(), MainWhiteLabelClass.parameterModelClassObject.getSendgridPassword());

		email = new SendGrid.Email();

		if (end.equalsIgnoreCase("stopcodeapi")) {
			email.addTo("gagan@mailtesting.us");
			email.setBcc(processStartsEndBccList);
		} else {
			email.addTo("gagan@mailtesting.us");
			email.setBcc(processStartsEndBccList);
		}

		email.setFrom(MainWhiteLabelClass.parameterModelClassObject.getFromEmailAddress());

		if (end.equalsIgnoreCase("settings"))
			email.setSubject("Api settings change in " + SettingsClass.dashboradFileName + " process");
		else
			email.setSubject(startSendMsg);

		String bodyMessage = "";

		if (end.equalsIgnoreCase("stop")) {

			String name = SettingsClass.queuName;
			String startTime = new Date().toString();
			String endTime = new Date().toString();
			String totalUsers = "";
			String userProcessed = "";
			String mailSent = "";
			String perSecondMail = "";
			String cloudSearchPer = "";
			String mailCloudPer = "";
			String duplicateUsers = "";
			String perSecondUsers = "";
			String newJobsMails = "";
			String newJobPercentage = "";

			try {
				startTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processStartTime));
				if (startTime == null || startTime.equalsIgnoreCase("null")) {
					startTime = "";
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				endTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processCurrentExecutionTime));
				if (endTime == null || endTime.equalsIgnoreCase("null")) {
					endTime = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				totalUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.totalUserKey));
				if (totalUsers == null || totalUsers.equalsIgnoreCase("null")) {
					totalUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				userProcessed = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.userProcessed));
				if (userProcessed == null || userProcessed.equalsIgnoreCase("null")) {
					userProcessed = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mailSent = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.emailSend));
				if (mailSent == null || mailSent.equalsIgnoreCase("null")) {
					mailSent = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				perSecondUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondUsers));
				if (perSecondUsers == null || perSecondUsers.equalsIgnoreCase("null")) {
					perSecondUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				perSecondMail = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondsMails));
				if (perSecondMail == null || perSecondMail.equalsIgnoreCase("null")) {
					perSecondMail = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				cloudSearchPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.cloudSearchUsersPer));
				if (cloudSearchPer == null || cloudSearchPer.equalsIgnoreCase("null")) {
					cloudSearchPer = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mailCloudPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.mailCloudJobsPer));
				if (mailCloudPer == null || mailCloudPer.equalsIgnoreCase("null")) {
					mailCloudPer = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				duplicateUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.duplicateUserKey));
				if (duplicateUsers == null || duplicateUsers.equalsIgnoreCase("null")) {
					duplicateUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				newJobPercentage = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.average_percentage_NewJobsMailsKey));
				if (newJobPercentage == null || newJobPercentage.equalsIgnoreCase("null")) {
					newJobPercentage = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				newJobsMails = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.numberofNewJobMailsKey));
				if (newJobsMails == null || newJobsMails.equalsIgnoreCase("null")) {
					newJobsMails = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				// perSecondUsers =
				// String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.per));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String duplicateUserOakAndCylcon = "";
			try {
				duplicateUserOakAndCylcon = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey));
				if (duplicateUserOakAndCylcon == null || duplicateUserOakAndCylcon.equalsIgnoreCase("null")) {
					duplicateUserOakAndCylcon = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			String body = "<body>" + "<div ><h3 >" + name + "</h3><div>" + "<span>Total Users : </span>" + totalUsers + "<br><span>User Processed : </span>" + userProcessed
					+ "<br><span>Total number of thread : </span>" + SettingsClass.TOTAL_N0_THREAD + "<br><span>User Processed Per Second: </span>" + perSecondUsers
					+ "<br><span>Duplicate Users : </span>" + duplicateUsers + "<br><span>Mails Sent : </span>" + mailSent + "<br><span>Mails Per Second : </span>" + perSecondMail
					+ "<br><span>Start Time : </span>" + startTime + "<br><span>End Time : </span>" + endTime + "<br><span>Cloud Search Percentage : </span>" + cloudSearchPer
					+ "<br><span>Mail Cloud Percentage : </span>" + mailCloudPer + "<br><span>Number of mails with new jobs : </span>" + newJobsMails
					+ "<br><span>Percentage of Number of mails with new jobs : </span>" + newJobPercentage + "<br><span>Elastic cache duplicate : </span>" + duplicateUserOakAndCylcon
					+ "</body></html>";

			bodyMessage = body;

		} else
			bodyMessage = startSendMsg;

		email.setHtml("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" + "Hi all  " + "<br/>" + bodyMessage);
		// + "has been started to process!") ;

		try {
			SendGrid.Response response = sendgrid.send(email);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
