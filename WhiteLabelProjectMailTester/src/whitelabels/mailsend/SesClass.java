package whitelabels.mailsend;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import net.spy.memcached.internal.OperationFuture;
import whitelabels.mainpackage.MainWhiteLabelClass;
import whitelabels.mainpackage.MainWhiteLableMailtesterMain;
import whitelabels.mainpackage.SettingsClass;
import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

public class SesClass implements MailSendInterface {

	GregorianCalendar calendar;

	static ArrayList<String> fromNameList = new ArrayList<String>();
	Calendar calcurrentDate;
	SimpleDateFormat formatter;
	String baseName = "";
	public List<String> bccMailList;
	EmailTemplates emailTemplateObject = null;

	private BasicAWSCredentials awsCredentialsForSES;

	private AmazonSimpleEmailServiceClient emailSendClient;
	static String awsAccessKeyForSES = "AKIAJB4W4NEOUTCHYQQA";
	static String awsSecretKeyForSES = "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";

	public SesClass() {

		awsCredentialsForSES = new BasicAWSCredentials(awsAccessKeyForSES, awsSecretKeyForSES);

		emailSendClient = new AmazonSimpleEmailServiceClient(awsCredentialsForSES);
		emailSendClient.setRegion(Region.getRegion(Regions.US_WEST_2));

		emailTemplateObject = new EmailTemplates();

		calcurrentDate = Calendar.getInstance();
		formatter = new SimpleDateFormat("yyyyMMdd");
		// basefolder
		baseName = formatter.format(calcurrentDate.getTime());

		// genrate random file name

		fromNameList.add("Grace Palmer");
		fromNameList.add("Jobs for You");
		fromNameList.add("Job Alerts - Alumni Recruiter.com");
		fromNameList.add("Job Alerts");

		// set bcc mail
		bccMailList = new ArrayList<String>();
		// if (SettingsClass.runOrignalTest) {
		bccMailList.clear();
		bccMailList.add("pawan@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("rajinder.signity@aol.com");
		bccMailList.add("duamit@aol.com");

		// } else {
		bccMailList.clear();
		bccMailList.add("mangesh@mailtesting.us");
		bccMailList.add("gaurav@mailtesting.us");
		bccMailList.add("pawan@mailtesting.us");
		bccMailList.add("jason@mailtesting.us");
		bccMailList.add("amit@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("raj@mailtesting.us");
		// }
	}

	public void sendEmail(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {
		SendEmailRequest request;
		// List<String> dataList = composePapaerRoseEmail(userDataObject,
		// jobsArray);
		// Construct an object to contain the recipient address.
		String html = "";

		// GroupObject groupObject =
		// Settings.groupObjectList.get(Settings.currentGroupCount.get());

		// String subjectLine_key = userDataObject.domainName.replace(" ",
		// "").trim() + "_" + userDataObject.sendGridCategory.replace(" ",
		// "").trim();

		String subjectLine_key = userDataObject.child_process_key;

		ArrayList<GroupObject> groupList = SettingsClass.child_Grouplist_map.get(subjectLine_key);

		if (groupList == null) {
			groupList = SettingsClass.groupObjectList;

		}

		if (localGroupCount >= groupList.size())
			localGroupCount = 0;

		GroupObject groupObject = new GroupObject(groupList.get(localGroupCount).getGroupId(), groupList.get(localGroupCount).getSubject(), groupList.get(localGroupCount).getFromName());

		// GroupObject groupObject = new
		// GroupObject(SettingsClass.groupObjectList.get(localGroupCount).getGroupId(),
		// SettingsClass.groupObjectList.get(localGroupCount).getSubject(),
		// SettingsClass.groupObjectList.get(localGroupCount).getFromName());

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		// TemplateObject templateObject =
		// SettingsClass.templateObjectList.get(4);

		// set city ,state or zipcode based on data availability for subject of
		// email
		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null")
				&& !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			userDataObject.locationString = userDataObject.city + ", " + userDataObject.state;
		} else {
			userDataObject.locationString = userDataObject.zipcode;
		}

		// replace both city and state if they both occur in subject line
		groupObject.setSubject(groupObject.getSubject().replace("CITY, STATE", userDataObject.locationString));

		// replace city if they only city in subject line
		if (userDataObject.city != null && !userDataObject.city.equalsIgnoreCase("") && !userDataObject.city.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.city));
		else
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.locationString));

		// replace state if they only state in subject line
		if (userDataObject.state != null && !userDataObject.state.equalsIgnoreCase("") && !userDataObject.state.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.state));
		else
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.locationString));

		// change radius ,job title , no.of jobs in subject line

		groupObject.setSubject(groupObject.getSubject().replace("RADIUS", " 30 Miles "));

		groupObject.setSubject(groupObject.getSubject().replace("JOB TITLE", userDataObject.getKeyword()));

		groupObject.setSubject(groupObject.getSubject().replace("NUMBER OF", String.valueOf(jobsList.size())));

		// replace first name on basis of it's presence
		if (userDataObject.getFirstName() != null && !userDataObject.getFirstName().toLowerCase().contains("null") && !userDataObject.getFirstName().equalsIgnoreCase(""))
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME", userDataObject.getFirstName()));
		else
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME ,", ""));

		groupObject.setSubject(groupObject.getSubject().replace("(current date)", SettingsClass.dateFormetForSubjectLine.format(new Date())));

		if (templateObject.getTemplateId() == 1) {

			if (MainWhiteLabelClass.isPapaerrose)
				html = emailTemplateObject.paperroseTemplate(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			else
				html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			// System.out.println("Template 1 ==========" + html);

		} else if (templateObject.getTemplateId() == 2) {

			// html = emailTemplate2(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 3) {

			// html = emailTemplate3(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 4) {

			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			// System.out.println("Template 4 ==========" + html);
		} else if (templateObject.getTemplateId() == 5) {

			// html = emailTemplate5(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 6) {

			// html = emailTemplate6(userDataObject, jobsList,
			// String.valueOf(groupObject.getGroupId()),
			// String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 7) {

			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		}

		// html = emailTemplate3(userDataObject, jobsList,
		// String.valueOf(groupObject.getGroupId()),String.valueOf(templateObject.getTemplateId()));

		try {
			// increment template count

			// create key combination of group id and template id for hash map
			// to store counts of template
			// stats=======================================
			String fileName = SettingsClass.child_subject_From_File_Names.get(subjectLine_key);
			String key = fileName + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();
			// =================================//==========================================

			// String key = groupObject.getGroupId() + "_" +
			// templateObject.getTemplateId();
			Integer templateCountObject = SettingsClass.templateCountHashMap.get(key);

			if (templateCountObject != null)
				SettingsClass.templateCountHashMap.put(key, templateCountObject + 1);
			else
				SettingsClass.templateCountHashMap.put(key, 1);

		} catch (Exception e) {
			// TODO: handle exception
		}

		// new code

		try {

			// increment template count
			// Settings.currentTemplateCount.getAndIncrement();

			// create key combination of group id and template id for hash map
			// to store counts of template
			// String tempKey = "";
			// if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
			// {
			// tempKey = "_" + userDataObject.compainId.replaceAll(" ",
			// "_").trim();
			// }
			// else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replaceAll(" ",
			// "_").trim();
			// }

			String key = userDataObject.providerName + "_" + userDataObject.child_process_key + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();

			Integer templateCountObject = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);

			// System.out.println("seting map value" + key + "   " +
			// templateCountObject);
			if (templateCountObject != null)
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, templateCountObject + 1);
			else
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, 1);

		} catch (Exception e) {

		}
		String emaiStr = "";

		Destination destination = new Destination();
		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				emaiStr = userDataObject.email;

				destination.withToAddresses(new String[] { userDataObject.email });
			} else {
				emaiStr = SettingsClass.testingEmailId;
				destination.withToAddresses(new String[] { SettingsClass.testingEmailId });
			}
		} else {
			emaiStr = userDataObject.email;
			destination.withToAddresses(new String[] { userDataObject.email });

			if (SettingsClass.firstEmailFlag) {
				destination.withBccAddresses(bccMailList);
				System.out.println("bcc sending....");
				SettingsClass.firstEmailFlag = false;
			}
		}

		if (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails) {
			destination.withBccAddresses(bccMailList);
			System.out.println("bcc sending....");
			SettingsClass.emailCount.set(0);
			SettingsClass.BccMails.getAndIncrement();
		}
		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		Content subject = new Content().withData(groupObject.getSubject());
		Content htmlbody = new Content().withData(html);
		Body body = new Body().withHtml(htmlbody);

		Content textHtmlBody = new Content().withData(textHtml);
		body.withText(textHtmlBody);

		// body.withText(htmlbody);

		// Create a message with the specified subject and body.
		com.amazonaws.services.simpleemail.model.Message message = new com.amazonaws.services.simpleemail.model.Message(subject, body);
		request = new SendEmailRequest(groupObject.getFromName() + "<" + userDataObject.fromDomainName + "> ", destination, message);

		try {
			emailSendClient.sendEmail(request);
			System.out.println("Email sent to " + emaiStr + " via Ses");

			SettingsClass.emailCount.incrementAndGet();
			SettingsClass.totalEmailCount.getAndIncrement();
			SettingsClass.newTotalEmailCount.getAndIncrement();

			if (!SettingsClass.isThisTestRun && MainWhiteLabelClass.parameterModelClassObject.isCylconSubProvider() && !MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
				Object duplicateUserEmailUPTo4MailSentObject = null;
				try {
					duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon");

				} catch (Exception e) {

					e.printStackTrace();
				}

				if (duplicateUserEmailUPTo4MailSentObject != null) {
					try {
						int count = Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString());
						OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, ++count);
					} catch (Exception e) {
					}
				} else {
					try {
						OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, 1);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (SettingsClass.isForMailtester) {
				userDataObject.mail_tester_domain = userDataObject.domainName;
				userDataObject.mail_tester_api_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0] + "&format=json";
				userDataObject.mail_tester_debug_link = "https://www.mail-tester.com/" + userDataObject.getEmail().split("@")[0];
				MainWhiteLableMailtesterMain.mailtesterDomain_users_List.add(userDataObject);

			}

			if (SettingsClass.isThisTestRun && !SettingsClass.isForMailtester) {
				if (SettingsClass.newTotalEmailCount.get() >= SettingsClass.numberOfTestingMails) {
					// Utility.updateMemcacheStatsInEnd();
					System.exit(0);
				}

			} else {
				try {
					SettingsClass.memcacheObj.set(userDataObject.email.replaceAll(" ", "") + "_" + userDataObject.keyword.replaceAll(" ", "") + "_" + userDataObject.zipcode.replaceAll(" ", "") + "_"
							+ SettingsClass.queuName, 0, String.valueOf("1")); //
				} catch (Exception e1) {

					e1.printStackTrace();
				}
			}
			try {
				SettingsClass.memcacheObj.set(SettingsClass.processCurrentExecutionTime, 0,
						String.valueOf(SettingsClass.cal.get(Calendar.HOUR_OF_DAY) + ":" + SettingsClass.cal.get(Calendar.MINUTE) + ":" + SettingsClass.cal.get(Calendar.SECOND)));
			} catch (Exception e1) {

				e1.printStackTrace();
			}

			try {
				SettingsClass.memcacheObj.set(SettingsClass.emailSend, 0, String.valueOf(SettingsClass.totalEmailCount)); //
			} catch (Exception e1) {

				e1.printStackTrace();
			}

			synchronized (SettingsClass.child_Process_stats_hashMap) {

				try {

					Integer childEmailSend = null;

					// String tempKey = "";
					// if
					// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
					// {
					// tempKey = "_" +
					// userDataObject.sendGridCategory.replaceAll(" ",
					// "").trim();
					// }
					// else if
					// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
					// {
					// tempKey = "_" + userDataObject.compainId.replaceAll(" ",
					// "").trim();
					// }
					// else if
					// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
					// {
					// tempKey = "_" +
					// userDataObject.sendGridCategory.replaceAll(" ",
					// "").trim();
					// }

					String childKeyStr = userDataObject.child_process_key + "_" + SettingsClass.emailSend;
					// for child domain emailSend processed count...
					// for child domain emailSend processed count...

					try {

						childEmailSend = SettingsClass.child_Process_stats_hashMap.get(childKeyStr);
						if (childEmailSend == null) {
							childEmailSend = 0;

						}
					} catch (Exception e) {
						// TODO: handle exception
						childEmailSend = 0;
					}

					childEmailSend++;

					SettingsClass.memcacheObj.set(childKeyStr, 0, String.valueOf(childEmailSend));

					// setting it in the hash map

					SettingsClass.child_Process_stats_hashMap.put(childKeyStr, childEmailSend);

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			subject = null;
			htmlbody = null;
			body = null;
			request = null;

		} catch (Exception ex) {
			System.out.println("The email was not sent. for id=" + userDataObject.email);
			System.out.println("Error message: " + ex.getMessage());
			try {
				if (ex.getMessage().contains("Daily message quota exceeded")) {
					Thread.sleep(1800000);
				} else if (ex.getMessage().contains("Maximum sending rate exceeded")) {
					Thread.sleep(1800000);
				}
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

	}

	@Override
	public void sendStartEndEmail(String startSendMsg, String end) {

		ArrayList<String> processEndBccList = new ArrayList<String>();

		processEndBccList.add("mangesh@mailtesting.us");
		processEndBccList.add("gaurav@mailtesting.us");
		processEndBccList.add("pawan@mailtesting.us");
		processEndBccList.add("amit@mailtesting.us");
		processEndBccList.add("raj@mailtesting.us");

		// processEndBccList.add("jason@mailtesting.us");
		SendEmailRequest request;
		Destination destination = new Destination();
		destination.withBccAddresses(processEndBccList);
		destination.withToAddresses(new String[] { "gagan@mailtesting.us" });

		String sub = "";
		if (end.equalsIgnoreCase("settings"))
			sub = "Api settings change in " + SettingsClass.dashboradFileName + " process";
		else
			sub = startSendMsg;
		Content subject = new Content().withData(sub);

		// =====================================================
		String bodyMessage = "";

		if (end.equalsIgnoreCase("stop")) {

			String name = SettingsClass.queuName;
			String startTime = new Date().toString();
			String endTime = new Date().toString();
			String totalUsers = "";
			String userProcessed = "";
			String mailSent = "";
			String perSecondMail = "";
			String cloudSearchPer = "";
			String mailCloudPer = "";
			String duplicateUsers = "";
			String perSecondUsers = "";
			String newJobsMails = "";
			String newJobPercentage = "";

			try {
				startTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processStartTime));
				if (startTime == null || startTime.equalsIgnoreCase("null")) {
					startTime = "";
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				endTime = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.processCurrentExecutionTime));
				if (endTime == null || endTime.equalsIgnoreCase("null")) {
					endTime = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				totalUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.totalUserKey));
				if (totalUsers == null || totalUsers.equalsIgnoreCase("null")) {
					totalUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				userProcessed = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.userProcessed));
				if (userProcessed == null || userProcessed.equalsIgnoreCase("null")) {
					userProcessed = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mailSent = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.emailSend));
				if (mailSent == null || mailSent.equalsIgnoreCase("null")) {
					mailSent = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				perSecondUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondUsers));
				if (perSecondUsers == null || perSecondUsers.equalsIgnoreCase("null")) {
					perSecondUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				perSecondMail = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.perSecondsMails));
				if (perSecondMail == null || perSecondMail.equalsIgnoreCase("null")) {
					perSecondMail = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				cloudSearchPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.cloudSearchUsersPer));
				if (cloudSearchPer == null || cloudSearchPer.equalsIgnoreCase("null")) {
					cloudSearchPer = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mailCloudPer = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.mailCloudJobsPer));
				if (mailCloudPer == null || mailCloudPer.equalsIgnoreCase("null")) {
					mailCloudPer = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				duplicateUsers = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.duplicateUserKey));
				if (duplicateUsers == null || duplicateUsers.equalsIgnoreCase("null")) {
					duplicateUsers = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				newJobPercentage = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.average_percentage_NewJobsMailsKey));
				if (newJobPercentage == null || newJobPercentage.equalsIgnoreCase("null")) {
					newJobPercentage = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				newJobsMails = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.numberofNewJobMailsKey));
				if (newJobsMails == null || newJobsMails.equalsIgnoreCase("null")) {
					newJobsMails = "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				// perSecondUsers =
				// String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.per));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String duplicateUserOakAndCylcon = "";
			try {
				duplicateUserOakAndCylcon = String.valueOf(SettingsClass.memcacheObj.get(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey));
				if (duplicateUserOakAndCylcon == null || duplicateUserOakAndCylcon.equalsIgnoreCase("null")) {
					duplicateUserOakAndCylcon = "";
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			String body = "<body>" + "<div ><h3 >" + name + "</h3><div>" + "<span>Total Users : </span>" + totalUsers + "<br /><span>User Processed : </span>" + userProcessed
					+ "<br /><span>Total number of thread : </span>" + SettingsClass.TOTAL_N0_THREAD + "<br /><span>User Processed Per Second: </span>" + perSecondUsers
					+ "<br /><span>Duplicate Users : </span>" + duplicateUsers + "<br /><span>Mails Sent : </span>" + mailSent + "<br /><span>Mails Per Second : </span>" + perSecondMail
					+ "<br /><span>Start Time : </span>" + startTime + "<br /><span>End Time : </span>" + endTime + "<br /><span>Cloud Search Percentage : </span>" + cloudSearchPer
					+ "<br /><span>Mail Cloud Percentage : </span>" + mailCloudPer + "<br /><span>Number of mails with new jobs : </span>" + newJobsMails
					+ "<br /><span>Percentage of Number of mails with new jobs : </span>" + newJobPercentage + "<br><span>Elastic cache duplicate : </span>" + duplicateUserOakAndCylcon
					+ "</body></html>";

			bodyMessage = body;

		} else
			bodyMessage = startSendMsg;

		Content htmlbody = new Content().withData("<html>" + bodyMessage + "</html>");
		// =====================================================

		Body body = new Body().withHtml(htmlbody);
		com.amazonaws.services.simpleemail.model.Message message = new com.amazonaws.services.simpleemail.model.Message(subject, body);
		request = new SendEmailRequest(MainWhiteLabelClass.parameterModelClassObject.getHostDomainName() + " <" + MainWhiteLabelClass.parameterModelClassObject.getFromEmailAddress() + ">",
				destination, message);
		emailSendClient.sendEmail(request);
		System.out.println("notify mail");

	}

}
