package whitelabels.mainpackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;
import whitelabels.mailsend.EmailClient;
import whitelabels.mailsend.MailSendInterface;
import whitelabels.model.ApiStatusModel;
import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.JobsResponseModel;
import whitelabels.model.ParameterModelClass;
import whitelabels.model.PassingObjectModel;
import whitelabels.model.ReadFeedOrderModel;
import whitelabels.model.ScoreBaseFeedModel;
import whitelabels.model.TemplateObject;
import whitelabels.model.UserQueueData;
import whitelabels.model.UsersData;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.util.EC2MetadataUtils;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MainWhiteLabelClass {

	public static Map<String, ClassLoader> trackLoadedJars = new HashMap<String, ClassLoader>();
	public ClassLoader classLoader;
	public Class<?> jobSearchclass;
	public Method method;
	public static ReadFeedOrderModel mReadFeedOrderModel = new ReadFeedOrderModel();
	public static ReadFeedOrderModel mOverlappedReadFeedOrderModel = new ReadFeedOrderModel();
	static Gson gson = new Gson();

	public static ParameterModelClass parameterModelClassObject = null;
	public static MainWhiteLabelClass mainClassObject = null;
	public static HashMap<String, Integer> maxJobsHashmap = new LinkedHashMap<String, Integer>();
	// public static List<String> sourceCloudSearchQueryList = new
	// ArrayList<String>();
	public static LinkedHashMap<String, Integer> jobProvidersJobCountMap = new LinkedHashMap<String, Integer>();
	public static boolean readLiveStatsFile = false;
	public static long processStartTime;
	ArrayList<UserQueueData> userQueueList;

	public static EmailClient mEmailClient = new EmailClient();

	// public static HashMap<String, String> ApiCPCHashMap = new HashMap<String,
	// String>();

	// public static String JuJuExactApiName = "JuJuExactApi";
	// public static String SimplyHiredExactApiName = "SimplyHiredExactApi";
	// public static String myJobHelperApiName = "myJobHelperApi";
	// public static String jobApprovedApiName = "jobApprovedApi";
	// public static String glassDoorApiName = "glassDoorApi";
	// public static String J2CExactApiName = "J2CExactApi";
	// public static String zipRecuriterApiName = "zipRecuriterApi";
	// public static String zipRecuriterNTLApiName = "zipRecuriterNTLApi";
	// public static String J2CIDEApiName = "J2CIDEApi";
	// public static String JuJuApiName = "JuJuApi";
	// public static String J2CApiName = "J2CApi";
	// public static String SimplyHiredApiName = "SimplyHiredApi";
	// public static String J2CTRUCKINGApiName = "J2CTRUCKINGApi";
	// public static String zipRecuriterNTLSecApiName = "zipRecuriterNTLSecApi";
	// public static String zipRecuriterNTLThirdApiName =
	// "zipRecuriterNTLThirdApi";
	// public static String J2CBoleanApiName = "J2CBooleanApi";
	// public static String lead5media = "Lead 5 mediaApi";
	// public static String zipAllAPi = "ZIPALLAPI";
	// public static String CareerBlissApi = "CareerBlissApi";
	//
	// public static String SimplyJobsApi = "SimplyJobsApi";
	//
	// public static String alwaysOnTopFeedsString = "";
	// static List<String> alwaysOnTopsourceCloudSearchQueryList = new
	// ArrayList<String>();
	//
	// public static String companyBlockedString =
	// "'college recruiter' 'alumini recruiter' 'sports authority' 'Contractstaffingrecruiters' 'Contractstaffingrecruiters.com' 'ameriplan' 'careerjimmy' 'Colangelo Synergy Marketing'";

	// public static MailSendInterface mailSendInterfaceObject = null;

	// public static List<String> sourceCloudSearchQueryListForCollegeRec = new
	// ArrayList<String>();
	//
	// public static String uberKeywords[] = { "sales", "Warehouse", "driver",
	// "package", "handler", "taxi", "delivery", "restaurant", "worker", "food",
	// "retail", "admin", "part time", "night shift",
	// "stocker", "ups", "fedex", "dhl" };

	// public static boolean collegActive = false;
	// public static boolean topUsaActive = false;

	public static boolean isJobvitals = false;

	public static boolean isPapaerrose = false;

	// public static String unsubString = "";
	public static String sparkpostunsubString = "";
	public static String startTimeForDashBoard = "";
	public static boolean isCombinedProcess = false;
	public static boolean isEncodeJobUrl = false;

	// cpc_tier: "L" Low = 9 cents
	// cpc_tier: "M" Med = 16 cents
	// cpc_tier: "H" High = 30 cents

	// public static float zipAllApi_L = 0.09f, zipAllApi_M = 0.16f, zipAllApi_H
	// = 0.30f;

	// FOR COMBINE PROCESS
	public static ArrayList<String> childDomainMemCacheArrayList = new ArrayList<String>();
	public static boolean readLiveChildStatsFile = false;
	public static ArrayList<String> categoriesList = new ArrayList<String>();
	public static Set<String> domains = new HashSet<String>();
	public static Set<String> categoryDomainWiseGroupTemplateStatsKeys = new HashSet<String>();
	public static HashMap<String, String> doimanWiseHashMap = new LinkedHashMap<String, String>();
	public static Set<String> providersSet = new HashSet<String>();

	public static String emailForTesting = "";

	public static String instanceId = "";

	public static void main(String args[]) {

		String processName = "Azuljob";

		try {

			if (args != null && args[0] != null) {
				SettingsClass.filePath = "/var/nfs-93/redirect/mis_logs/";
				processName = args[0];

				String processType = "";
				try {
					processType = args[1].trim();
				} catch (Exception e) {
				}

				if (!processType.equalsIgnoreCase("")) {

					if (processType.equalsIgnoreCase("runtest")) {
						SettingsClass.isThisTestRun = true;
						SettingsClass.runOrignalTest = false;
					} else if (processType.equalsIgnoreCase("runtest_original")) {
						SettingsClass.isThisTestRun = false;
						SettingsClass.runOrignalTest = true;
					} else {
						System.out.println("Run time Parameters not match! Please try again");
						System.exit(0);
					}

				} else {
					SettingsClass.isThisTestRun = false;
					SettingsClass.runOrignalTest = false;
				}

				try {
					emailForTesting = args[2].trim();
				} catch (Exception e) {
					emailForTesting = "";
				}

			} else {
				System.out.println("Run time Parameters not found! Please try again");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("Run time Parameters not found! Please try again");
			// System.exit(0);
		}

		if (processName.equalsIgnoreCase("")) {
			System.out.println("Run time Parameters not found! Please try again");
			Utility.sendTextSms("Run time parameter found empty please check cron with empty parameter.");
			System.exit(0);
		}

		if (!SettingsClass.isThisTestRun)
			instanceId = EC2MetadataUtils.getInstanceId();

		startTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
		mainClassObject = new MainWhiteLabelClass();
		loadExtraJarFiles();
		parameterModelClassObject = mainClassObject.readDataFromDb(processName);

		// check for evengin alerts to read feed order files from different
		// folder
		if (parameterModelClassObject.isForEveningAlerts()) {
			SettingsClass.feedOrderFolderName = "EveningFeedOrderFiles";
		} else {
			// for mornig alerts
			SettingsClass.feedOrderFolderName = "FeedOrderFilesForDemo";
		}
		new SettingsClass();

		if (!emailForTesting.equalsIgnoreCase(""))
			SettingsClass.testingEmailId = emailForTesting;

		System.out.println(SettingsClass.whitelabel_processname);

		try {
			SettingsClass.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
			if (!MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95"))
				SettingsClass.elasticMemCacheObj = new MemcachedClient(new InetSocketAddress("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211));
		} catch (Exception e) {
			e.printStackTrace();
		}

		processStartTime = System.currentTimeMillis();

		try {
			ReadApiParametersThread readFileThread = new ReadApiParametersThread();
			new Thread(readFileThread).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// String hashMapKey = "MailgunDomainName";
		//
		// if
		// (parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
		// {
		// mailSendInterfaceObject = new SendGridClass();
		// // unsubString = "<UNSUBSCRIBE>";
		// hashMapKey = "DomainUrl";
		// } else if
		// (parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
		// {
		// mailSendInterfaceObject = new MailGunClass();
		// // unsubString = "%unsubscribe_url%";
		// hashMapKey = "MailgunDomainName";
		// } else if
		// (parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
		// {
		// mailSendInterfaceObject = new SesClass();
		// hashMapKey = "DomainUrl";
		// }
		//
		// else if
		// (parameterModelClassObject.getEmailClient().equalsIgnoreCase("sparkpost"))
		// {
		// mailSendInterfaceObject = new SparkPostClass();
		// sparkpostunsubString = "data-msys-unsubscribe=\"1\"";
		// /*** check if hash key need to change ***/
		// hashMapKey = "DomainUrl";
		// }

		// + "where EmailClient = '"+ parameterModelClassObject.getEmailClient()
		// + "'";

		createDomainWiseHashMap();

		if (parameterModelClassObject.getDashboardName().toLowerCase().contains("jobvital")) {
			isJobvitals = true;
		} else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("paper")) {
			isPapaerrose = true;
		}

		if (SettingsClass.dashboradFileName.toLowerCase().contains("lempop") || SettingsClass.dashboradFileName.toLowerCase().contains("jobvitals")
				|| SettingsClass.dashboradFileName.toLowerCase().contains("blazealert")) {
			isEncodeJobUrl = true;
		}

		SettingsClass.awsCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);
		SettingsClass.sqs = new AmazonSQSClient(SettingsClass.awsCredentials);
		SettingsClass.sqs.setRegion(SettingsClass.region);

		// reading stats from the MemCache.............................
		readStatsFromMemCache();

		// reading stats from the file.............................
		if (readLiveStatsFile) {
			readStatsFromFile();
		}

		if (readLiveChildStatsFile) {
			// didn't get data from memcache,then get it from file
			readChildDomainsStatsFromFile();
		}

		try {
			WriteApiResponseTimeInFileThread writeFileThread = new WriteApiResponseTimeInFileThread();
			new Thread(writeFileThread).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		mainClassObject.getDataFromQueue();

	}

	private static void loadExtraJarFiles() {
		File directory = new File(SettingsClass.filePath + "jobslib/jobslib_lib");

		if (directory.exists()) {
			try {
				System.out.println("START execution....");

				if (trackLoadedJars != null && trackLoadedJars.size() > 0) {

					for (File file : directory.listFiles()) {

						if (trackLoadedJars.get(file.getName()) != null)
							continue;
						else {
							URL[] urls = new URL[] { file.toURL() };
							trackLoadedJars.put(file.getName(), new URLClassLoader(urls));
						}

					}
				} else {
					trackLoadedJars = new HashMap<String, ClassLoader>();
					for (File file : directory.listFiles()) {
						URL[] urls = new URL[] { file.toURL() };
						trackLoadedJars.put(file.getName(), new URLClassLoader(urls));
					}

				}
			} catch (Exception e) {
				Utility.sendTextSms(e.getMessage() + " problem occurs in loadExtraJarFiles for " + SettingsClass.dashboradFileName + " processs");
				e.printStackTrace();
			}

			try {
				File file = new File(SettingsClass.filePath + "jobslib/jobslib.jar");
				URL url = file.toURL();
				URL[] urls = new URL[] { url };

				if (mainClassObject.classLoader == null)
					mainClassObject.classLoader = new URLClassLoader(urls);

				if (mainClassObject.jobSearchclass == null)
					mainClassObject.jobSearchclass = mainClassObject.classLoader.loadClass("com.main.JobSearchLibraryMain");

				if (mainClassObject.method == null)
					mainClassObject.method = mainClassObject.jobSearchclass.getDeclaredMethod("enterInLib", new Class[] { String.class });
			} catch (Exception e) {
				Utility.sendTextSms(e.getMessage() + " problem occurs in loadExtraJarFiles for " + SettingsClass.dashboradFileName + " processs");
				e.printStackTrace();
			}

		}
	}

	public void jobSearchList(UsersData mUsersData, int localTemplateCount, int localGroupCount) {

		try {

			mUsersData.seeAllMatchingUrl = "http://" + mUsersData.domainName + "/jobs.php?q=" + mUsersData.keyword.replace(" ", "%20") + "&l=" + mUsersData.zipcode.replace(" ", "%20");

			File file = new File(SettingsClass.filePath + "jobslib/jobslib.jar");
			URL url = file.toURL();
			URL[] urls = new URL[] { url };

			if (mainClassObject.classLoader == null)
				mainClassObject.classLoader = new URLClassLoader(urls);

			if (mainClassObject.jobSearchclass == null)
				mainClassObject.jobSearchclass = mainClassObject.classLoader.loadClass("com.main.JobSearchLibraryMain");

			if (method == null)
				method = mainClassObject.jobSearchclass.getDeclaredMethod("enterInLib", new Class[] { String.class });

			PassingObjectModel mPassingObjectModel = new PassingObjectModel();

			if (mUsersData.providerName.toLowerCase().contains("overlapped"))
				mPassingObjectModel.setmReadFeedOrderModel(MainWhiteLabelClass.mOverlappedReadFeedOrderModel);
			else
				mPassingObjectModel.setmReadFeedOrderModel(MainWhiteLabelClass.mReadFeedOrderModel);

			mPassingObjectModel.setmUsersData(mUsersData);
			String input = gson.toJson(mPassingObjectModel);
			// Invoke method with input
			String str = (String) method.invoke(mainClassObject.jobSearchclass.newInstance(), input);

			JobsResponseModel jobsResponseModel = gson.fromJson(str, JobsResponseModel.class);

			ArrayList<Jobs> finalAllJobsList = jobsResponseModel.getFinalAllJobsList();
			SettingsClass.apiResponseTimeHashmap = jobsResponseModel.getApiResponseTimeHashmap();
			SettingsClass.apiUserProcessingCountHashmap = jobsResponseModel.getApiUserProcessingCountHashmap();
			if (finalAllJobsList.size() != 0) {
				createJobsFormat(finalAllJobsList, mUsersData, localTemplateCount, localGroupCount);
			} else {
				mUsersData.errorMessage = "No JObs Found";
				SettingsClass.userNotProcessedList.add(mUsersData);
			}

		} catch (Exception e) {
			sendSms(e.getMessage());
			e.printStackTrace();
		}
	}

	synchronized public void sendSms(String error) {
		if (SettingsClass.errorCount.get() <= 2)
			Utility.sendTextSms(error + " problem occurs in jobSearchList for " + SettingsClass.dashboradFileName);
		else {
			Utility.sendTextSms(SettingsClass.dashboradFileName + " process stopped due to any problem");
			System.exit(0);
		}
		SettingsClass.errorCount.incrementAndGet();
	}

	public void createJobsFormat(List<Jobs> finalJobList, UsersData userDataObject, int localTemplateCount, int localGroupCount) {
		if (finalJobList.size() > 0) {
			int csJobsCount = 0;

			if (userDataObject.isDistanceLessThan70Miles.toLowerCase().equalsIgnoreCase("yes")) {
				finalJobList = addUpsJobObject(finalJobList);
			}

			for (int i = 0; i < finalJobList.size(); i++) {
				if (finalJobList.get(i).getDisplaysource().equalsIgnoreCase("Zip Recruiter"))
					finalJobList.get(i).setDisplaysource("ZR");
				if (finalJobList.get(i).getApiOrCS().equalsIgnoreCase(SettingsClass.CS))
					csJobsCount++;
				String feed = finalJobList.get(i).getSourcename();
				if (MainWhiteLabelClass.jobProvidersJobCountMap.get(feed) != null) {
					MainWhiteLabelClass.jobProvidersJobCountMap.put(feed, MainWhiteLabelClass.jobProvidersJobCountMap.get(feed) + 1);
				} else {
					MainWhiteLabelClass.jobProvidersJobCountMap.put(feed, 1);
				}

				// new code
				String keyForhashMap = "";
				String keySourceName = finalJobList.get(i).getSourcename();
				keyForhashMap = userDataObject.domainName + "_" + keySourceName;

				// add cloudsearch jobs count according to feed
				if (SettingsClass.new_jobProviderCountDomainWise.get(keyForhashMap) != null) {
					// use set method to put incremented value into hashmap
					// MainOakJobSearchEnigne.jobProvidersJobCountMapValue.set(
					//
					// MainWhiteLabelClass.jobProvidersJobCountMap.get(feedName).incrementAndGet());

					SettingsClass.new_jobProviderCountDomainWise.put(keyForhashMap.trim(), SettingsClass.new_jobProviderCountDomainWise.get(keyForhashMap) + 1);

				} else {
					// if no key exists, then put default value

					SettingsClass.new_jobProviderCountDomainWise.put(keyForhashMap, 1);

				}
			}

			// got full 40 jobs from the CS and how many jobs from cs in
			try {
				if (csJobsCount == 40) {
					SettingsClass.user_CloudSearch_ProcessingCount.incrementAndGet();

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				float job_perc = 0;
				job_perc = csJobsCount / 40f * 100f;
				SettingsClass.cloudSearchJobPerc = SettingsClass.cloudSearchJobPerc + job_perc;
			} catch (Exception e) {
			}

			try {
				SettingsClass.jobsCountArray.set(finalJobList.size() - 1, SettingsClass.jobsCountArray.get(finalJobList.size() - 1) + 1);
			} catch (Exception e) {
			}

			Object emailMemchaheObject = null;

			try {

				emailMemchaheObject = SettingsClass.memcacheObj.get(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_"
						+ userDataObject.zipcode.replace(" ", "") + "_" + SettingsClass.queuName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			MailSendInterface mailSendInterfaceObject = null;

			System.out.println("userDataObject.sendGridCategory==>" + userDataObject.sendGridCategory);

			if (userDataObject.sendGridCategory.contains("default value")) {
				mailSendInterfaceObject = MainWhiteLabelClass.mEmailClient.getEmailClientInterface("ses");
			} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
				// userDataObject.sparkpostunsubString =
				// "data-msys-unsubscribe=\"1\"";
				mailSendInterfaceObject = MainWhiteLabelClass.mEmailClient.getEmailClientInterface("sparkpost");
			} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
				mailSendInterfaceObject = MainWhiteLabelClass.mEmailClient.getEmailClientInterface("mailgun");
			} else {
				mailSendInterfaceObject = MainWhiteLabelClass.mEmailClient.getEmailClientInterface("sendgrid");
			}

			if (emailMemchaheObject != null && emailMemchaheObject.equals("0")) {

				// calling send mail method to send..................
				try {
					mailSendInterfaceObject.sendEmail(userDataObject, finalJobList, localTemplateCount, localGroupCount);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (SettingsClass.isForMailtester) {
				System.out.println("send mail is calling");
				mailSendInterfaceObject.sendEmail(userDataObject, finalJobList, localTemplateCount, localGroupCount);

			}

		}

	}

	public List<Jobs> addUpsJobObject(List<Jobs> finalJobList) {
		Jobs job = new Jobs();
		job.setTitle("Package Handler Part Time");
		job.setSourcename("UPS");
		job.setZipcode("224630");
		job.setJoburl("https://www.jobs-ups.com/job/louisville/package-handler-part-time/1187/224630/?howheard=P4821");
		job.setEmployer("UPS");
		job.setCity("");
		job.setState("");
		ArrayList<Jobs> finalTempList = new ArrayList<Jobs>();
		finalTempList.add(job);
		finalTempList.addAll(finalJobList);
		if (finalTempList.size() >= 40) {
			finalTempList.remove(finalTempList.size() - 1);
		}
		finalJobList.clear();
		finalJobList.addAll(finalTempList);

		return finalJobList;

	}

	private static void createDomainWiseHashMap() {
		String query = "select DomainUrl,MailgunDomainName,FromEmailAddress,SendgridUsername,SendgridPassword,EmailClient,sparkpost_key from whitelabels_process";
		Connection con = openConnection();
		ResultSet rs = null;

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			rs = pst.executeQuery();

			while (rs.next()) {

				String key = rs.getString("DomainUrl");
				String emailClient = rs.getString("EmailClient");
				if (emailClient.equalsIgnoreCase("mailgun")) {
					key = rs.getString("MailgunDomainName");
				} else if (emailClient.equalsIgnoreCase("sparkpost")) {
					key = rs.getString("DomainUrl") + "_sparkpost";
				}
				String value = rs.getString("FromEmailAddress") + "| " + rs.getString("SendgridUsername") + "| " + rs.getString("SendgridPassword") + "| " + rs.getString("sparkpost_key");
				MainWhiteLabelClass.doimanWiseHashMap.put(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}

	}

	private void getDataFromQueue() {

		// now we have to identify the users queues in the system...

		userQueueList = new ArrayList<UserQueueData>();

		int totalQueueDataSize = 0;

		UserQueueData userQueueData = new UserQueueData();

		userQueueData.setUrl(SettingsClass.baseQueurl + SettingsClass.queuName);
		userQueueData.setSize(mainClassObject.getQueueSize(SettingsClass.baseQueurl + SettingsClass.queuName));
		totalQueueDataSize = totalQueueDataSize + userQueueData.getSize();

		userQueueList.add(userQueueData);

		System.out.println("\n Total users in all Queue..." + totalQueueDataSize);

		int total = totalQueueDataSize * 100;
		try {
			if (SettingsClass.totalUsersCount == null || SettingsClass.totalUsersCount.equalsIgnoreCase("")) {
				SettingsClass.totalUsersCount = "" + total;
				SettingsClass.memcacheObj.set(SettingsClass.totalUserKey, 0, String.valueOf(total));
			}
		} catch (Exception e) {
			//
			e.printStackTrace();
		}
		// here we will calculate the bcc percenatage...
		if (!SettingsClass.runOrignalTest && !SettingsClass.isThisTestRun) {

			try {
				SettingsClass.bccSendEmailIntervalCount = (Integer.parseInt(SettingsClass.totalUsersCount) * SettingsClass.bccSendEmailIntervalPercentage) / 100;
			} catch (Exception e) {

			}

		}

		for (int i = 0; i < userQueueList.size(); i++) {
			int thread_count = SettingsClass.TOTAL_N0_THREAD;
			userQueueList.get(i).setThreadCount(thread_count);

			System.out.println("\n User Queue Url..." + userQueueList.get(i).getUrl());
			System.out.println("\n QUEUE data size..." + userQueueList.get(i).getSize());
			System.out.println("\n Thread count for this QUEUE ..." + userQueueList.get(i).getThreadCount());

		}

		if (userQueueList.get(0).getSize() == 0 && !SettingsClass.runOrignalTest && !SettingsClass.isThisTestRun) {
			String text = SettingsClass.dashboradFileName + " has 0 data in the queue";
			Utility.sendTextSms(text);
			System.exit(0);
		}

		for (int i = 0; i < userQueueList.size(); i++) {
			GetUserFromQueueThread getUsersFromQueueThread = new GetUserFromQueueThread(userQueueList.get(i).getUrl(), userQueueList.get(i).getThreadCount());
			getUsersFromQueueThread.start();
		}

		userQueueList = null;
		System.gc();
	}

	public int getQueueSize(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessages");
		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessages"));
		} catch (Exception e) {

		}

		return size;

	}

	public int getQueueSizeInFlight(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessagesNotVisible");

		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessagesNotVisible"));
		} catch (Exception e) {

		}

		return size;

	}

	public static String getSearchEndPoint() {
		String searchEndpoint = "";
		int counter = 0;
		boolean flag = true;
		// AmazonSQSClient sqs = new
		// AmazonSQSClient(SettingsClass.awsCredentials);

		BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);
		AmazonSQSClient sqs = new AmazonSQSClient(basicAWSCredentials);
		sqs.setRegion(SettingsClass.region);

		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SettingsClass.domainQueueUrl);
		receiveMessageRequest.setMaxNumberOfMessages(10);
		receiveMessageRequest.getMessageAttributeNames();
		receiveMessageRequest.getAttributeNames();
		ReceiveMessageResult messageResult;
		while (flag) {
			System.out.println("reading queue");
			messageResult = sqs.receiveMessage(receiveMessageRequest);
			if (messageResult != null) {
				if (messageResult.getMessages().size() != 0) {
					com.amazonaws.services.sqs.model.Message message = messageResult.getMessages().get(0);
					String messData[] = message.getBody().split("\\|");
					searchEndpoint = messData[1].toString();
					flag = false;
				} else {
					counter++;
					if (counter >= 10) {
						// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail("There is some problem in getting search end point in "
						// + SettingsClass.dashboradFileName, "endpoint");
						flag = false;
					}
				}
			}
		}
		return searchEndpoint;
	}

	private static void readStatsFromMemCache() {
		try {
			String lastUserProcessed = (String) SettingsClass.memcacheObj.get(SettingsClass.userProcessed);
			if (lastUserProcessed != null)
				SettingsClass.userProcessingCount.set(Integer.parseInt(lastUserProcessed));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {
		}

		try {
			String cloudSearchUsersprocessed = (String) SettingsClass.memcacheObj.get(SettingsClass.cloudSearchUsersPer);
			if (cloudSearchUsersprocessed != null)
				SettingsClass.user_CloudSearch_ProcessingCount.set(Integer.parseInt(cloudSearchUsersprocessed));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {
		}

		try {
			String lastEmailSent = (String) SettingsClass.memcacheObj.get(SettingsClass.emailSend);
			if (lastEmailSent != null)
				SettingsClass.totalEmailCount.set(Integer.parseInt(lastEmailSent));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {
		}

		try {
			SettingsClass.memcacheObj.set(SettingsClass.processStartTime, 0,
					String.valueOf(SettingsClass.cal.get(Calendar.HOUR_OF_DAY) + ":" + SettingsClass.cal.get(Calendar.MINUTE) + ":" + SettingsClass.cal.get(Calendar.SECOND)));
		} catch (Exception e) {
		}
		try {
			String lastNewJobInMail = (String) SettingsClass.memcacheObj.get(SettingsClass.numberofNewJobMailsKey);
			if (lastNewJobInMail != null)
				SettingsClass.numberOfNewJobMails.set(Integer.parseInt(lastNewJobInMail));
			else
				readLiveStatsFile = true;
		} catch (Exception e) {

		}
		try {

			String jobs_per = (String) SettingsClass.memcacheObj.get(SettingsClass.jobsCountInMails);

			if (jobs_per != null) {
				Utility.jobsCountInMail_toArray(jobs_per);
			} else
				readLiveStatsFile = true;

		} catch (Exception e) {

		}
		// get groupAndTemplateCount value for hashmap
		try {

			String groupTemplateCount_arr = (String) SettingsClass.memcacheObj.get(SettingsClass.groupTemplatesCountKey);

			if (groupTemplateCount_arr != null) {
				Utility.groupTemplateIds_toMap(groupTemplateCount_arr);
			} else {

				readLiveStatsFile = true;
			}
			try {

				String duplicateUser = (String) SettingsClass.memcacheObj.get(SettingsClass.duplicateUserKey);
				if (duplicateUser != null) {
					SettingsClass.duplicateUser.set(Integer.parseInt(duplicateUser));
				}

			} catch (Exception e) {

			}

		} catch (Exception e) {

		}
		try {

			String jobsCount_arr = (String) SettingsClass.memcacheObj.get(SettingsClass.JobProvidersjobsCountKey);

			if (jobsCount_arr != null)
				Utility.providersjobsCount_toMap(jobsCount_arr);
			else
				readLiveStatsFile = true;

		} catch (Exception e) {

		}

		// ========================================child process
		// code============================================
		// here we also need to get values for others domain as the same
		// way ...if exists...

		for (int i = 0; i < MainWhiteLabelClass.childDomainMemCacheArrayList.size(); i++) {

			try {

				// Settings.userProcessed
				if (SettingsClass.memcacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.userProcessed) == null)
					readLiveChildStatsFile = true;
				else {
					SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.userProcessed,
							Integer.parseInt(SettingsClass.memcacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.userProcessed).toString()));
				}

				// System.out.println("readLiveChildStatsFile" +
				// MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_"
				// + SettingsClass.userProcessed + " " +
				// readLiveChildStatsFile);

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				// Settings.emailSend
				if (SettingsClass.memcacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.emailSend) == null)
					readLiveChildStatsFile = true;
				else {
					SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.emailSend,
							Integer.parseInt(SettingsClass.memcacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.emailSend).toString()));
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				if (!MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {

					// elastic duplicate users count for the child process
					if (SettingsClass.elasticMemCacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.elasticDuplicateMemcacheKey) == null)
						readLiveChildStatsFile = true;
					else
						SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.elasticDuplicateMemcacheKey, Integer
								.parseInt(SettingsClass.elasticMemCacheObj.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.elasticDuplicateMemcacheKey).toString()));

				}
			} catch (Exception e) {

			}
		}

		// get groupAndTemplateCount value for hashmap
		try {

			String groupTemplateCount_arr = (String) SettingsClass.memcacheObj.get(SettingsClass.new_DomainCaterotyWiseGroupTemplateStatedMemcacaheKey);

			if (groupTemplateCount_arr != null) {
				Utility.convert_categoryDomainWiseGroupTemplateStats_ToMap(groupTemplateCount_arr);
			} else {

				readLiveStatsFile = true;
			}

		} catch (Exception e) {

		}
		// jobprovider count as per dthe domain
		try {

			String jobsCount_arr = (String) SettingsClass.memcacheObj.get(SettingsClass.new_jobProviderCountDomainWiseKey);

			if (jobsCount_arr != null) {
				Utility.convert_JobDomainwiseProviderCount_ToHashMap(jobsCount_arr);
			} else {

				readLiveStatsFile = true;
			}
		} catch (Exception e) {

		}

		try {

			String duplicateUser = (String) SettingsClass.memcacheObj.get(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey);
			if (duplicateUser != null) {
				SettingsClass.duplicateUserFromElasticCache.set(Integer.parseInt(duplicateUser));
			}
		} catch (Exception e) {

		}

	}

	private static void readStatsFromFile() {
		BufferedReader bufferedReader = null;
		String readLine = null;
		Boolean processRestarted = false;
		try {
			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "dashboard" + File.separator + SettingsClass.queuName + "_live_stats.txt"));
			readLine = bufferedReader.readLine();
			String Data[] = readLine.toString().split("\\|");
			int runningState = Integer.parseInt(Data[10]);
			boolean TodayDate = true;
			Date date1 = null, date2 = null;
			Calendar calendar = new GregorianCalendar();
			String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			try {

				date1 = sdf.parse(currentdate);

				date2 = sdf.parse(Data[0]);

			} catch (ParseException e) {

				e.printStackTrace();
			}

			if (date1.equals(date2)) {
				TodayDate = true;
			} else {
				TodayDate = false;
			}

			if (runningState == 0 && TodayDate) {
				// process has restarted
				processRestarted = true;
				SettingsClass.userProcessingCount.set(Integer.parseInt(Data[2]));
				SettingsClass.totalEmailCount.set(Integer.parseInt(Data[4]));
				SettingsClass.numberOfNewJobMails.set(Integer.parseInt(Data[11]));

				// CLOUD SEARCH USERS PERCENTAGE
				double usersProcssedCS = Double.parseDouble(Data[7]);
				Integer CSUsersCount = (int) ((usersProcssedCS / 100) * SettingsClass.userProcessingCount.get());

				SettingsClass.user_CloudSearch_ProcessingCount.set(CSUsersCount);
			}

		} catch (Exception e) {

		}
		bufferedReader = null;

		if (processRestarted) {
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "Jobs_count" + File.separator + SettingsClass.queuName + "_process_stats.txt"));
			} catch (Exception e) {

			}
			try {

				readLine = bufferedReader.readLine();

				// System.out.println(readLine.toString());

				String Data1[] = readLine.toString().split("\\|");

				Utility.jobsCountInMail_toArray(Data1[1]);

			} catch (Exception e) {

				e.printStackTrace();
			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "jobProvidersJobCount" + File.separator + SettingsClass.queuName
						+ "_providersJobsCount.json"));
				readLine = bufferedReader.readLine();
				Utility.convertJobProviderJobCountJsontoMap(readLine);
			} catch (Exception e) {

			}
			bufferedReader = null;

			// try {
			// bufferedReader = new BufferedReader(new
			// FileReader(SettingsClass.filePath + File.separator +
			// "java_subject_line_stats" + File.separator +
			// SettingsClass.queuName
			// + "_groupAndTemplateCount.json"));
			// readLine = bufferedReader.readLine();
			// Utility.templateGsonToMap(readLine);
			// }
			// catch (Exception e) {
			//
			// }

			// ====================================================//new subject
			// line child wise=========================================

			try {

				Iterator it = SettingsClass.child_subject_From_File_Names.entrySet().iterator();

				while (it.hasNext()) {

					Map.Entry pair = (Map.Entry) it.next();
					String fileName = pair.getValue().toString();

					try {
						bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "java_subject_line_stats" + File.separator + SettingsClass.queuName + "_"
								+ fileName + ".json"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						// here we are continue if file not found in the folder
						continue;
					}
					System.out.println("File name=" + SettingsClass.filePath + File.separator + "java_subject_line_stats" + File.separator + SettingsClass.queueName_instanceId + "_" + fileName
							+ ".json");

					readLine = bufferedReader.readLine();

					Utility.templateGsonToMap_New_Method(readLine, fileName);
					// Today DateTime|1to 40 job count|Process name

				}
			} catch (Exception e) {

			}

			// =======================================================//======================================

		} // if condition ended process restarted

	}

	private static void readChildDomainsStatsFromFile() {

		// read all necessary live stats from file

		BufferedReader bufferedReader = null;
		String readLine = null;
		Boolean processRestarted = false;

		// /read live stats from file
		try {

			for (int i = 0; i < MainWhiteLabelClass.childDomainMemCacheArrayList.size(); i++) {

				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "combine_process_dashboard/" + MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_live_stats.txt"));

				// System.out.println("reading stats from file=" +
				// SettingsClass.filePath + "combine_process_dashboard/" +
				// MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) +
				// "_live_stats.txt");

				readLine = bufferedReader.readLine();

				// System.out.println(readLine.toString());

				String Data[] = readLine.toString().split("\\|");

				// read Settings.processesIsRunning to check whether these
				// values
				// are today's or not

				int runningState = Integer.parseInt(Data[10]);

				boolean TodayDate = true;

				Date date1 = null, date2 = null;
				Calendar calendar = new GregorianCalendar();

				String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

				try {

					date1 = sdf.parse(currentdate);

					date2 = sdf.parse(Data[0]);

				} catch (ParseException e) {

					e.printStackTrace();
				}

				if (date1.equals(date2)) {
					TodayDate = true;
				} else {
					TodayDate = false;
				}
				synchronized (SettingsClass.child_Process_stats_hashMap) {

					if (runningState == 0 && TodayDate) {

						// here we set the stats in the memecache from the file
						processRestarted = true;
						SettingsClass.memcacheObj.set(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.userProcessed, 0, Integer.parseInt(Data[2]));

						SettingsClass.memcacheObj.set(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.emailSend, 0, Integer.parseInt(Data[4]));

						// setting chilc procees stats in the hasp map

						SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.userProcessed, Integer.parseInt(Data[2]));

						SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.emailSend, Integer.parseInt(Data[4]));

						SettingsClass.child_Process_stats_hashMap.put(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i) + "_" + SettingsClass.elasticDuplicateMemcacheKey,
								Integer.parseInt(Data[18]));
					}
				}
			}

		} catch (Exception e) {

		}
		if (processRestarted) {
			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "new_jobProvidersJobCount" + File.separator + SettingsClass.queuName + "_providersJobsCount.json"));
				readLine = bufferedReader.readLine();
				Utility.convert_JobproviderCountHashMapstatsToMap_Fromfile(readLine);
			} catch (Exception e) {

			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "new_jobProvidersJobCount" + File.separator + SettingsClass.queuName + "_providersJobsCount.json"));
				readLine = bufferedReader.readLine();
				Utility.convert_JobproviderCountHashMapstatsToMap_Fromfile(readLine);
			} catch (Exception e) {

			}
		}
	}

	public ParameterModelClass readDataFromDb(String args) {

		System.out.println(args);

		Connection con = openConnection();
		ResultSet rs = null;
		ParameterModelClass modelClassObject = null;
		String query = "select * from whitelabels_process where DashboardName='" + args + "'";
		System.out.println("query==>" + query);

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			rs = pst.executeQuery();

			if (rs.next()) {
				JSONObject convert = mainClassObject.convert(rs);
				modelClassObject = new Gson().fromJson(convert.toString(), ParameterModelClass.class);
			} else {
				Utility.sendTextSms("No row found for " + args + " dashboard name please check");
				System.exit(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}

		return modelClassObject;
	}

	public JSONObject convert(ResultSet rs) {
		JSONObject obj = new JSONObject();
		try {

			ResultSetMetaData rsmd = rs.getMetaData();

			// if (rs.next()) {
			int numColumns = rsmd.getColumnCount();

			for (int i = 1; i < numColumns + 1; i++) {
				String column_name = rsmd.getColumnName(i);

				try {

					if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
						obj.put(column_name, rs.getArray(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
						obj.put(column_name, rs.getBoolean(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
						obj.put(column_name, rs.getBlob(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
						obj.put(column_name, rs.getDouble(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
						obj.put(column_name, rs.getFloat(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
						obj.put(column_name, rs.getNString(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
						obj.put(column_name, rs.getString(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
						obj.put(column_name, rs.getDate(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
						obj.put(column_name, rs.getTimestamp(column_name));
					} else {
						obj.put(column_name, rs.getObject(column_name));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static Connection openConnection() {
		String HOST_NAME = "oakuserdbinstance.ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";
		String DB_NAME = "oakalerts";
		String username = "gagangill";
		String password = "GiG879#$%";
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username, password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}

}

class GetUserFromQueueThread extends Thread {

	String mailQueueName;
	String userQueueUrl;
	int userQueueThreadCount;

	GetUserFromQueueThread(String userQueueUrl, int userQueueThreadCount) {

		// this.mailQueueName = mailQueueName;
		this.userQueueUrl = userQueueUrl;
		this.userQueueThreadCount = userQueueThreadCount;

	}

	@Override
	public void run() {

		try {
			Thread.sleep(10 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(userQueueUrl);

			if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest)
				receiveMessageRequest.setMaxNumberOfMessages(1);
			else
				receiveMessageRequest.setMaxNumberOfMessages(10);

			receiveMessageRequest.getMessageAttributeNames();
			receiveMessageRequest.getAttributeNames();

			// start process mail...
			// if (!SettingsClass.isThisTestRun &&
			// !SettingsClass.runOrignalTest)
			// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(SettingsClass.dashboradFileName
			// + " Process has started", "start");

			for (int i = 0; i < userQueueThreadCount; i++) {
				JobSearchThread jobSearchThread = new JobSearchThread(receiveMessageRequest, userQueueUrl);
				jobSearchThread.start();

			}

		} catch (Exception e) {
			System.out.println("\nException in GetUsersFromQueueThread..." + e.toString());
		}

		System.gc();

	}
}

class JobSearchThread extends Thread {

	ReceiveMessageRequest receiveMessageRequest;
	String userQueueName = "", userQueueUrl = "";

	AtomicInteger localGroupThreadCount = new AtomicInteger(0);
	AtomicInteger localTemplateThreadCount = new AtomicInteger(0);

	String company_title;
	LinkedHashMap<String, Integer> jobsKeyJobTitleMap;

	JobSearchThread(ReceiveMessageRequest receiveMessageRequest, String userQueueUrl) {
		this.receiveMessageRequest = receiveMessageRequest;
		this.userQueueUrl = userQueueUrl;

	}

	public JobSearchThread() {
		company_title = "";
		jobsKeyJobTitleMap = new LinkedHashMap<String, Integer>();
	}

	@Override
	public void run() {

		while (true) {

			try {

				SettingsClass.cal = Calendar.getInstance();
				if (SettingsClass.cal.get(Calendar.HOUR_OF_DAY) >= 23 && SettingsClass.cal.get(Calendar.MINUTE) >= 30) {

					// change these values in end of process to show stop status
					// and end time
					SettingsClass.processesIsRunning = 1;

					SettingsClass.currentTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());

					Utility.updateMemcacheStatsInEnd();
					Utility.killProcess();
				}

				// if emptyUserMessageCount is more then 100 then we will not go
				// for fetching users from queue...
				if (SettingsClass.emptyUserMessageCount.get() <= 100) {

					List<Message> messages = SettingsClass.sqs.receiveMessage(receiveMessageRequest).getMessages();

					if (messages.size() == 0) {
						// System.out.println(" \n\nQueue is empty...");

						// reset the empty queue count..
						SettingsClass.emptyUserMessageCount.incrementAndGet();
						// if msg is 0 then sleep thread for 1 second.
						// sleep(1000);

					} else {

						// reset the empty queue count..
						SettingsClass.emptyUserMessageCount.set(0);

						for (Message message : messages) {

							// split the multiple users into array
							String[] usersList = message.getBody().split("\n");

							for (String user : usersList) {

								company_title = "";

								// hashmap for company+title , count(max 3)
								jobsKeyJobTitleMap = new LinkedHashMap<String, Integer>();

								String[] bodyData = user.split("\\|");
								UsersData userDataObject = new UsersData();

								try {
									userDataObject.id = bodyData[0];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.keyword = (URLDecoder.decode(Utility.html2text(bodyData[1].trim()), "UTF-8"));
									if (userDataObject.keyword.contains("$pipe$")) {
										userDataObject.keyword.replace("$pipe$", "|");
									}
								} catch (Exception e2) {
									userDataObject.keyword = (bodyData[1].trim());
									if (userDataObject.keyword.contains("$pipe$")) {
										userDataObject.keyword.replace("$pipe$", "|");
									}
									e2.printStackTrace();
								}

								try {
									userDataObject.zipcode = bodyData[2];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.email = bodyData[3];
									userDataObject.email = userDataObject.email.replace(" ", "");
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.advertisement_id = bodyData[4];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.registration_date = bodyData[5];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								// 5 //6 -- missing
								try {
									userDataObject.city = bodyData[6];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.state = bodyData[7];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.latitude = bodyData[8];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.longitude = bodyData[9];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.upperLatitude = bodyData[10];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.upperLongitude = bodyData[11];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.lowerLatitude = bodyData[12];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}
								try {
									userDataObject.lowerLongitude = bodyData[13];
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								try {
									userDataObject.isOpener_Clicker = bodyData[14];
								}

								catch (Exception e2) {
									userDataObject.isOpener_Clicker = "0";
								}

								try {
									userDataObject.openTime = bodyData[15];
								} catch (Exception e2) {
									userDataObject.openTime = "";
								}

								try {

									String code = bodyData[19];

									if (code != null && !code.equalsIgnoreCase("") && !code.toLowerCase().contains("null")) {
										userDataObject.affiliatCode = "-" + bodyData[19];
									} else {
										userDataObject.affiliatCode = "";
									}

								} catch (Exception e2) {
									userDataObject.affiliatCode = "";
									e2.printStackTrace();
								}

								try {
									userDataObject.searchKeyword = (URLDecoder.decode(Utility.html2text(bodyData[22].trim()), "UTF-8"));
									if (userDataObject.searchKeyword.contains("$pipe$")) {
										userDataObject.searchKeyword.replace("$pipe$", "|");
									}
								} catch (Exception e2) {
									userDataObject.searchKeyword = (bodyData[22].trim());
									if (userDataObject.searchKeyword.contains("$pipe$")) {
										userDataObject.searchKeyword.replace("$pipe$", "|");
									}
									e2.printStackTrace();
								}

								try {
									userDataObject.isDistanceLessThan70Miles = bodyData[23];
								} catch (Exception e2) {
									userDataObject.isDistanceLessThan70Miles = "NO";
								}

								try {
									userDataObject.scheduleSendForMailgun = bodyData[24];
								} catch (Exception e) {
									e.printStackTrace();

								}

								try {
									userDataObject.firstName = bodyData[17];

									if (userDataObject.firstName == null || userDataObject.firstName.equalsIgnoreCase("") || userDataObject.firstName.equalsIgnoreCase("null")) {
										userDataObject.firstName = "";
									}

								} catch (Exception e2) {

									// e2.printStackTrace();
									userDataObject.firstName = "";
								}

								try {
									userDataObject.radius = bodyData[20];
									if (userDataObject.radius == null || userDataObject.radius.equalsIgnoreCase("") || userDataObject.radius.equalsIgnoreCase("null"))
										userDataObject.radius = "30";
								} catch (Exception e2) {

									userDataObject.radius = "30";
								}

								try {
									userDataObject.userMailQueueName = userQueueName;
								} catch (Exception e2) {
									//
									e2.printStackTrace();
								}

								/*
								 * new parameters for combined process
								 */

								try {
									userDataObject.t2Value = bodyData[29];
								} catch (Exception e2) {
								}
								try {
									userDataObject.jujuChannel = bodyData[30];
								} catch (Exception e2) {
								}
								try {
									userDataObject.liveRamp = bodyData[31];
								} catch (Exception e2) {
								}

								try {
									userDataObject.compainId = bodyData[32];
								} catch (Exception e2) {
								}

								try {
									userDataObject.domainName = bodyData[33];
								} catch (Exception e2) {
								}

								try {
									userDataObject.userSourceNameForHtml = bodyData[27];
								} catch (Exception e2) {
								}

								try {
									userDataObject.providerName = bodyData[28];
								} catch (Exception e2) {
								}

								try {
									userDataObject.categoryProviderForOpenPixels = bodyData[34];
								} catch (Exception e2) {
								}

								try {
									userDataObject.sendGridCategory = bodyData[36];
								} catch (Exception e2) {
								}

								try {

									String sparkpostKey = "";
									if (userDataObject.sendGridCategory.toLowerCase().contains("sparkpost"))
										sparkpostKey = "_sparkpost";

									String[] spilt = MainWhiteLabelClass.doimanWiseHashMap.get(userDataObject.domainName + sparkpostKey).split("\\|");
									userDataObject.fromDomainName = spilt[0].trim();
									userDataObject.sendGridUserName = spilt[1].trim();
									userDataObject.sendGridUserPassword = spilt[2].trim();
									userDataObject.sparkpost_key = spilt[3].trim();
								} catch (Exception e2) {
								}

								try {
									userDataObject.country = bodyData[37];
								} catch (Exception e2) {
								}

								try {
									userDataObject.campgain_category_mapping_key = bodyData[38];
								} catch (Exception e2) {
								}

								userDataObject.dashboradFileName = SettingsClass.dashboradFileName;

								if (userDataObject.providerName.toLowerCase().contains("blazealerts")) {

									if (userDataObject.providerName.toLowerCase().contains("deckinc")) {
										userDataObject.t2Value = "&t2=deck";
										userDataObject.jujuChannel = "deck";
									} else if (userDataObject.providerName.toLowerCase().contains("bowering")) {
										userDataObject.t2Value = "&t2=bow";
										userDataObject.jujuChannel = "bow";
									}

								}

								if (!SettingsClass.isThisTestRun && MainWhiteLabelClass.parameterModelClassObject.isCylconSubProvider()
										&& !MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
									try {
										Object duplicateUserEmailUPTo4MailSentObject = null;
										try {
											duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replace(" ", "") + "_Oak_Cylcon");
										} catch (Exception e) {

											e.printStackTrace();
										}

										if (duplicateUserEmailUPTo4MailSentObject == null) {
											try {
												OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replace(" ", "") + "_Oak_Cylcon", 0, 0);
											} catch (Exception e) {
												e.printStackTrace();
											}
										} else if (Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString()) >= 4 && !userDataObject.providerName.contains("-web")) {
											try {
												SettingsClass.memcacheObj.set(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey, 0,
														String.valueOf(SettingsClass.duplicateUserFromElasticCache.incrementAndGet()));

											} catch (Exception e) {
												e.printStackTrace();
											}

											addChildElasticDuplicate(userDataObject);

											continue;
										} else if (Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString()) >= 10) {
											try {
												SettingsClass.memcacheObj.set(SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey, 0,
														String.valueOf(SettingsClass.duplicateUserFromElasticCache.incrementAndGet()));
											} catch (Exception e) {
												e.printStackTrace();
											}
											addChildElasticDuplicate(userDataObject);
											continue;
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}

								Object duplicateUserEmailObject = null;
								try {

									duplicateUserEmailObject = SettingsClass.memcacheObj.get(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_"
											+ userDataObject.zipcode.replace(" ", "") + "_" + SettingsClass.queuName);

								} catch (Exception e) {
									//
									e.printStackTrace();
								}
								String child_process_key = "";
								Integer childUserProcessed = null;
								String tempKey = "";

								if (userDataObject.sendGridCategory.contains("default value")) {
									tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
								} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
									tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
								} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
									tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
								} else {
									tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
								}

								// if
								// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
								// {
								// tempKey = "_" +
								// userDataObject.sendGridCategory.replace(" ",
								// "").trim();
								// }
								// else if
								// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
								// {
								// tempKey = "_" +
								// userDataObject.compainId.replace(" ",
								// "").trim();
								// }
								// else if
								// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
								// {
								// tempKey = "_" +
								// userDataObject.sendGridCategory.replace(" ",
								// "").trim();
								// }
								// else if
								// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sparkpost"))
								// {
								// tempKey = "_" +
								// userDataObject.compainId.replace(" ",
								// "").trim();
								// }
								child_process_key = userDataObject.domainName.replace(" ", "").trim() + tempKey.replace(" ", "").trim();
								// here we are setting the child process
								// key with the user
								userDataObject.child_process_key = child_process_key;

								if (duplicateUserEmailObject == null) {
									SettingsClass.userProcessingCount.getAndIncrement();
									SettingsClass.newuserProcessingCount.getAndIncrement();
									try {
										SettingsClass.memcacheObj.set(SettingsClass.userProcessed, 0, String.valueOf(SettingsClass.userProcessingCount));

									} catch (Exception e) {
										e.printStackTrace();
									}

									try {

										String childUserProcessedKeyStr = userDataObject.domainName.replace(" ", "").trim() + tempKey + "_" + SettingsClass.userProcessed;

										synchronized (SettingsClass.child_Process_stats_hashMap) {

											try {
												childUserProcessed = SettingsClass.child_Process_stats_hashMap.get(childUserProcessedKeyStr);
											} catch (Exception e) {
												childUserProcessed = 0;
											}
											if (childUserProcessed == null)
												childUserProcessed = 0;
											childUserProcessed++;
											SettingsClass.memcacheObj.set(childUserProcessedKeyStr, 0, String.valueOf(childUserProcessed));
											SettingsClass.child_Process_stats_hashMap.put(childUserProcessedKeyStr, childUserProcessed);
										}
									} catch (Exception e) {
									}

									try {
										SettingsClass.memcacheObj.set(
												userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_" + userDataObject.zipcode.replace(" ", "") + "_"
														+ SettingsClass.queuName, 0, String.valueOf("0"));
									} catch (Exception e1) {

									}
									try {
										if (!userDataObject.email.equalsIgnoreCase("") && !userDataObject.keyword.equalsIgnoreCase("") && !userDataObject.zipcode.equalsIgnoreCase("")) {

											ArrayList<GroupObject> groupObjectList = SettingsClass.child_Grouplist_map.get(child_process_key);

											if (groupObjectList == null) {
												groupObjectList = SettingsClass.groupObjectList;
												String data = "child_grouplist_key  " + child_process_key + "\n" + new Gson().toJson(userDataObject) + "\n" + "\n" + user + "\n";
												data += "=======================================================\n";
												BufferedWriter bufferedWriter = null;
												File file = new File("/var/nfs-93/redirect/mis_logs/test_sujectline_key/" + userDataObject.dashboradFileName + ".txt");
												try {
													bufferedWriter = new BufferedWriter(new FileWriter(file, true));
													bufferedWriter.write(data);

												} catch (Exception e1) {
													e1.printStackTrace();
												} finally {
													try {
														bufferedWriter.close();
													} catch (Exception e2) {
														e2.printStackTrace();
													}
												}
											}

											if (localTemplateThreadCount.get() >= SettingsClass.templateObjectList.size()) {
												localTemplateThreadCount.set(0);

												// increment group id count
												localGroupThreadCount.getAndIncrement();

												if (localGroupThreadCount.get() >= groupObjectList.size())
													localGroupThreadCount.set(0);
											}

											MainWhiteLabelClass.mainClassObject.jobSearchList(userDataObject, localTemplateThreadCount.get(), localGroupThreadCount.get());
											localTemplateThreadCount.getAndIncrement();

										}

									} catch (Exception e) {
										System.out.println("users_id=" + userDataObject.id + " complete data=" + user.toString());
										e.printStackTrace();

									}
								} else {

									if (duplicateUserEmailObject.equals("0")) {
										try {
											if (!userDataObject.email.equalsIgnoreCase("") && !userDataObject.keyword.equalsIgnoreCase("") && !userDataObject.zipcode.equalsIgnoreCase("")) {
												ArrayList<GroupObject> groupObjectList = SettingsClass.child_Grouplist_map.get(child_process_key);

												if (groupObjectList == null) {
													groupObjectList = SettingsClass.groupObjectList;
													String data = "child_grouplist_key  " + child_process_key + "\n" + new Gson().toJson(userDataObject) + "\n" + "\n" + user + "\n";
													data += "=======================================================\n";
													BufferedWriter bufferedWriter = null;
													File file = new File("/var/nfs-93/redirect/mis_logs/test_sujectline_key/" + userDataObject.dashboradFileName + ".txt");
													try {
														bufferedWriter = new BufferedWriter(new FileWriter(file, true));
														bufferedWriter.write(data);

													} catch (Exception e1) {
														e1.printStackTrace();
													} finally {
														try {
															bufferedWriter.close();
														} catch (Exception e2) {
															e2.printStackTrace();
														}
													}
												}

												if (localTemplateThreadCount.get() >= SettingsClass.templateObjectList.size()) {
													localTemplateThreadCount.set(0);

													// increment group id count
													localGroupThreadCount.getAndIncrement();

													if (localGroupThreadCount.get() >= groupObjectList.size())
														localGroupThreadCount.set(0);
												}

												MainWhiteLabelClass.mainClassObject.jobSearchList(userDataObject, localTemplateThreadCount.get(), localGroupThreadCount.get());
												localTemplateThreadCount.getAndIncrement();

											}

										} catch (Exception e) {
											System.out.println("users_id=" + userDataObject.id + "\n complete data=" + user.toString());
											e.printStackTrace();

										}

									} else {
										SettingsClass.duplicateUser.getAndIncrement();
										try {
											SettingsClass.memcacheObj.set(SettingsClass.duplicateUserKey, 0, String.valueOf(SettingsClass.duplicateUser.get()));
										} catch (Exception e) {
											//
											e.printStackTrace();
										}
									}
								}

							}

							// deleting the msg after full processing..now we
							// are processing max 125 users in a msg..
							try {
								String messageRecieptHandle = message.getReceiptHandle();
								SettingsClass.sqs.deleteMessage(new DeleteMessageRequest(this.userQueueUrl, messageRecieptHandle));
							} catch (Exception e) {
								//
								e.printStackTrace();
							}

							// here exit from the running process on specific
							// mail count...

							if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest) {
								System.out.println("Stopped Process!");
								System.exit(0);
							}

						}

					} // end of else part..

				} // end of emptyUserMessageCount check..

				if (SettingsClass.emptyUserMessageCount.get() > 100) {

					if (SettingsClass.emptyQueueCount.get() < 10) {

						// getting queue size from the aws server
						int queueSize = MainWhiteLabelClass.mainClassObject.getQueueSize(userQueueUrl);
						// getting size of flight messages
						int flightQueueSize = MainWhiteLabelClass.mainClassObject.getQueueSizeInFlight(userQueueUrl);

						if (queueSize == 0 && flightQueueSize == 0) {

							SettingsClass.emptyQueueCount.incrementAndGet();
						} else if (queueSize != 0) {

							SettingsClass.emptyQueueCount.set(0);
							SettingsClass.emptyUserMessageCount.set(0);

						} else if (flightQueueSize != 0) {

							SettingsClass.emptyQueueCount.set(0);

							sleep(1000 * 60 * 2);
							System.out.println(" \n Process slept!");

						}

					} else {
						SettingsClass.processesIsRunning = 1;
						SettingsClass.currentTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
						Utility.updateMemcacheStatsInEnd();
						Utility.killProcess();

					}

				}

			} catch (Exception e) {
				//
				e.printStackTrace();
			}

		}

	}

	public void addChildElasticDuplicate(UsersData userDataObject) {
		try {
			Integer elasticChildDuplicate = null;

			String tempKey = "";

			if (userDataObject.sendGridCategory.contains("default value")) {
				tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
			} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
				tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
			} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
				tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
			} else {
				tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
			}

			// if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("sendgrid"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replace(" ",
			// "").trim();
			// } else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("mailgun"))
			// {
			// tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
			// } else if
			// (MainWhiteLabelClass.parameterModelClassObject.getEmailClient().equalsIgnoreCase("ses"))
			// {
			// tempKey = "_" + userDataObject.sendGridCategory.replace(" ",
			// "").trim();
			// }

			String elasticchildUserDuplicateKeyStr = userDataObject.domainName.replace(" ", "").trim() + tempKey + "_" + SettingsClass.elasticDuplicateMemcacheKey;

			synchronized (SettingsClass.child_Process_stats_hashMap) {
				try {
					elasticChildDuplicate = SettingsClass.child_Process_stats_hashMap.get(elasticchildUserDuplicateKeyStr);
				} catch (Exception e) {
					elasticChildDuplicate = 0;
				}
				if (elasticChildDuplicate == null)
					elasticChildDuplicate = 0;
				elasticChildDuplicate++;
				if (!MainWhiteLabelClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
					OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(elasticchildUserDuplicateKeyStr, 0, String.valueOf(elasticChildDuplicate));
				}
				SettingsClass.child_Process_stats_hashMap.put(elasticchildUserDuplicateKeyStr, elasticChildDuplicate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

class WriteApiResponseTimeInFileThread implements Runnable {

	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
	FileWriter fileWriterObj;
	File log_folder;
	File log_dashboard_folder;

	public void run() {
		try {
			while (true) {
				try {
					// for three minute sleep
					Thread.sleep(3 * 60 * 1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Utility.updateMemcacheStatsInEnd();
			} // end of while

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}

/**
 * 
 * Thread class to read usability of Api's in file
 * 
 */

class ReadApiParametersThread implements Runnable {

	String preVersionOverlapped = "";

	public void run() {

		String preVersion = "";
		BufferedReader bufferedReader = null;
		boolean isCloudActive = true;
		try {

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "zip recruiter time/zip_recruiter_time.txt"));
				String line = bufferedReader.readLine();
				String[] spiltedArray = line.split("\\|");
				// SettingsClass.zipRecuiterStartTimeHours = spiltedArray;
				MainWhiteLabelClass.mReadFeedOrderModel.zipRecuiterStartTimeHours = spiltedArray;
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.zipRecuiterStartTimeHours = spiltedArray;

				line = bufferedReader.readLine();

				if (line != null) {
					spiltedArray = line.split("\\|");
					if (spiltedArray[1].toString().equalsIgnoreCase("0"))
						isCloudActive = false;
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "score_base_feed/" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "_score_base_feeds.json"));
				String line = bufferedReader.readLine();
				String data = "";
				while (line != null) {
					data += line;
					line = bufferedReader.readLine();
				}

				List<ScoreBaseFeedModel> asList = Arrays.asList(new Gson().fromJson(data, ScoreBaseFeedModel[].class));

				for (ScoreBaseFeedModel mScoreBaseFeedModel : asList) {
					MainWhiteLabelClass.mReadFeedOrderModel.scoreBaseFeedModelList.put(mScoreBaseFeedModel.getName(), mScoreBaseFeedModel);
					MainWhiteLabelClass.mOverlappedReadFeedOrderModel.scoreBaseFeedModelList.put(mScoreBaseFeedModel.getName(), mScoreBaseFeedModel);
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "score_base_feed/rotate_cpc_score.txt"));
				String line = bufferedReader.readLine();
				line = bufferedReader.readLine();
				String[] split = line.split("\\|");
				MainWhiteLabelClass.mReadFeedOrderModel.rotateCpcScore = split[1];

				if (split[1].equalsIgnoreCase("0"))
					MainWhiteLabelClass.mOverlappedReadFeedOrderModel.rotateCpcScore = "1";
				else
					MainWhiteLabelClass.mOverlappedReadFeedOrderModel.rotateCpcScore = "0";

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;

			// ====================================================================================

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "Java_process_stats/" + SettingsClass.queuName + "_size.txt"));

				// read version line(first line)
				String line1 = null;

				line1 = bufferedReader.readLine();

				while (line1 != null) {

					String[] totalAndDomainChildMemchahe = line1.split("\\|");

					// TotalUserscount|Domain|Sendgrid
					// 125055|Oak|global

					// TotalUserscount|Domain|Sendgrid
					// TotalUserscount|Domain|Sendgrid
					// TotalUserscount|Domain|Sendgrid

					// Main parent Main classs...
					if (totalAndDomainChildMemchahe[2].toLowerCase().contains("global")) {
						// SettingsClass.dashboradFileName =
						// totalAndDomainChildMemchahe[2].trim().replace("_global",
						// "");

						String Globalkey = totalAndDomainChildMemchahe[2].replace(" ", "").trim();
						SettingsClass.totalUsersCount = totalAndDomainChildMemchahe[0].replace(" ", "").trim();

						SettingsClass.totalUserKey = Globalkey + SettingsClass.totalUserKey;
						SettingsClass.processStartTime = Globalkey + SettingsClass.processStartTime;
						SettingsClass.userProcessed = Globalkey + SettingsClass.userProcessed;
						SettingsClass.emailSend = Globalkey + SettingsClass.emailSend;
						SettingsClass.duplicateUserKey = Globalkey + SettingsClass.duplicateUserKey;
						SettingsClass.perSecondsMails = Globalkey + SettingsClass.perSecondsMails;
						SettingsClass.processCurrentExecutionTime = Globalkey + SettingsClass.processCurrentExecutionTime;
						SettingsClass.cloudSearchUsersPer = Globalkey + SettingsClass.cloudSearchUsersPer;
						SettingsClass.mailCloudJobsPer = Globalkey + SettingsClass.mailCloudJobsPer;
						SettingsClass.perSecondUsers = Globalkey + SettingsClass.perSecondUsers;
						SettingsClass.cloudSearchUsersPer = Globalkey + SettingsClass.cloudSearchUsersPer;
						SettingsClass.average_percentage_NewJobsMailsKey = Globalkey + SettingsClass.average_percentage_NewJobsMailsKey;
						SettingsClass.numberofNewJobMailsKey = Globalkey + SettingsClass.numberofNewJobMailsKey;
						SettingsClass.jobsCountInMails = Globalkey + SettingsClass.jobsCountInMails;
						SettingsClass.JobProvidersjobsCountKey = Globalkey + SettingsClass.JobProvidersjobsCountKey;
						SettingsClass.groupTemplatesCountKey = Globalkey + SettingsClass.groupTemplatesCountKey;
						SettingsClass.elasticDuplicateMemcacheKey = Globalkey + SettingsClass.elasticDuplicateMemcacheKey;

						try {
							SettingsClass.memcacheObj.set(SettingsClass.totalUserKey, 0, SettingsClass.totalUsersCount);
						} catch (Exception e) {

							e.printStackTrace();
						}
						// for group and templte category wise keys
						MainWhiteLabelClass.categoriesList.add(totalAndDomainChildMemchahe[4].replace(" ", "_").trim());
						MainWhiteLabelClass.domains.add(totalAndDomainChildMemchahe[1].trim());
						MainWhiteLabelClass.providersSet.add(totalAndDomainChildMemchahe[3].trim());
						MainWhiteLabelClass.categoryDomainWiseGroupTemplateStatsKeys.add(totalAndDomainChildMemchahe[1] + "_" + totalAndDomainChildMemchahe[4].replace(" ", "_").trim());

					} else {

						// we need to set up child memcahe keys ...Total

						String childKeys = totalAndDomainChildMemchahe[1].replace(" ", "").trim() + "_" + totalAndDomainChildMemchahe[2].replace(" ", "").trim();

						// this is the child mem key..
						MainWhiteLabelClass.childDomainMemCacheArrayList.add(childKeys);

						// System.out.println("\n\n\n\n\n\n childKeys :" +
						// childKeys);

						try {
							// here we set the total count values of all child
							// domain processes..
							// setttng the totoal users count for the child
							// process in the map
							synchronized (SettingsClass.child_Process_stats_hashMap) {
								SettingsClass.memcacheObj.set(childKeys + "_" + SettingsClass.totalUserKey, 0, totalAndDomainChildMemchahe[0].trim());
								SettingsClass.child_Process_stats_hashMap.put(childKeys + "_" + SettingsClass.totalUserKey, Integer.parseInt(totalAndDomainChildMemchahe[0].trim()));

							}
						} catch (Exception e) {

							e.printStackTrace();
						}
						// for group and templte category wise keys
						MainWhiteLabelClass.categoriesList.add(totalAndDomainChildMemchahe[2].replace(" ", "_").trim());
						MainWhiteLabelClass.domains.add(totalAndDomainChildMemchahe[1].trim());
						MainWhiteLabelClass.providersSet.add(totalAndDomainChildMemchahe[3].trim());
						MainWhiteLabelClass.categoryDomainWiseGroupTemplateStatsKeys.add(totalAndDomainChildMemchahe[1] + "_" + totalAndDomainChildMemchahe[2].replace(" ", "_").trim());
						//
						// adding file name in the list of subject list
						SettingsClass.child_subject_From_File_Names.put(childKeys, totalAndDomainChildMemchahe[4]);
						SettingsClass.child_postal_addresses.put(childKeys, totalAndDomainChildMemchahe[5]);
						SettingsClass.child_process_arbor_pixels.put(childKeys, totalAndDomainChildMemchahe[6]);

					}

					line1 = bufferedReader.readLine();
					// reading the subject line from files
				}
				readAllSubjectLines();
			} catch (Exception e2) {
				e2.printStackTrace();
				try {
					String totalusers = (String) SettingsClass.memcacheObj.get(SettingsClass.totalUserKey);
					if (totalusers != null && SettingsClass.totalUsersCount.equalsIgnoreCase(""))
						SettingsClass.totalUsersCount = totalusers;
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			bufferedReader = null;
			String searchEndpoint = "";
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.domainConfigfilePath));
				String line1 = null;
				line1 = bufferedReader.readLine();
				String Data[] = line1.toString().split("\\|");
				System.out.println("Search endoint : " + Data[1].toString());
				searchEndpoint = Data[1].toString();

			} catch (Exception e2) {
				try {
					searchEndpoint = MainWhiteLabelClass.getSearchEndPoint();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (isCloudActive) {
				MainWhiteLabelClass.mReadFeedOrderModel.searchEndpoint = searchEndpoint;
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.searchEndpoint = searchEndpoint;
			} else {
				MainWhiteLabelClass.mReadFeedOrderModel.searchEndpoint = "";
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.searchEndpoint = "";
			}

			bufferedReader = null;
			ArrayList<GroupObject> tempGroupSubjectLineList = new ArrayList<GroupObject>();
			ArrayList<TemplateObject> tempTemplateList = new ArrayList<TemplateObject>();
			try {

				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "subjectAndFromName/" + MainWhiteLabelClass.parameterModelClassObject.getSubjectsFileName() + ".txt"));

				tempGroupSubjectLineList = new Gson().fromJson(bufferedReader, new TypeToken<List<GroupObject>>() {
				}.getType());

			} catch (Exception e) {

			}

			for (int i = 1; i <= 6; i++) {

				if (i == 1 || i == 4) {
					TemplateObject object = new TemplateObject();
					object.setActive(1);
					object.setTemplateId(i);
					tempTemplateList.add(object);
				}

			}

			if (SettingsClass.dashboradFileName.toLowerCase().contains("torchalerts")) {
				TemplateObject object = new TemplateObject();
				object.setActive(1);
				object.setTemplateId(7);
				tempTemplateList.add(object);
			}

			// add active group ids.............................

			for (int i = 0; i < tempGroupSubjectLineList.size(); i++) {

				if (tempGroupSubjectLineList.get(i).getActive().equals("1")) {
					SettingsClass.groupObjectList.add(tempGroupSubjectLineList.get(i));
				}

			}

			// add active template only
			for (int i = 0; i < tempTemplateList.size(); i++) {

				if (tempTemplateList.get(i).getActive() == 1) {
					SettingsClass.templateObjectList.add(tempTemplateList.get(i));
				}

			}

			// =======================================================================================

			while (true) {

				if (MainWhiteLabelClass.parameterModelClassObject.isForEveningAlerts())
					SettingsClass.feedOrderFolderName = "EveningFeedOrderFiles";
				else
					SettingsClass.feedOrderFolderName = "FeedOrderFilesForDemo";

				// }
				try {

					overlappedUserFeedOrder();

					bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + SettingsClass.feedOrderFolderName + "/"
							+ MainWhiteLabelClass.parameterModelClassObject.getFeedOrderFileName() + ".txt"));

					// read version line(first line)
					String line = bufferedReader.readLine();
					String textmsg = "";
					String version = "";

					String mailText = "";

					boolean isVersionChange = false;

					if (!preVersion.equalsIgnoreCase(line.toString())) {
						version = line.toString();

						SettingsClass.feedOrderList.clear();
						MainWhiteLabelClass.mReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.clear();
						MainWhiteLabelClass.mReadFeedOrderModel.alwaysOnTopFeedsString = "";
						MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryList.clear();

						isVersionChange = true;
					}

					line = bufferedReader.readLine();
					line = bufferedReader.readLine();

					textmsg += line.toString() + "<br/>";

					line = bufferedReader.readLine();

					while (line != null) {
						textmsg += line.toString() + "<br/>";

						String[] spiltedArray = line.split("\\|");
						try {
							// if (SettingClass.apiFeedJobscountFlag) {
							List<String> sourceList = SettingsClass.apiFeedOrderToDbNameMatchMap.get(spiltedArray[0].trim());
							if (sourceList != null) {
								for (String source : sourceList) {
									SettingsClass.feedApiListForProviderWiseSourceCountStats.add(source);
								}
							} else if (sourceList == null) {
								if (MainWhiteLabelClass.parameterModelClassObject.getDashboardName().toLowerCase().contains("nexgoal") && spiltedArray[0].trim().equalsIgnoreCase("NexGoal"))
									SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim() + " Direct");
								else

									SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim());
							}
							// }
						} catch (Exception e) {
							SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim());
						} // add feed order name into list only if it is
							// active

						if (isVersionChange) {
							if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

								if (spiltedArray[11].toString().trim().equalsIgnoreCase("1")) {
									MainWhiteLabelClass.mReadFeedOrderModel.alwaysOnTopFeedsString += "# " + spiltedArray[0].toString().trim() + " #";
									if (!spiltedArray[0].toString().trim().toLowerCase().contains("api"))
										MainWhiteLabelClass.mReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());
								}
								MainWhiteLabelClass.mReadFeedOrderModel.maxJobsHashmap.put(spiltedArray[0].toString().trim(), Integer.parseInt(spiltedArray[3].toString().trim()));
								SettingsClass.feedOrderList.add(spiltedArray[0].toString().trim());

								if (!spiltedArray[0].toString().trim().equalsIgnoreCase("Uber") && !spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
									if (SettingsClass.dashboradFileName.toLowerCase().contains("topusa") && spiltedArray[0].contains("TopUSAJobs"))
										MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0]);
									else if (SettingsClass.dashboradFileName.toLowerCase().contains("college") && spiltedArray[0].toString().trim().contains("CollegeRecruiter"))
										MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
									else if (SettingsClass.dashboradFileName.toLowerCase().contains("nexgoal") && spiltedArray[0].toString().trim().contains("NexGoal"))
										MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
									else if (SettingsClass.dashboradFileName.toLowerCase().contains("linkus") && spiltedArray[0].toString().trim().contains("Linkus"))
										MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
									else
										MainWhiteLabelClass.mReadFeedOrderModel.sourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());

								}

							}
						}

						if (spiltedArray[1].toString().trim().equalsIgnoreCase("1") && spiltedArray[0].toString().trim().toLowerCase().contains("api")) {

							try {
								if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
									MainWhiteLabelClass.mReadFeedOrderModel.ApiCPCHashMap.put(
											spiltedArray[0].toString().toLowerCase(),
											spiltedArray[5].trim() + "|" + spiltedArray[6].trim() + "|" + spiltedArray[12].trim() + "|" + spiltedArray[13].trim() + "|" + spiltedArray[14].trim() + "|"
													+ spiltedArray[15].trim() + "|" + spiltedArray[16].trim() + "|" + spiltedArray[17].trim() + "|" + spiltedArray[18].trim() + "|"
													+ spiltedArray[19].trim() + "|" + spiltedArray[20].trim() + "|" + spiltedArray[21].trim() + "|" + spiltedArray[7].trim().replace("null", "0") + "|"
													+ spiltedArray[8].trim().replace("null", "0") + "|" + spiltedArray[9].trim().replace("null", "0") + "|"
													+ spiltedArray[10].trim().replace("null", "0"));

								}
							} catch (Exception e) {
								e.printStackTrace();
							}

							ApiStatusModel apiStatusModelObject = null;

							apiStatusModelObject = MainWhiteLabelClass.mReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());

							if (apiStatusModelObject == null)
								apiStatusModelObject = new ApiStatusModel();

							apiStatusModelObject.setActive(true);

							Double responseTime = SettingsClass.apiResponseTimeHashmap.get(spiltedArray[0].toString().trim());

							Integer userProcessed = SettingsClass.apiUserProcessingCountHashmap.get(spiltedArray[0].toString().trim());

							if (responseTime == null)
								responseTime = 0.0;
							if (userProcessed == null)
								userProcessed = 0;

							if (responseTime / userProcessed > 1000 && !apiStatusModelObject.isStopped()) {
								apiStatusModelObject.setStopped(true);
								SettingsClass.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
								SettingsClass.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);

								mailText += "<br> " + spiltedArray[0].toString().trim() + " Has <font size='3' color='RED'> STOPED </font>  in <br>"
										+ MainWhiteLabelClass.parameterModelClassObject.getDashboardName();

								apiStatusModelObject.setStopTime(System.currentTimeMillis());
							} else if (apiStatusModelObject.isStopped() && ((System.currentTimeMillis() - apiStatusModelObject.getStopTime()) / (60 * 1000)) >= 10) {

								mailText += "<br> " + spiltedArray[0].toString().trim() + " Has <font size='3' color='green'> STARTED </font>  in <br>"
										+ MainWhiteLabelClass.parameterModelClassObject.getDashboardName();
								apiStatusModelObject.setStopped(false);
								apiStatusModelObject.setStopTime(0);
								SettingsClass.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
								SettingsClass.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);
							} else {
								apiStatusModelObject.setStopped(false);
								apiStatusModelObject.setStopTime(0);
							}
							MainWhiteLabelClass.mReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);

						}

						// new code for new api end

						line = bufferedReader.readLine();
					}

					// callZipAllApiCPC("zipAllApi_L",
					// MainWhiteLabelClass.zipAllApi_L);
					// callZipAllApiCPC("zipAllApi_M",
					// MainWhiteLabelClass.zipAllApi_M);
					// callZipAllApiCPC("zipAllApi_H",
					// MainWhiteLabelClass.zipAllApi_H);

					// if (!mailText.equalsIgnoreCase("")) {
					// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(mailText,
					// "settings");
					// }

					if (isVersionChange) {
						// send mail to tell if SettingsClass has changed
						// if (!preVersion.equalsIgnoreCase("")) {
						// try {
						// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(textmsg,
						// "settings");
						// } catch (Exception e) {
						// e.printStackTrace();
						// }
						// }
						textmsg = "";
						preVersion = version;
					}

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				try {
					Thread.sleep(5 * 60 * 1000);
				} catch (InterruptedException e) {
					//
					e.printStackTrace();
				} // 3 minutes sleep

			}
		} catch (Exception e) {

		}

	}

	private void readAllSubjectLines() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		Iterator it = SettingsClass.child_subject_From_File_Names.entrySet().iterator();

		while (it.hasNext()) {

			Map.Entry pair = (Map.Entry) it.next();
			String fileName = pair.getValue().toString();
			String child_process_key = pair.getKey().toString();
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt"));

				ArrayList<GroupObject> localGroupList = new ArrayList<GroupObject>();

				localGroupList = new Gson().fromJson(bufferedReader, new TypeToken<List<GroupObject>>() {
				}.getType());

				for (int i = 0; i < localGroupList.size(); i++) {
					// here we are removing all the inactive subject lines
					if (localGroupList.get(i).getActive().equalsIgnoreCase("0")) {
						localGroupList.remove(localGroupList.get(i));
						i = i - 1;

					}

				}

				SettingsClass.child_Grouplist_map.put(child_process_key, localGroupList);

			} catch (Exception e) {

				Utility.sendTextSms("Subject From Name File not found " + SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt Please check...");

			}
			bufferedReader = null;
		}

	}

	public void overlappedUserFeedOrder() {
		BufferedReader bufferedReader = null;
		try {

			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + SettingsClass.feedOrderFolderName + "/Cylcon Overlap Feeders.txt"));

			// read version line(first line)
			String line = bufferedReader.readLine();
			String textmsg = "";
			String version = "";

			String mailText = "";

			boolean isVersionChange = false;

			if (!preVersionOverlapped.equalsIgnoreCase(line.toString())) {
				version = line.toString();

				SettingsClass.feedOrderList.clear();
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.clear();
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.alwaysOnTopFeedsString = "";
				MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryList.clear();

				isVersionChange = true;
			}

			line = bufferedReader.readLine();
			line = bufferedReader.readLine();

			textmsg += line.toString() + "<br/>";

			line = bufferedReader.readLine();

			while (line != null) {
				textmsg += line.toString() + "<br/>";

				String[] spiltedArray = line.split("\\|");

				if (isVersionChange) {
					if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

						if (spiltedArray[11].toString().trim().equalsIgnoreCase("1")) {
							MainWhiteLabelClass.mOverlappedReadFeedOrderModel.alwaysOnTopFeedsString += "# " + spiltedArray[0].toString().trim() + " #";
							if (!spiltedArray[0].toString().trim().toLowerCase().contains("api"))
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());
						}
						MainWhiteLabelClass.mOverlappedReadFeedOrderModel.maxJobsHashmap.put(spiltedArray[0].toString().trim(), Integer.parseInt(spiltedArray[3].toString().trim()));
						SettingsClass.feedOrderList.add(spiltedArray[0].toString().trim());

						if (!spiltedArray[0].toString().trim().equalsIgnoreCase("Uber") && !spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
							if (SettingsClass.dashboradFileName.toLowerCase().contains("topusa") && spiltedArray[0].contains("TopUSAJobs"))
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0]);
							else if (SettingsClass.dashboradFileName.toLowerCase().contains("college") && spiltedArray[0].toString().trim().contains("CollegeRecruiter"))
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
							else if (SettingsClass.dashboradFileName.toLowerCase().contains("nexgoal") && spiltedArray[0].toString().trim().contains("NexGoal"))
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
							else if (SettingsClass.dashboradFileName.toLowerCase().contains("linkus") && spiltedArray[0].toString().trim().contains("Linkus"))
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
							else
								MainWhiteLabelClass.mOverlappedReadFeedOrderModel.sourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());

						}

					}
				}

				if (spiltedArray[1].toString().trim().equalsIgnoreCase("1") && spiltedArray[0].toString().trim().toLowerCase().contains("api")) {

					try {
						if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
							MainWhiteLabelClass.mOverlappedReadFeedOrderModel.ApiCPCHashMap.put(spiltedArray[0].toString().toLowerCase(), spiltedArray[5].trim() + "|" + spiltedArray[6].trim() + "|"
									+ spiltedArray[12].trim() + "|" + spiltedArray[13].trim() + "|" + spiltedArray[14].trim() + "|" + spiltedArray[15].trim() + "|" + spiltedArray[16].trim() + "|"
									+ spiltedArray[17].trim() + "|" + spiltedArray[18].trim() + "|" + spiltedArray[19].trim() + "|" + spiltedArray[20].trim() + "|" + spiltedArray[21].trim() + "|"
									+ spiltedArray[7].trim().replace("null", "0") + "|" + spiltedArray[8].trim().replace("null", "0") + "|" + spiltedArray[9].trim().replace("null", "0") + "|"
									+ spiltedArray[10].trim().replace("null", "0"));

						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					ApiStatusModel apiStatusModelObject = null;

					apiStatusModelObject = MainWhiteLabelClass.mOverlappedReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());

					if (apiStatusModelObject == null)
						apiStatusModelObject = new ApiStatusModel();

					apiStatusModelObject.setActive(true);

					Double responseTime = SettingsClass.apiResponseTimeHashmap.get(spiltedArray[0].toString().trim());

					Integer userProcessed = SettingsClass.apiUserProcessingCountHashmap.get(spiltedArray[0].toString().trim());

					if (responseTime == null)
						responseTime = 0.0;
					if (userProcessed == null)
						userProcessed = 0;

					if (responseTime / userProcessed > 1000 && !apiStatusModelObject.isStopped()) {
						apiStatusModelObject.setStopped(true);
						SettingsClass.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
						SettingsClass.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);

						mailText += "<br> " + spiltedArray[0].toString().trim() + " Has <font size='3' color='RED'> STOPED </font>  in <br>"
								+ MainWhiteLabelClass.parameterModelClassObject.getDashboardName();

						apiStatusModelObject.setStopTime(System.currentTimeMillis());
					} else if (apiStatusModelObject.isStopped() && ((System.currentTimeMillis() - apiStatusModelObject.getStopTime()) / (60 * 1000)) >= 10) {

						mailText += "<br> " + spiltedArray[0].toString().trim() + " Has <font size='3' color='green'> STARTED </font>  in <br>"
								+ MainWhiteLabelClass.parameterModelClassObject.getDashboardName();
						apiStatusModelObject.setStopped(false);
						apiStatusModelObject.setStopTime(0);
						SettingsClass.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
						SettingsClass.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);
					} else {
						apiStatusModelObject.setStopped(false);
						apiStatusModelObject.setStopTime(0);
					}
					MainWhiteLabelClass.mOverlappedReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);

				}

				// new code for new api end

				line = bufferedReader.readLine();
			}

			// if (!mailText.equalsIgnoreCase("")) {
			// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(mailText,
			// "settings");
			// }

			if (isVersionChange) {
				// send mail to tell if SettingsClass has changed
				// if (!preVersionOverlapped.equalsIgnoreCase("")) {
				// try {
				// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(textmsg,
				// "settings");
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				// }
				textmsg = "";
				preVersionOverlapped = version;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
