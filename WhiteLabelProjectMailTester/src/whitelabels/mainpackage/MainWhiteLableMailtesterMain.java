package whitelabels.mainpackage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import net.spy.memcached.MemcachedClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

import com.amazonaws.auth.BasicAWSCredentials;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.main.mailtester.models.MailtesterGsonModel;
import com.main.model.MailTesterDomainModel;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

public class MainWhiteLableMailtesterMain {

	public static List<UsersData> mailtesterDomain_users_List;
	public static HashMap<String, Integer> mailtester_confirmation_map = new HashMap<String, Integer>();
	// for local
	// public static String filePath = "/home/signity/aws_stats/";

	// public static String filePath =
	// "/home/parveen/Desktop/aws_stats/Mailtester_users/mailtester_users_info.txt";

	// for orgignal on server
	public static String filePath = "/var/nfs-93/redirect/mis_logs/";

	static String name_Of_DashBoardFile_ForTesting = "";

	// static String name_Of_Email_forTesting = "andrewpippio@yahoo.com";

	static String name_Of_Email_forTesting = "";

	public static void main(String ar[]) {

		if (ar != null && ar.length > 0) {
			try {
				name_Of_DashBoardFile_ForTesting = ar[0];
			} catch (Exception e) {
				// TODO Auto-generated catch block
				name_Of_DashBoardFile_ForTesting = "";
			}
			try {
				name_Of_Email_forTesting = ar[1];
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}

		mailtesterDomain_users_List = new ArrayList<UsersData>();
		SettingsClass.emailSend = "mailtester";
		SettingsClass.duplicateUserKey = "mailtester";
		SettingsClass.perSecondsMails = "mailtester";
		SettingsClass.processCurrentExecutionTime = "mailtester";
		SettingsClass.cloudSearchUsersPer = "mailtester";
		SettingsClass.mailCloudJobsPer = "mailtester";
		SettingsClass.perSecondUsers = "mailtester";
		SettingsClass.cloudSearchUsersPer = "mailtester";
		SettingsClass.average_percentage_NewJobsMailsKey = "mailtester";
		SettingsClass.numberofNewJobMailsKey = "mailtester";
		SettingsClass.jobsCountInMails = "mailtester";
		SettingsClass.JobProvidersjobsCountKey = "mailtester";
		SettingsClass.groupTemplatesCountKey = "mailtester";

		SettingsClass.isForMailtester = true;
		SettingsClass.awsCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);

		if (name_Of_DashBoardFile_ForTesting.equalsIgnoreCase("")) {

			System.out.println("Enter the dashboard filename");
			Scanner sc = new Scanner(System.in);
			name_Of_DashBoardFile_ForTesting = sc.nextLine();
			startMailTestingProcess(name_Of_DashBoardFile_ForTesting);

		} else {
			startMailTestingProcess(name_Of_DashBoardFile_ForTesting);

		}

	}

	public static void startMailTestingProcess(String nameOfDomain) {
		Set<MailTesterDomainModel> domainSet = getDomainFromDataBase(nameOfDomain);
		ArrayList<UsersData> userList = readUserInfoFromUsers(filePath + "Mailtester_users/mailtester_users_info.txt");
		// SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		// getting search end point
		MainWhiteLabelClass.getSearchEndPoint();

		MainWhiteLabelClass mainWhiteLabelClass;

		try {
			SettingsClass.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// ===============================iniatilizing the group and template
			// object======================================

		// ==============================================/==============================================================

		System.out.println("TOtal number of domain list=" + domainSet.size());

		for (MailTesterDomainModel domainModel : domainSet) {

			for (int j = 0; j < userList.size(); j++) {

				UsersData userDataObect = new UsersData();
				userDataObect.setKeyword(userList.get(j).getKeyword());
				userDataObect.setZipcode(userList.get(j).getZipcode());
				if (name_Of_Email_forTesting.equalsIgnoreCase("")) {
					userDataObect.setEmail("fbg-" + domainModel.domainName + System.currentTimeMillis() + "@mail-tester.com");
				} else {
					System.out.println(name_Of_Email_forTesting);
					userDataObect.setEmail(name_Of_Email_forTesting.trim());
				}
				// // userDataObect.setEmail("sparkposttestacct@gmail.com");
				// userDataObect.setEmail("pawan@signitysolutions.co.in");
				// userDataObect.setEmail("jason@jasonstevens.ca");

				userDataObect.setSearchKeyword(userDataObect.getKeyword());
				// userDataObect = getLati_longForuser(userDataObect);
				userDataObect.domainName = domainModel.domainName;
				userDataObect.setAdvertiseMent("");
				userDataObect.setFirstName("");
				userDataObect.compainId = "";
				userDataObect.sendGridCategory = domainModel.SendGridCategoryName;
				userDataObect.fromDomainName = domainModel.FromEmailAddress;
				userDataObect.sendGridUserName = domainModel.SendgridUsername;
				userDataObect.sendGridUserPassword = domainModel.SendgridPassword;
				userDataObect.mail_tester_whiteLableName = domainModel.whiteLableName;

				userDataObect.sparkpost_key = domainModel.sparkpost_key;

				userDataObect.sparkpost_bounce_domain = domainModel.sparkpost_bounce_domain;

				userDataObect.child_process_key = "mailtester";
				initializeTheSubjectAndFromList(domainModel, userDataObect);

				try {

					// SettingsClass.memcacheObj.set(userDataObect.email.replaceAll(" ",
					// "") + "_"
					// + userDataObect.keyword.replaceAll(" ", "") + "_"
					// + userDataObect.zipcode.replaceAll(" ", "") + "_"
					// + SettingsClass.queuName, 0, "0");

				} catch (Exception e1) {

					e1.printStackTrace();
				}

				MainWhiteLabelClass.mainClassObject = new MainWhiteLabelClass();
				MainWhiteLabelClass.parameterModelClassObject = MainWhiteLabelClass.mainClassObject.readDataFromDb(domainModel.dashBoardFile);

				new SettingsClass();

				List<Jobs> jobsList = null;
				try {
					jobsList = getJobCount(userDataObect);
					System.out.println("size of jobslist=" + jobsList.size());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AtomicInteger localGroupThreadCount = new AtomicInteger(0);
				AtomicInteger localTemplateThreadCount = new AtomicInteger(0);

				if (jobsList != null && jobsList.size() > 0) {
					if (localTemplateThreadCount.get() >= SettingsClass.templateObjectList.size()) {
						localTemplateThreadCount.set(0);

						// increment group id count
						localGroupThreadCount.getAndIncrement();

						if (localGroupThreadCount.get() >= SettingsClass.groupObjectList.size())
							localGroupThreadCount.set(0);
					}
					System.out.println("create job format");
					MainWhiteLabelClass.mainClassObject.createJobsFormat(jobsList, userDataObect, localTemplateThreadCount.get(), localGroupThreadCount.get());
					localTemplateThreadCount.getAndIncrement();

				}

				System.out.println(userDataObect.mail_tester_whiteLableName);
				// JobSearchThread jobsearch = new JobSearchThread();
				// // jobsearch.callJobPortalApis(userDataObect);

			}

		}
		// here we insert once for the mailtester process in the
		// table================================================
		System.out.println("size of list=" + MainWhiteLableMailtesterMain.mailtesterDomain_users_List.size());
		System.out.println("Getting mailtester rating");

		try {
			Thread.sleep(300000);
		} catch (Exception e) {

		}
		for (int i = 0; i < MainWhiteLableMailtesterMain.mailtesterDomain_users_List.size(); i++) {
			UsersData user = MainWhiteLableMailtesterMain.mailtesterDomain_users_List.get(i);
			float razor_final_value = 0;
			MailtesterGsonModel mailtestGson = getMailtesterRatingFromApiLink(user.mail_tester_api_link);
			String Razor_cf_range_51_100;
			try {
//				System.out.println("mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_51_100()" + mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_51_100().getScore());
				Razor_cf_range_51_100 = mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_51_100().getScore();
			} catch (Exception e4) {
				// TODO Auto-generated catch block
				Razor_cf_range_51_100 = "0.0";
			}
			String Razor_cf_range_e8_51_100;
			try {
//				System.out.println("mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_E8_51_100().getScore();" + mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_E8_51_100().getScore());
				Razor_cf_range_e8_51_100 = mailtestGson.getSpamAssassin().getRules().getRAZOR2_CF_RANGE_E8_51_100().getScore();
			} catch (Exception e4) {
				// TODO Auto-generated catch block
				Razor_cf_range_e8_51_100 = "0.0";
			}
			String Razor_check;
			try {
//				System.out.println("mailtestGson.getSpamAssassin().getRules().getRAZOR2_CHECK().getScore();" + mailtestGson.getSpamAssassin().getRules().getRAZOR2_CHECK().getScore());
				Razor_check = mailtestGson.getSpamAssassin().getRules().getRAZOR2_CHECK().getScore();
			} catch (Exception e4) {
				// TODO Auto-generated catch block
				Razor_check = "0.0";
			}

			try {
				razor_final_value = Float.parseFloat(Razor_cf_range_51_100) + Float.parseFloat(Razor_cf_range_e8_51_100) + Float.parseFloat(Razor_check);
			} catch (NumberFormatException e4) {
				// TODO Auto-generated catch block
				razor_final_value = 0;
			}

			try {
				user.mail_tester_rating = mailtestGson.getDisplayedMark();
				user.mail_tester_internal_rating_forSort = user.mail_tester_rating.split("/")[0];

			} catch (Exception e3) {
				// TODO Auto-generated catch block
				user.mail_tester_rating = "0";
			}
			try {
				user.razor_value = "" + razor_final_value;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				user.mail_tester_signature_mark = "0";
			}
			Double rating_without_razor = 0.0;
			try {
				rating_without_razor = Math.abs(Double.parseDouble(user.mail_tester_internal_rating_forSort)) + Math.abs(Double.parseDouble(user.razor_value));
				DecimalFormat df = new DecimalFormat("#.000");
//				System.out.print(df.format(rating_without_razor));

				user.rating_without_razor = "" + df.format(rating_without_razor);
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				user.rating_without_razor = "" + rating_without_razor;
			}

			try {
				user.mail_tester_blacklists_mark = "" + mailtestGson.getBlacklists().getMark();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				user.mail_tester_blacklists_mark = "0";
			}
			try {
				user.mail_tester_body_mark = "" + mailtestGson.getBody().getMark();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				user.mail_tester_body_mark = "0";
			}
			try {
				user.mail_tester_brokenLinks_mark = "" + mailtestGson.getLinks().getMark();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				user.mail_tester_brokenLinks_mark = "0";
			}
			try {
				user.mail_tester_spamAssassin_mark = "" + mailtestGson.getSpamAssassin().getMark();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				user.mail_tester_spamAssassin_mark = "0";
			}
			try {
				user.mail_tester_signature_mark = "" + mailtestGson.getSignature().getMark();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				user.mail_tester_signature_mark = "0";
			}

		}
//		System.out.println("Getting mailtester rating done");
		if (name_Of_Email_forTesting.equalsIgnoreCase("")) {
			insertDataInTheMailtester_table(mailtesterDomain_users_List);
		}
		try {
			if (SettingsClass.con != null)
				SettingsClass.con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.exit(0);

	}

	public static ArrayList<UsersData> readUserInfoFromUsers(String filePath) {
		ArrayList<UsersData> list = new ArrayList<UsersData>();

		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));

			// read version line(first line)
			String line1 = null;

			line1 = bufferedReader.readLine();
			while (line1 != null) {

				String[] userString = line1.split("\\|");
				UsersData userData = new UsersData();
				userData.setKeyword(userString[0].trim());
				userData.setZipcode(userString[1].trim());
				list.add(userData);

				line1 = bufferedReader.readLine();
			}
			return list;

		} catch (Exception e) {

		}

		return list;
	}

	public static Set<MailTesterDomainModel> getDomainFromDataBase(String nameOfDomain) {
		Set<MailTesterDomainModel> set = new HashSet<MailTesterDomainModel>();

		String query = "";
		if (nameOfDomain.equalsIgnoreCase("all")) {
			query = "select * from whitelabels_process where dashboardName not like '%alumni%' and dashboardName not like '%college%' and SubjectsFileName!=''";

		} else {

			query = "select * from whitelabels_process where dashboardName ='" + nameOfDomain + "' and  SubjectsFileName!=''";

		}

//		System.out.println(query);

		try {

			Class.forName("com.mysql.jdbc.Driver");
			if (SettingsClass.con == null)
				SettingsClass.con = DriverManager.getConnection(SettingsClass.url, SettingsClass.username, SettingsClass.password);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
		}

		Statement st;
		try {
			st = SettingsClass.con.createStatement();
			ResultSet res = st.executeQuery(query);

			while (res != null && res.next()) {
				MailTesterDomainModel model = new MailTesterDomainModel();
				model.emailClient = res.getString("emailclient");
				model.dashBoardFile = res.getString("DashboardName");
				model.FromEmailAddress = res.getString("FromEmailAddress");
				model.SendgridUsername = res.getString("SendgridUsername");
				model.SendgridPassword = res.getString("SendgridPassword");
				model.whiteLableName = res.getString("whitelabel_name");
				model.SendGridCategoryName = res.getString("SendGridCategoryName");
				model.SubjectsFileName = res.getString("SubjectsFileName");
				model.PostalAddress = res.getString("PostalAddress");
				model.sparkpost_key = res.getString("sparkpost_key");
				model.sparkpost_bounce_domain = res.getString("sparkpost_bounce_domain");

				if (model.emailClient.equalsIgnoreCase("mailgun")) {
					model.domainName = res.getString("MailgunDomainName");

				} else {
					model.domainName = res.getString("domainurl");

				}
				set.add(model);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return set;

	}

	public static UsersData getLati_longForuser(UsersData userData) {

		String query = "select latitude ,Longitude,primary_city,state from tbl_zipcodes where zip='" + userData.getZipcode() + "' limit 1";
//		System.out.println(query);

		try {
			Class.forName("com.mysql.jdbc.Driver");
			if (SettingsClass.con == null)
				SettingsClass.con = DriverManager.getConnection(SettingsClass.url, SettingsClass.username, SettingsClass.password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}

		Statement st;
		try {
			st = SettingsClass.con.createStatement();
			ResultSet res = st.executeQuery(query);
			if (res != null && res.next()) {
				Double R = (double) 6371; // earth radius in km
				double radius = 30; // km
				try {
					userData.latitude = res.getString("latitude");
				} catch (Exception e) {
					// latitude = 0;
				}

				try {
					userData.longitude = res.getString("Longitude");
				} catch (Exception e) {
					// longitude = 0;
				}

				try {
					userData.city = res.getString("primary_city");
				} catch (Exception e) {
					// longitude = 0;
				}
				try {
					userData.state = res.getString("state");
				} catch (Exception e) {
					// longitude = 0;
				}
				try {
					if (!userData.latitude.equalsIgnoreCase("0") && !userData.longitude.equalsIgnoreCase("0")) {
						// real
						// get the upper and lower bounds lat-long
						userData.upperLatitude = "" + (Double.parseDouble(userData.latitude) + Math.toDegrees(radius / R));
						userData.upperLongitude = "" + (Double.parseDouble(userData.longitude) - Math.toDegrees(radius / R / Math.cos(Math.toRadians(Double.parseDouble(userData.longitude)))));
						userData.lowerLongitude = "" + (Double.parseDouble(userData.longitude) + Math.toDegrees(radius / R / Math.cos(Math.toRadians(Double.parseDouble(userData.latitude)))));
						userData.lowerLatitude = "" + (Double.parseDouble(userData.latitude) - Math.toDegrees(radius / R));
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return userData;

	}

	public static MailtesterGsonModel getMailtesterRatingFromApiLink(String url) {

		try {
			URL website = new URL(url);
			URLConnection connection = website.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			StringBuilder response = new StringBuilder();
			String inputLine;

			while ((inputLine = in.readLine()) != null)
				response.append(inputLine);

			in.close();
			String temp = response.toString().replaceAll("\t", "");
			MailtesterGsonModel gsonModel = new Gson().fromJson(temp, MailtesterGsonModel.class);

			if (gsonModel != null) {
				return gsonModel;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void insertDataInTheMailtester_table(List<UsersData> mailtesterUsersList) {
		try {
			Collections.sort(mailtesterUsersList);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		String html_domainString_withnoRating = "<html><head><title>Domain with their mailtester mail</title></head>" + "<h1 align ='center'>Mailtester Report</h1>" + "<table cellspacing='0' cellpadding='12' border='0' width='700'>" + "<tbody style=text-align: center; width: 700px" + "<tr><th style='border-bottom: 2px solid #C35D5D;'>Domain</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Rating</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Status</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Spam Mark</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Signature Mark" + "</th><th style='border-bottom: 2px solid #C35D5D;'>Body Mark</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Blacklist Mark</th>"
				+ "<th style='border-bottom: 2px solid #C35D5D;'>Broken Links Mark</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Razor value</th>" + "<th style='border-bottom: 2px solid #C35D5D;'>Rating Without Razor</th>" + "</tr>";
		String query = "insert into tbl_mailtester (whitelable,domain,test_email,api_link,rating,debug_link,spam_mark,signature_mark,body_mark,blacklists_mark,broken_links_mark) values ";
//		System.out.println(query);
		String ratingStyle = "";
		for (int i = 0; i < mailtesterUsersList.size(); i++) {
			try {
				if ((int) Double.parseDouble(mailtesterUsersList.get(i).mail_tester_rating.split("\\/")[0]) < 7) {
					ratingStyle = "'color:#FF0000;'";
				} else {
					ratingStyle = "'color:#000000;'";
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			html_domainString_withnoRating = html_domainString_withnoRating + "<tr ><td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).domainName + "</td><td style='border-bottom: 1px solid black;' " + ratingStyle + "align ='center'>" + mailtesterUsersList.get(i).mail_tester_rating + "</td><td style='border-bottom: 1px solid black;' align ='center'><a href='" + mailtesterUsersList.get(i).mail_tester_debug_link + "'> click Here </a></td><td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).mail_tester_spamAssassin_mark + "</td>" + "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).mail_tester_signature_mark + "</td>"
					+ "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).mail_tester_body_mark + "</td>" + "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).mail_tester_blacklists_mark + "</td>" + "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).mail_tester_brokenLinks_mark + "</td>" + "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).razor_value + "</td>" + "<td style='border-bottom: 1px solid black;' align ='center'>" + mailtesterUsersList.get(i).rating_without_razor + "</td>" + "" + "</tr>";

			query = query + "('" + mailtesterUsersList.get(i).mail_tester_whiteLableName + "','" + mailtesterUsersList.get(i).domainName + "','" + mailtesterUsersList.get(i).email + "','" + mailtesterUsersList.get(i).mail_tester_api_link + "','" + mailtesterUsersList.get(i).mail_tester_rating + "','" + mailtesterUsersList.get(i).mail_tester_debug_link + "','" + mailtesterUsersList.get(i).mail_tester_spamAssassin_mark + "','" + mailtesterUsersList.get(i).mail_tester_signature_mark + "','" + mailtesterUsersList.get(i).mail_tester_body_mark + "','" + mailtesterUsersList.get(i).mail_tester_blacklists_mark + "','" + mailtesterUsersList.get(i).mail_tester_brokenLinks_mark + "'),";

		}

		html_domainString_withnoRating = html_domainString_withnoRating + "</tbody></table></html>";
		if (name_Of_DashBoardFile_ForTesting.equalsIgnoreCase("all")) {
			sendEmailJustForAlerts(html_domainString_withnoRating);

		} else {
			sendEmailJustForAlerts_forInternal(html_domainString_withnoRating);

		}

//		System.out.println(html_domainString_withnoRating);
		query = query.substring(0, query.length() - 1);

//		System.out.println(query);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			if (SettingsClass.con == null)
				SettingsClass.con = DriverManager.getConnection(SettingsClass.url, SettingsClass.username, SettingsClass.password);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

		Statement st;
		try {
			st = SettingsClass.con.createStatement();
			boolean res = st.execute(query);
			if (!res) {
				System.out.println("inserted successfully");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void sendEmailJustForAlerts_forInternal(String body) {

		String[] TestBcclist = new String[4];
		TestBcclist[0] = "navneet@signitysolutions.in";
		TestBcclist[1] = "nitika.chadha@signitysolutions.in";
		TestBcclist[2] = "kanav@signitysolutions.com";
		TestBcclist[3] = "rajinder@signitysolutions.com";

		SendGrid sendgrid;
		SendGrid.Email email;
		// create global sendgrid obj
		sendgrid = new SendGrid("oakjobscom", "fbg3002056-3");
		email = new SendGrid.Email();
		// sending mail from email id..
		email.setFrom("alerts@oakjobs.com");

		email.setSubject("Mailtester Report");

		email.setHtml(body);
		email.addTo("pawan@signitysolutions.co.in");
		email.addCc(TestBcclist);
		try {
			SendGrid.Response response = sendgrid.send(email);
			if (response.getStatus()) {
				System.out.println("send");
			}
		} catch (SendGridException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void sendEmailJustForAlerts(String body) {

		String[] TestBcclist = new String[5];
		TestBcclist[0] = "navneet@signitysolutions.in";
		TestBcclist[1] = "nitika.chadha@signitysolutions.in";
		TestBcclist[2] = "kanav@signitysolutions.com";
		TestBcclist[3] = "rajinder@signitysolutions.com";
		TestBcclist[4] = "jason@jasonstevens.ca";

		SendGrid sendgrid;
		SendGrid.Email email;
		// create global sendgrid obj
		sendgrid = new SendGrid("oakjobscom", "fbg3002056-3");
		email = new SendGrid.Email();
		// sending mail from email id..
		email.setFrom("alerts@oakjobs.com");

		email.setSubject("Mailtester Report");

		email.setHtml(body);
		email.addTo("pawan@signitysolutions.co.in");
		email.addCc(TestBcclist);
		try {
			SendGrid.Response response = sendgrid.send(email);
			if (response.getStatus()) {
				System.out.println("send");
			}
		} catch (SendGridException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static List<Jobs> getJobCount(UsersData userData) {

		String url = "http://s2.oakjobalerts.com:8080/JavaStaggingApi/?" + "keyword=" + userData.keyword.replace(" ", "%20%2B") + "&zipcode=" + userData.zipcode.replace(" ", "%20") + "&domain=" + userData.domainName + "&" + "offset=40";
		List<Jobs> jobsList = null;

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet request = new HttpGet(url);
		HttpResponse response;
		// FinalObject finalObject = null;
		try {
			response = httpclient.execute(request);
			String json = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONObject jsonObject = new JSONObject(json);

			if (jsonObject != null) {

				JSONArray jobsArray = jsonObject.getJSONArray("jobs");
				jobsList = Arrays.asList(new Gson().fromJson(jobsArray.toString(), Jobs[].class));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//
		return jobsList;

	}

	public static void initializeTheSubjectAndFromList(MailTesterDomainModel domainModel, UsersData userDataObject) {
		BufferedReader bufferedReader = null;
		SettingsClass.groupObjectList.clear();
		ArrayList<GroupObject> tempGroupSubjectLineList = new ArrayList<GroupObject>();
		ArrayList<TemplateObject> tempTemplateList = new ArrayList<TemplateObject>();
		try {

			if (SettingsClass.groupObjectList.size() != 0) {
				SettingsClass.groupObjectList.clear();
			}
			bufferedReader = new BufferedReader(new FileReader(filePath + "subjectAndFromName/" + domainModel.SubjectsFileName + ".txt"));

			tempGroupSubjectLineList = new Gson().fromJson(bufferedReader, new TypeToken<List<GroupObject>>() {
			}.getType());

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (SettingsClass.templateObjectList.size() == 0) {

			for (int i = 1; i <= 6; i++) {

				TemplateObject object = new TemplateObject();

				if (i == 1 || i == 4) {

					object.setActive(1);
					object.setTemplateId(i);

					tempTemplateList.add(object);
				}

			}
			// add active template only
			for (int i = 0; i < tempTemplateList.size(); i++) {

				if (tempTemplateList.get(i).getActive() == 1) {
					SettingsClass.templateObjectList.add(tempTemplateList.get(i));
				}

			}
		}

		// add active group ids.............................

		for (int i = 0; i < tempGroupSubjectLineList.size(); i++) {

			if (tempGroupSubjectLineList.get(i).getActive().equals("1")) {
				SettingsClass.groupObjectList.add(tempGroupSubjectLineList.get(i));
			}

		}

		SettingsClass.child_Grouplist_map.put(userDataObject.child_process_key, SettingsClass.groupObjectList);

		SettingsClass.child_postal_addresses.put(userDataObject.child_process_key, domainModel.PostalAddress);

	}

}
