package whitelabels.mainpackage;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

import net.spy.memcached.MemcachedClient;
import whitelabels.model.ApiStatusModel;
import whitelabels.model.GroupObject;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;

public class SettingsClass {

	public static int TOTAL_N0_THREAD = 1;
	public static boolean isThisTestRun = true;

	public static boolean runOrignalTest = false;
	public static int numberOfTestingMails = 2;

	public static String testingEmailId = "parveen.k@signitysolutions.in";

	// public static String testingEmailId = "abhishek.k@signitysolutions.in";

	// public static String testingEmailId = "web-oJ66TV@mail-tester.com";

	// public static String testingEmailId = "web-SuIrUB@mail-tester.com";

	public static boolean firstEmailFlag = true;

//	 public static String filePath = "/home/parveen/Desktop/aws_stats/";

	public static String filePath = "/home/signity/aws_stats/";

	public static String feedOrderFolderName = "FeedOrderFilesForDemo";

	static AWSCredentials awsCredentials;
	public static String awsAccessKey = "AKIAJFSBEF3PYJ7SNOCQ";
	public static String awsSecretKey = "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";
	static AmazonSQSClient sqs;
	static String baseQueurl = "https://sqs.us-east-1.amazonaws.com/306640124653/";
	static Region region = Region.getRegion(Regions.US_EAST_1);
	public static MemcachedClient memcacheObj;

	public static MemcachedClient elasticMemCacheObj;
	public static String elasticDublicateKey = "elasticDublicateKey";
	public static AtomicInteger duplicateUserFromElasticCache = new AtomicInteger(0);

	public static String totalUsersCount;
	public static Calendar cal;
	public static DecimalFormat decFormat = new DecimalFormat("###.######");

	public static String whitelabel_processname = "";

	public static String queuName;
	// memcache keys
	public static String dashboradFileName = "";
	public static String totalUserKey = "totalUserKey";
	public static String userProcessed = "userProcessed";
	public static String cloudSearchUsersPer = "cloudSearchUsersPer";
	public static String emailSend = "emailSend";
	public static String processStartTime = "processStartTime";
	public static String numberofNewJobMailsKey = "numberofNewJobMailsKey";
	public static String jobsCountInMails = "jobsCountInMails";
	public static String groupTemplatesCountKey = "groupTemplatesCountKey";
	public static String duplicateUserKey = "duplicateUserKey";
	public static String JobProvidersjobsCountKey = "JobProvidersjobsCountKey";
	public static String perSecondsMails = "perSecondsMails";
	public static String mailCloudJobsPer = "mailCloudJobsPer";
	public static String perSecondUsers = "perSecondUsers";
	public static String average_percentage_NewJobsMailsKey = "average_percentage_NewJobsMailsKey";
	public static String processCurrentExecutionTime = "processCurrentExecutionTime";

	// public static String userSourceNameForHtml;
	// path
	public static String domainConfigfilePath = "/var/nfs-93/redirect/mis_logs/Java_process_stats/rdsDomainConfig.txt";
	// public static String searchEndpoint = "";
	static String domainQueueUrl = "https://sqs.us-east-1.amazonaws.com/306640124653/rdsdomainConfig";

	public static boolean isProcesskilled = false;

	// public static String zipRecuiterStartTimeHours[] = null;

	public static ArrayList<GroupObject> groupObjectList = new ArrayList<GroupObject>();
	public static ArrayList<TemplateObject> templateObjectList = new ArrayList<TemplateObject>();
	public static ArrayList<String> feedOrderList = new ArrayList<String>();

	public static LinkedHashMap<String, ApiStatusModel> apiStatusHashmap = new LinkedHashMap<String, ApiStatusModel>();
	public static ConcurrentHashMap<String, Double> apiResponseTimeHashmap = new ConcurrentHashMap<String, Double>();
	public static ConcurrentHashMap<String, Integer> apiUserProcessingCountHashmap = new ConcurrentHashMap<String, Integer>();
	public static HashMap<String, Integer> templateCountHashMap = new HashMap<String, Integer>();

	public static float cloudSearchJobPerc = 0;
	public static int processesIsRunning = 0;

	// AtomicInteger variables
	public static AtomicInteger userProcessingCount = new AtomicInteger(0);
	public static AtomicInteger totalEmailCount = new AtomicInteger(0);
	public static AtomicInteger numberOfNewJobMails = new AtomicInteger(0);
	public static AtomicInteger user_CloudSearch_ProcessingCount = new AtomicInteger(0);
	public static AtomicInteger duplicateUser = new AtomicInteger(0);
	public static AtomicInteger newTotalEmailCount = new AtomicInteger(0);
	public static AtomicInteger emptyUserMessageCount = new AtomicInteger(0);
	public static AtomicInteger newuserProcessingCount = new AtomicInteger(0);
	public static AtomicInteger emptyQueueCount = new AtomicInteger(0);
	public static AtomicInteger csUserProcessingCount = new AtomicInteger(0);
	// public static AtomicInteger currentTemplateCount = new AtomicInteger(0);
	// public static AtomicInteger currentGroupCount = new AtomicInteger(0);
	public static AtomicInteger emailCount = new AtomicInteger(0);
	public static AtomicInteger BccMails = new AtomicInteger(0);

	public static AtomicInteger topResumeCounter = new AtomicInteger(1);

	public static AtomicIntegerArray jobsCountArray = new AtomicIntegerArray(40);

	public static double cloudSearchQueryResponseTime = 0;

	public static int bccSendEmailIntervalCount = 100001;
	public final static int BccTotalMails = 100;
	public static int bccSendEmailIntervalPercentage = 30;
	public static String currentTimeForDashBoard = "";
	public static SimpleDateFormat processTimeDateFormat = new SimpleDateFormat("hh:mm aaa");
	static String ipAddress = "52.6.190.27";
	public static int exitTime = 23;

	public static SimpleDateFormat dateFormetForSubjectLine = new SimpleDateFormat("MMM d, yyyy");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static String todayDate = "";

	SettingsClass() {

		feedApiListForProviderWiseSourceCountStats = new HashSet<String>();
		setupApiFeedToDbMap();

		try {
			if (!isThisTestRun && !runOrignalTest)
				TOTAL_N0_THREAD = Integer.parseInt(MainWhiteLabelClass.parameterModelClassObject.getThreadCount());
		} catch (Exception e) {
		}

		// String memKey =
		// MainWhiteLabelClass.parameterModelClassObject.getMemcacheKeyName() +
		// "_";
		// totalUserKey = memKey + totalUserKey;
		// userProcessed = memKey + userProcessed;
		// emailSend = memKey + emailSend;
		// perSecondsMails = memKey + perSecondsMails;
		// duplicateUserKey = memKey + duplicateUserKey;
		// processStartTime = memKey + processStartTime;
		// processCurrentExecutionTime = memKey + processCurrentExecutionTime;
		// perSecondUsers = memKey + perSecondUsers;
		// cloudSearchUsersPer = memKey + cloudSearchUsersPer;
		// mailCloudJobsPer = memKey + mailCloudJobsPer;
		// average_percentage_NewJobsMailsKey = memKey +
		// average_percentage_NewJobsMailsKey;
		// numberofNewJobMailsKey = memKey + numberofNewJobMailsKey;
		// JobProvidersjobsCountKey = memKey + JobProvidersjobsCountKey;
		// groupTemplatesCountKey = memKey + groupTemplatesCountKey;

		/*
		 * For Add To EMail Keywords List
		 */

		keywordList.add("Accounting");
		keywordList.add("General Business");
		keywordList.add("Other");
		keywordList.add("Admin Clerical");
		keywordList.add("General Labor");
		keywordList.add("Pharmaceutical");
		keywordList.add("Automotive");
		keywordList.add("Government");
		keywordList.add("Professional Services");
		keywordList.add("Banking");
		keywordList.add("Grocery");
		keywordList.add("Purchasing - Procurement");
		keywordList.add("Biotech");
		keywordList.add("Health Care");
		keywordList.add("QA - Quality Control");
		keywordList.add("Broadcast - Journalism");
		keywordList.add("Hotel - Hospitality");
		keywordList.add("Real Estate");
		keywordList.add("Business Development");
		keywordList.add("Human Resources");
		keywordList.add("Research");
		keywordList.add("Construction");
		keywordList.add("Information Technology");
		keywordList.add("Restaurant - Food Service");
		keywordList.add("Consultant");
		keywordList.add("Installation - Maint - Repair");
		keywordList.add("Retail");
		keywordList.add("Customer Service");
		keywordList.add("Insurance");
		keywordList.add("Sales");
		keywordList.add("Design");
		keywordList.add("Inventory");
		keywordList.add("Science");
		keywordList.add("Distribution - Shipping");
		keywordList.add("Legal");
		keywordList.add("Skilled Labor - Trades");
		keywordList.add("Education - Teaching");
		keywordList.add("Legal Admin");
		keywordList.add("Strategy - Planning");
		keywordList.add("Engineering");
		keywordList.add("Management");
		keywordList.add("Supply Chain");
		keywordList.add("Entry Level - New Grad");
		keywordList.add("Manufacturing");
		keywordList.add("Telecommunications");
		keywordList.add("Executive");
		keywordList.add("Marketing");
		keywordList.add("Training");
		keywordList.add("Facilities");
		keywordList.add("Media - Journalism - Newspaper");
		keywordList.add("Transportation");
		keywordList.add("Finance");
		keywordList.add("Nonprofit - Social Services");
		keywordList.add("Warehouse");
		keywordList.add("Franchise");
		keywordList.add("Nurse");

		queuName = MainWhiteLabelClass.parameterModelClassObject.getQueueName();
		whitelabel_processname = MainWhiteLabelClass.parameterModelClassObject.getWhitelabel_name();
		// userSourceNameForHtml =
		// MainWhiteLabelClass.parameterModelClassObject.getHtmlSourceName();
		dashboradFileName = MainWhiteLabelClass.parameterModelClassObject.getDashboardName();

		String suffix = "";

		Calendar calendar = new GregorianCalendar();

		int n = calendar.get(Calendar.DATE);

		switch (n % 10) {
		case 1:
			suffix = "st";
			break;
		case 2:
			suffix = "nd";
			break;
		case 3:
			suffix = "rd";
			break;
		default:
			suffix = "th";
			break;

		}

		if (n >= 11 && n <= 13) {
			suffix = "th";
		}

		SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE, MMMM dd'" + suffix + "'  yyyy");
		// SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE, dd MMM, yyyy");

		todayDate = sdf1.format(new Date());

		cal = Calendar.getInstance();

	}

	public final static String API = "a";
	public final static String CS = "c";
	public final static String EXACT = "e";
	public final static String SYNONYMorSIMPLE = "s";

	public static String ec2AwsAccessKey = "AKIAIJJZZVULVKZNE7RQ";
	public static String ec2AwsSecretKey = "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";

	// New server 98 access and secret key
	public static String ec2_98AwsAccessKey = "AKIAI6ISH4ZK4DMLB7UA";
	public static String ec2_98AwsSecretKey = "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";

	public static String server189InstanceId = "i-9ceb9534";
	public static String server62InstanceId = "i-5d9ceef5";
	public static String server7InstanceId = "i-d436b07c";
	public static String server235InstanceId = "i-3416539f";
	public static String server109InstanceId = "i-9db6a735";
	public static String server98InstanceId = "i-0c44166521af2ad95";

	public static String killserverFilePath = "/var/nfs-93/redirect/mis_logs/Java_process_stats/";

	// new stats variable added by pawan===================================
	public static AtomicInteger emptyJobsUrlCount = new AtomicInteger(0);
	public static String emptyJobsUrlStats = "";
	public static boolean emptyJobsUrlSmsAlertSentStatus = false;

	//
	// for combine process
	public static Map<String, Integer> child_Process_stats_hashMap = Collections.synchronizedMap(new HashMap<String, Integer>());
	public static HashMap<String, Integer> doaminCategoryWiseTemplateGroupIdMap = new HashMap<String, Integer>();
	public static LinkedHashMap<String, Integer> new_jobProviderCountDomainWise = new LinkedHashMap<String, Integer>();
	public static String new_jobProviderCountDomainWiseKey = "globalDomainCaterotyjobProviderCount";
	public static String new_DomainCaterotyWiseGroupTemplateStatedMemcacaheKey = "globalDomainCaterotyWiseGroupTemplateStatsMemcacaheKey";
	public static String queueName_instanceId = "";

	// new code of jobprovider source count by pawan
	public static Set<String> feedApiListForProviderWiseSourceCountStats;
	public static Map<String, List<String>> apiFeedOrderToDbNameMatchMap;

	public void setupApiFeedToDbMap() {

		apiFeedOrderToDbNameMatchMap = new HashMap<String, List<String>>();

		List<String> apiFeedList = new ArrayList<String>();
		apiFeedList.add("CareerBlissApi_med");
		apiFeedList.add("CareerBlissApi_low");
		apiFeedList.add("CareerBlissApi_high");
		apiFeedList.add("CareerBlissApi_lower");
		apiFeedList.add("CareerBlissApi_lowest");
		apiFeedOrderToDbNameMatchMap.put("CareerBlissApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("ZIPALLAPI_L");
		apiFeedList.add("ZIPALLAPI_M");
		apiFeedList.add("ZIPALLAPI_H");
		apiFeedList.add("ZIPALLAPI");
		apiFeedOrderToDbNameMatchMap.put("ZIPALLAPI", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Juju API");
		apiFeedOrderToDbNameMatchMap.put("JuJuApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Juju API");
		apiFeedOrderToDbNameMatchMap.put("JuJuExactApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Glassdoor API");
		apiFeedOrderToDbNameMatchMap.put("glassDoorApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Lead 5 media Api");
		apiFeedOrderToDbNameMatchMap.put("Lead 5 mediaApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Zip Recruiter NTL Sec API");
		apiFeedOrderToDbNameMatchMap.put("zipRecuriterNTLSecApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Zip Recruiter NTL Third API");
		apiFeedOrderToDbNameMatchMap.put("zipRecuriterNTLThirdApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Zip Recruiter NTL API");
		apiFeedOrderToDbNameMatchMap.put("zipRecuriterNTLApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Jobs2Careers API");
		apiFeedOrderToDbNameMatchMap.put("J2CExactApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("Zip Recruiter API");
		apiFeedOrderToDbNameMatchMap.put("zipRecuriterApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("J2C APIDE");
		apiFeedOrderToDbNameMatchMap.put("J2CIDEApi", apiFeedList);

		apiFeedList = new ArrayList<String>();
		apiFeedList.add("J2C Boolean");
		apiFeedOrderToDbNameMatchMap.put("J2CBooleanApi", apiFeedList);

	}

	public static boolean apiFeedJobscountFlag = true;
	public static boolean isForMailtester = false;
	static String DB_NAME = "oakalerts";
	static String username = "awsoakuser";
	static String password = "awsoakusersignity";
	static String HOST_NAME = "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";// FOR

	// ==============================================================================================
	static String url = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;

	public static Connection con;
	public static List<UsersData> userNotProcessedList = new ArrayList<UsersData>();
	public static AtomicInteger errorCount = new AtomicInteger(0);
	public static String elasticDuplicateMemcacheKey = "childElasticDuplicateMemcacheKey";
	public static List<String> keywordList = new ArrayList<String>();

	// subject line list

	public static HashMap<String, String> child_subject_From_File_Names = new HashMap<String, String>();

	public static HashMap<String, ArrayList<GroupObject>> child_Grouplist_map = new HashMap<String, ArrayList<GroupObject>>();

	public static HashMap<String, String> child_postal_addresses = new HashMap<String, String>();

	public static HashMap<String, String> child_process_arbor_pixels = new HashMap<String, String>();

}
