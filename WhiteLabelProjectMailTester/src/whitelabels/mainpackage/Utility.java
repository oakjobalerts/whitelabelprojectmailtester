package whitelabels.mainpackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;

import whitelabels.model.FinalGroupObject;
import whitelabels.model.GroupObject;
import whitelabels.model.JobProviderJobCountData;
import whitelabels.model.TemplateObject;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.util.EC2MetadataUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.main.model.Category;
import com.main.model.Domains;
import com.main.model.JobSourceCountModel;
import com.main.model.JobsDomain;
import com.main.model.Providers;

public class Utility {
	public static String userNotProcessedString() {

		String userNotProcessedString = "";
		for (int i = 0; i < SettingsClass.userNotProcessedList.size(); i++) {
			userNotProcessedString += SettingsClass.userNotProcessedList.get(i).errorMessage + "|" + SettingsClass.userNotProcessedList.get(i).getKeyword() + "|"
					+ SettingsClass.userNotProcessedList.get(i).getEmail() + "|" + SettingsClass.userNotProcessedList.get(i).getZipcode() + " | "
					+ SettingsClass.userNotProcessedList.get(i).scheduleSendForMailgun + "\n";
		}
		return userNotProcessedString;
	}

	public static void convertJobProviderJobCountJsontoMap(String bufferedReader) {

		JobProviderJobCountData JobProviderJobCountObj = new JobProviderJobCountData();
		JobProviderJobCountObj = new Gson().fromJson(bufferedReader, new TypeToken<JobProviderJobCountData>() {
		}.getType());
		try {
			MainWhiteLabelClass.jobProvidersJobCountMap.putAll(JobProviderJobCountObj.getSources());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void jobsCountInMail_toArray(String jobs_per) {

		String jobC_arr[] = jobs_per.split(",");

		for (int i = 0; i < jobC_arr.length; i++) {
			SettingsClass.jobsCountArray.set(i, Integer.parseInt(jobC_arr[i].trim()));
		}
	}

	public static void templateGsonToMap(String bufferedReader) {

		List<FinalGroupObject> finalGroupObjList = new ArrayList<FinalGroupObject>();
		finalGroupObjList = new Gson().fromJson(bufferedReader, new TypeToken<List<FinalGroupObject>>() {
		}.getType());

		try {
			for (int i = 0; i < finalGroupObjList.size(); i++) {
				for (int temp_id = 0; temp_id < finalGroupObjList.get(i).template.size(); temp_id++) {
					String key = finalGroupObjList.get(i).getGroup_id() + "_" + finalGroupObjList.get(i).template.get(temp_id).getTemplateId();
					SettingsClass.templateCountHashMap.put(key, finalGroupObjList.get(i).template.get(temp_id).getTemplateCount());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void groupTemplateIds_toMap(String group_template_count_string) {
		try {
			String group_template_count[] = group_template_count_string.split(",");
			for (int i = 0; i < group_template_count.length; i++) {
				try {

					// initialize template hash map
					SettingsClass.templateCountHashMap.put(group_template_count[i].split(":")[0], Integer.parseInt(group_template_count[i].split(":")[1]));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void providersjobsCount_toMap(String jobsCount) {
		String jobscount_arr[] = jobsCount.split(",");
		for (int i = 0; i < jobscount_arr.length; i++) {
			MainWhiteLabelClass.jobProvidersJobCountMap.put(SettingsClass.feedOrderList.get(i), Integer.parseInt(jobscount_arr[i].trim()));
		}

	}

	public static String jobsCountInMail_toString() {
		String result = SettingsClass.jobsCountArray.get(0) + "";
		for (int i = 1; i < SettingsClass.jobsCountArray.length(); i++) {
			result = result + "," + SettingsClass.jobsCountArray.get(i);
		}
		return result;
	}

	public static String providersjobsCount_toString() {
		String result = "";
		for (String feed : SettingsClass.feedOrderList) {

			if (MainWhiteLabelClass.jobProvidersJobCountMap.get(feed) != null) {
				if (result.equalsIgnoreCase("")) {
					result = MainWhiteLabelClass.jobProvidersJobCountMap.get(feed) + "";
				} else {
					result = result + "," + MainWhiteLabelClass.jobProvidersJobCountMap.get(feed);
				}
			} else {
				// no value exits corresponding to feedName
				if (result.equalsIgnoreCase("")) {
					result = 0 + "";
				} else {
					result = result + "," + 0;
				}
			}

		}

		return result;
	}

	public static String groupTemplateIds_toString() {
		String result = "";
		String key = "";
		Integer value = 0;
		try {
			for (int i = 0; i < SettingsClass.groupObjectList.size(); i++) {
				for (int j = 0; j < SettingsClass.templateObjectList.size(); j++) {
					key = SettingsClass.groupObjectList.get(i).getGroupId() + "_" + SettingsClass.templateObjectList.get(j).getTemplateId();
					try {
						if (SettingsClass.templateCountHashMap.get(key) == null) {
							value = 0;
						} else {
							value = SettingsClass.templateCountHashMap.get(key);
						}
						result += key + ":" + value + ",";
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return result.substring(0, result.length() - 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";

	}

	public static synchronized void killProcess() {

		if (!SettingsClass.isProcesskilled) {
			SettingsClass.isProcesskilled = true;
			// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(SettingsClass.dashboradFileName
			// + " has been completed successfully", "stop");
			Utility.stopServer();
			System.exit(0);

		}

	}

	public static void stopServer() {

		AmazonEC2 ec2;
		AWSCredentials ec2AwsCredentials;
		boolean stopServer = false;
		int processCount = 2;

		try {
			String instanceId = EC2MetadataUtils.getInstanceId();
			System.out.println(" \ninstanceId..." + instanceId);

			if (instanceId.contains(SettingsClass.server98InstanceId)) {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2_98AwsAccessKey, SettingsClass.ec2_98AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
				instanceId = EC2MetadataUtils.getInstanceId();
			} else {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2AwsAccessKey, SettingsClass.ec2AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
			}

			String fileName = "";
			if (instanceId.contains(SettingsClass.server189InstanceId)) {
				fileName = "server189_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server62InstanceId)) {
				fileName = "server62_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server7InstanceId)) {
				fileName = "server7_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server235InstanceId)) {
				fileName = "server235_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server109InstanceId)) {
				fileName = "server109_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server98InstanceId)) {
				fileName = "server98_on_off_stats.txt";
			}

			BufferedReader bufferedReader = null;

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.killserverFilePath + fileName));

				// bufferedReader = new BufferedReader(new
				// FileReader("/home/sandeep/Desktop/Drive_D/AWS
				// jars/july/"+fileName));

				processCount = Integer.parseInt(bufferedReader.readLine());

				System.out.println(processCount + "");

				if (processCount == 0) {
					stopServer = true;
				} else {
					// decrease process count by 1
					processCount = processCount - 1;

					if (processCount == 0) {
						stopServer = true;
					}
				}

				bufferedReader = null;

			} catch (Exception e2) {
				//
				e2.printStackTrace();
			}

			if ((instanceId.contains(SettingsClass.server189InstanceId) || instanceId.contains(SettingsClass.server62InstanceId) || instanceId.contains(SettingsClass.server7InstanceId)
					|| instanceId.contains(SettingsClass.server235InstanceId) || instanceId.contains(SettingsClass.server109InstanceId) || instanceId.contains(SettingsClass.server98InstanceId))
					&& stopServer) {

				System.out.println(" \n Entered in block of stop server");

				// Stop Ec2 Instance
				StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instanceId);
				StopInstancesResult stopResult = ec2.stopInstances(stopRequest);

				System.out.println(" \nstopResult..." + stopResult);
			}
			// update process count in file

			BufferedWriter bufferedWriter = null;
			try {
				bufferedWriter = new BufferedWriter(new FileWriter(SettingsClass.killserverFilePath + fileName));

				bufferedWriter.write(String.valueOf(processCount));

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					bufferedWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (AmazonServiceException e) {
			//
			e.printStackTrace();
		} catch (AmazonClientException e) {
			//
			e.printStackTrace();
		} catch (Exception e) {
			//
			e.printStackTrace();
		}

	}

	public static String html2text(String html) {

		try {
			return Jsoup.parse(html).text();
		} catch (Exception e) {

		}

		return "";
	}

	public static synchronized void updateMemcacheStatsInEnd() {

		// Update per second email count
		try {

			Long TimeDifference = System.currentTimeMillis() - MainWhiteLabelClass.processStartTime;

			String TimeDifferenceEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / (TimeDifference / 1000));

			System.out.println(" \n Per second emails..." + TimeDifferenceEmailCount);

			SettingsClass.memcacheObj.set(SettingsClass.perSecondsMails, 0, TimeDifferenceEmailCount);

			SettingsClass.memcacheObj.set(SettingsClass.numberofNewJobMailsKey, 0, String.valueOf(SettingsClass.numberOfNewJobMails.get()));

		} catch (Exception e) {
			//
			e.printStackTrace();
		}

		// write in memcache -- cloud search jobs % in single mail
		try {

			float cloudSearchJobPerc = SettingsClass.cloudSearchJobPerc / SettingsClass.newTotalEmailCount.get();

			System.out.println(" \nCloud search jobs % in single mail..." + cloudSearchJobPerc);

			SettingsClass.memcacheObj.set(SettingsClass.mailCloudJobsPer, 0, String.valueOf(cloudSearchJobPerc));

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// write in memcache -- cloud search total fully processed users %

		float usersProcssedCS = ((float) SettingsClass.user_CloudSearch_ProcessingCount.get() / (float) SettingsClass.userProcessingCount.get()) * 100f;
		try {

			System.out.println(" \nCloud search totally fully processed users %..." + usersProcssedCS);
			SettingsClass.memcacheObj.set(SettingsClass.cloudSearchUsersPer, 0, String.valueOf(usersProcssedCS));
		} catch (Exception e1) {
			//
			e1.printStackTrace();
		}

		// total users processed in persecond

		Long TimeDifference = System.currentTimeMillis() - MainWhiteLabelClass.processStartTime;
		// write in memcache -- processed users %
		float usersProcssed = SettingsClass.userProcessingCount.get() / (TimeDifference / 1000);
		try {
			System.out.println(" \ntotal users processed in persecond..." + usersProcssed);
			SettingsClass.memcacheObj.set(SettingsClass.perSecondUsers, 0, String.valueOf(usersProcssed));
		} catch (Exception e1) {
			//
			e1.printStackTrace();
		}

		try {

			SettingsClass.memcacheObj.set(SettingsClass.numberofNewJobMailsKey, 0, String.valueOf(SettingsClass.numberOfNewJobMails));
			SettingsClass.memcacheObj.set(SettingsClass.average_percentage_NewJobsMailsKey, 0,
					String.valueOf(SettingsClass.decFormat.format((double) ((double) SettingsClass.numberOfNewJobMails.get() / (double) SettingsClass.totalEmailCount.get()) * 100)));
		} catch (Exception e) {

		}

		try {
			SettingsClass.memcacheObj.set(SettingsClass.jobsCountInMails, 0, Utility.jobsCountInMail_toString());
		} catch (Exception e1) {
			//
			e1.printStackTrace();
		}
		// write jobProvidersJobCount to memcache

		try {
			SettingsClass.memcacheObj.set(SettingsClass.JobProvidersjobsCountKey, 0, Utility.providersjobsCount_toString());
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		try {

			SettingsClass.memcacheObj.set(SettingsClass.groupTemplatesCountKey, 0, Utility.groupTemplateIds_toString());
		} catch (Exception e1) {

			e1.printStackTrace();
		}

		try {
			SettingsClass.memcacheObj.set(SettingsClass.new_jobProviderCountDomainWiseKey, 0, Utility.convert_JobDomainwiseProviderCountStats_ToString());
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		try {

			SettingsClass.memcacheObj.set(SettingsClass.new_DomainCaterotyWiseGroupTemplateStatedMemcacaheKey, 0, Utility.convert_DomainCategoryGroupWiseStatsFromMap_ToString());

		} catch (Exception e1) {

			e1.printStackTrace();
		}

		updateFilesData();

	}

	public static synchronized void updateFilesData() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		FileWriter fileWriterObj = null;
		File log_folder = null;
		File log_dashboard_folder = null;
		File log_process_stats_folder = null;
		// File log_groupIds_stats_folder = null;
		File log_jobsProvidersJobCount_stats_folder = null;
		File log_groupAndTemplateCount_stats_folder = null;
		File userNotProcessedStats_folder = null;
		File log_domainCategoryWisegroupAndTemplateCount_stats_folder = null;
		File new_log_jobsProvidersJobCount_stats_folder = null, child_log_dashboard_folder = null;

		try {

			DecimalFormat decFormat = new DecimalFormat("###.##");
			try {
				// main daily date folder..
				log_folder = new File(SettingsClass.filePath + SettingsClass.dateFormat.format(Calendar.getInstance().getTime()));

				if (!log_folder.exists()) {
					log_folder.mkdir();
				}

				try {
					log_dashboard_folder = new File(SettingsClass.filePath + "dashboard/");
				} catch (Exception e) {
					//
					e.printStackTrace();
				}

				if (!log_dashboard_folder.exists()) {
					log_dashboard_folder.mkdir();
				}

				try {
					log_process_stats_folder = new File(SettingsClass.filePath + "Jobs_count/");
				} catch (Exception e) {
					//
					e.printStackTrace();
				}

				if (!log_process_stats_folder.exists()) {
					log_process_stats_folder.mkdir();
				}

				try {
					log_jobsProvidersJobCount_stats_folder = new File(SettingsClass.filePath + "jobProvidersJobCount/");// /var/nfs-93/redirect/mis_logs/jobProvidersJobCount/
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (!log_jobsProvidersJobCount_stats_folder.exists()) {
					log_jobsProvidersJobCount_stats_folder.mkdir();
				}

				try {
					log_groupAndTemplateCount_stats_folder = new File(SettingsClass.filePath + "java_subject_line_stats/");// /var/nfs-93/redirect/mis_logs/jobProvidersJobCount/
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (!log_groupAndTemplateCount_stats_folder.exists()) {
					log_groupAndTemplateCount_stats_folder.mkdir();
				}

				if (!log_jobsProvidersJobCount_stats_folder.exists()) {
					log_jobsProvidersJobCount_stats_folder.mkdir();
				}

				try {
					log_domainCategoryWisegroupAndTemplateCount_stats_folder = new File(SettingsClass.filePath + "new_java_subject_line_stats/");// /var/nfs-93/redirect/mis_logs/jobProvidersJobCount/
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (!log_domainCategoryWisegroupAndTemplateCount_stats_folder.exists()) {
					log_domainCategoryWisegroupAndTemplateCount_stats_folder.mkdir();
				}

				try {
					new_log_jobsProvidersJobCount_stats_folder = new File(SettingsClass.filePath + "new_jobProvidersJobCount/");
					// /var/nfs-93/redirect/mis_logs/jobProvidersJobCount/
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (!new_log_jobsProvidersJobCount_stats_folder.exists()) {
					new_log_jobsProvidersJobCount_stats_folder.mkdir();
				}

				try {
					child_log_dashboard_folder = new File(SettingsClass.filePath + "combine_process_dashboard/");
				} catch (Exception e) {

					e.printStackTrace();
				}
				if (!child_log_dashboard_folder.exists()) {
					child_log_dashboard_folder.mkdir();
				}

				try {
					userNotProcessedStats_folder = new File(SettingsClass.filePath + "usersNotProcessedStats/");
				} catch (Exception e) {

					e.printStackTrace();
				}
				if (!userNotProcessedStats_folder.exists()) {
					userNotProcessedStats_folder.mkdir();
				}

			} catch (Exception e1) {
				//
				e1.printStackTrace();
			}

			// apiRresponseTimeFileWriterObj.write("APINAME|AVERAGE RESPONSE
			// TIME|PROCESSED USERS|CURRENT DATE TIME\n");
			// apiRresponseTimeFileWriterObj.close();

			// ===========================================api response
			// stats=============================

			try {

				fileWriterObj = new FileWriter(log_folder + File.separator + SettingsClass.dateFormat.format(new Date()) + "_" + SettingsClass.queuName + ".txt", false);
				String writeString = "";
				try {
					writeString += "CloudSearchQuery|" + decFormat.format((SettingsClass.apiResponseTimeHashmap.get("cloud search") / SettingsClass.apiUserProcessingCountHashmap.get("cloud search")))
							+ "|" + SettingsClass.apiUserProcessingCountHashmap.get("cloud search") + "|" + df.format(Calendar.getInstance().getTime()) + "|" + SettingsClass.dashboradFileName + "\n";
				} catch (Exception e) {
					e.printStackTrace();
				}
				for (String feed : MainWhiteLabelClass.mReadFeedOrderModel.apiStatusHashmap.keySet()) {
					Double respnseTime = SettingsClass.apiResponseTimeHashmap.get(feed);
					Integer usersProcessed = SettingsClass.apiUserProcessingCountHashmap.get(feed);
					double response = 0;
					try {
						if (respnseTime != null && usersProcessed != null) {
							response = respnseTime / usersProcessed;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					writeString += feed + "|" + decFormat.format(response) + "|" + usersProcessed + "|" + df.format(Calendar.getInstance().getTime()) + "|" + SettingsClass.dashboradFileName + "\n";
				}
				fileWriterObj.write(writeString);

			} catch (Exception e) {
				//
				e.printStackTrace();
			}

			try {
				fileWriterObj.close();
			} catch (IOException e) {
				//
				e.printStackTrace();
			}

			fileWriterObj = null;
			// =================== Live stats update
			// ====================================================================================

			double cloudSearchJobPerc = 0.0;
			double usersProcssedCS = 0.0;
			double perSecondUsersProcssed = 0.0;
			double TimeDifference = System.currentTimeMillis() - MainWhiteLabelClass.processStartTime;
			double timeInSeconds = (TimeDifference / 1000);
			String perSecondPercentageEmailCount = "";
			String averageMailsWithNewJob = "";
			try {
				fileWriterObj = new FileWriter(log_dashboard_folder + File.separator + SettingsClass.queuName + "_live_stats.txt");
				cloudSearchJobPerc = SettingsClass.cloudSearchJobPerc / SettingsClass.newTotalEmailCount.get();
				usersProcssedCS = ((float) SettingsClass.user_CloudSearch_ProcessingCount.get() / (float) SettingsClass.userProcessingCount.get()) * 100f;
				perSecondUsersProcssed = SettingsClass.newuserProcessingCount.get() / timeInSeconds;
				perSecondPercentageEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / timeInSeconds);
				averageMailsWithNewJob = String.valueOf(SettingsClass.decFormat.format(((float) SettingsClass.numberOfNewJobMails.get() / (float) SettingsClass.totalEmailCount.get()) * 100f));
			} catch (Exception e) {
				System.out.println("File wrting problem 1");
				e.printStackTrace();

			}

			try {
				String writeString = df1.format(new Date()) + "|" + SettingsClass.totalUsersCount + "|" + SettingsClass.userProcessingCount.get() + "|" + decFormat.format(perSecondUsersProcssed)
						+ "|" + SettingsClass.totalEmailCount + "|" + perSecondPercentageEmailCount + "|" + MainWhiteLabelClass.startTimeForDashBoard + "|" + decFormat.format(usersProcssedCS) + "|"
						+ decFormat.format(cloudSearchJobPerc) + "|" + SettingsClass.dashboradFileName + "|" + SettingsClass.processesIsRunning + "|" + SettingsClass.numberOfNewJobMails + "|"
						+ averageMailsWithNewJob + "|" + SettingsClass.currentTimeForDashBoard + "|" + SettingsClass.duplicateUser.get() + "|" + SettingsClass.duplicateUserFromElasticCache.get();
				fileWriterObj.write(writeString);
			} catch (Exception e) {
				System.out.println("File wrting problem 2");
				e.printStackTrace();
			} finally {
				fileWriterObj.close();
			}

			// ================================================================================================================

			// ==========================================1to 40 jobs count in
			// processes=============================================================
			try {
				fileWriterObj = new FileWriter(log_process_stats_folder + File.separator + SettingsClass.queuName + "_process_stats.txt");
			} catch (Exception e) {

			}

			// Today DateTime|1to 40 job count|Process name

			try {
				String writeString = df1.format(new Date()) + "|" + Utility.jobsCountInMail_toString() + "|" + SettingsClass.dashboradFileName + "\n";

				fileWriterObj.write(writeString);

				// System.out.println("writing done..........");

			} catch (Exception e) {
				//
				e.printStackTrace();
			}
			try {
				fileWriterObj.close();
			} catch (IOException e) {
				//
				e.printStackTrace();
			}

			// ==========================================group Ids count in
			// processes=============================================================

			// Today DateTime|subject lines count|Process name
			// try {
			// String writeString = "";
			// fileWriterObj = new
			// FileWriter(log_groupAndTemplateCount_stats_folder +
			// File.separator + SettingsClass.queuName +
			// "_groupAndTemplateCount.json");
			//
			// writeString = Utility.templateGsonResult() + "\n";
			//
			// fileWriterObj.write(writeString);
			//
			// }
			// catch (Exception e) {
			//
			// }

			try {
				fileWriterObj.close();
			} catch (IOException e) {
				//
				e.printStackTrace();
			}

			// ////////////////=====================================================================================

			// Count in file
			// =============================================================
			try {
				fileWriterObj = new FileWriter(log_jobsProvidersJobCount_stats_folder + File.separator + SettingsClass.dashboradFileName + "_providersJobsCount.json");
				// writing job provider json in the file
				String writeString = Utility.jobProviderJobCountFinalJsonResult() + "\n";
				fileWriterObj.write(writeString);

			} catch (Exception e) {

			}

			try {
				fileWriterObj.close();
			} catch (Exception e) {

				e.printStackTrace();
			}

			// =============================================================

			// Today DateTime|providers job count|Process name

			try {

				// System.out.println("writing done..........");

			} catch (Exception e) {

				e.printStackTrace();
			}
			try {
				fileWriterObj.close();
			} catch (Exception e) {

				e.printStackTrace();
			}

			// child process
			synchronized (SettingsClass.child_Process_stats_hashMap) {

				for (int i = 0; i < MainWhiteLabelClass.childDomainMemCacheArrayList.size(); i++) {

					String childDomainName = MainWhiteLabelClass.childDomainMemCacheArrayList.get(i);

					try {
						fileWriterObj = new FileWriter(child_log_dashboard_folder + File.separator + childDomainName + "_live_stats.txt");

					} catch (Exception e) {

					}

					int totalUsersCount = 0;
					try {
						totalUsersCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.totalUserKey);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
					}
					int userProcessingCount = 0;
					try {
						userProcessingCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.userProcessed);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
					}
					int totalEmailCount = 0;
					try {
						totalEmailCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.emailSend);
					} catch (Exception e1) {
						// TODO Auto-generated catch block

					}
					int elasticDuplicateCount = 0;
					try {
						elasticDuplicateCount = SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.elasticDuplicateMemcacheKey);
					} catch (Exception e1) {

					}
					try {
						String writeString = df1.format(new Date()) + "|" + totalUsersCount + "|" + userProcessingCount + "|" + 0 + "|" + totalEmailCount + "|" + 0 + "|"
								+ MainWhiteLabelClass.startTimeForDashBoard + "|" + 0 + "|" + 0 + "|" + childDomainName + "|" + SettingsClass.processesIsRunning + "|" + 0 + "|" + 0 + "|"
								+ SettingsClass.currentTimeForDashBoard + "|" + SettingsClass.duplicateUser.get() + "|" + SettingsClass.duplicateUserFromElasticCache.get() + "|"
								+ SettingsClass.dashboradFileName + "|" + SettingsClass.whitelabel_processname + "|" + elasticDuplicateCount;

						fileWriterObj.write(writeString);
						fileWriterObj.close();

						// System.out.println("writing done..........");

					} catch (Exception e) {

						e.printStackTrace();
					}

					// new code to write the
					// stat==================================//==================
					String fileName = SettingsClass.child_subject_From_File_Names.get(MainWhiteLabelClass.childDomainMemCacheArrayList.get(i));

					String resultData = templateGsonResult_New_Method(fileName, MainWhiteLabelClass.childDomainMemCacheArrayList.get(i));
					System.out.println("file name= for subject lines list" + fileName);

					try {
						fileWriterObj = new FileWriter(log_groupAndTemplateCount_stats_folder + File.separator + SettingsClass.queuName + "_" + fileName + ".json");

						fileWriterObj.write(resultData);
						fileWriterObj.close();
					} catch (Exception e) {

					}
				}

			}

			// =================================group and template count
			try {

				try {
					fileWriterObj = new FileWriter(log_domainCategoryWisegroupAndTemplateCount_stats_folder + File.separator + SettingsClass.queuName + "_groupAndTemplateCount.json");
					fileWriterObj.write(convert_DomainWiseSubjectLineStats_toJson());
				} catch (Exception e) {

				} finally {
					fileWriterObj.close();
				}

			} catch (Exception e) {

				e.printStackTrace();
			}

			try {
				fileWriterObj = new FileWriter(new_log_jobsProvidersJobCount_stats_folder + File.separator + SettingsClass.queuName + "_providersJobsCount.json");
				String writeString = Utility.convert_jobproviderHashMapStats_toJason() + "\n";
				fileWriterObj.write(writeString);
			} catch (Exception e) {

			} finally {
				fileWriterObj.close();
			}

			// =============================================================
			try {
				fileWriterObj = new FileWriter(userNotProcessedStats_folder + "/" + SettingsClass.queuName + SettingsClass.dateFormat.format(new Date()) + ".txt");
				fileWriterObj.write(userNotProcessedString());
			} catch (Exception e) {

			} finally {
				fileWriterObj.close();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static String templateGsonResult() {

		String gson = "";

		try {

			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			ArrayList<FinalGroupObject> finalGroupList = new ArrayList<FinalGroupObject>();

			for (int group_id = 0; group_id < SettingsClass.groupObjectList.size(); group_id++) {

				FinalGroupObject finalGroupObject = new FinalGroupObject();

				int groupCount = 0;

				for (int temp_id = 0; temp_id < SettingsClass.templateObjectList.size(); temp_id++) {

					String key = SettingsClass.groupObjectList.get(group_id).getGroupId() + "_" + SettingsClass.templateObjectList.get(temp_id).getTemplateId();

					Integer tempValueObject = SettingsClass.templateCountHashMap.get(key);

					if (tempValueObject == null) {
						tempValueObject = 0;
					}

					TemplateObject template = new TemplateObject();
					template.setTemplateId(SettingsClass.templateObjectList.get(temp_id).getTemplateId());
					template.setTemplateCount(tempValueObject);

					finalGroupObject.template.add(template);

					groupCount = groupCount + tempValueObject;

				}

				finalGroupObject.setGroup_id(SettingsClass.groupObjectList.get(group_id).getGroupId());
				finalGroupObject.setFrom_name(SettingsClass.groupObjectList.get(group_id).getFromName());
				finalGroupObject.setSubject(SettingsClass.groupObjectList.get(group_id).getSubject());
				finalGroupObject.setGroup_count(groupCount);
				finalGroupObject.setProcess_name(SettingsClass.dashboradFileName);
				finalGroupObject.setDate(df1.format(new Date()));

				finalGroupList.add(finalGroupObject);

			}

			gson = new Gson().toJson(finalGroupList);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return gson;

	}

	public static String jobProviderJobCountFinalJsonResult() {

		String gson = "";

		try {

			JobProviderJobCountData JobProviderJobCountObj = new JobProviderJobCountData();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			JobProviderJobCountObj.setDate(df1.format(new Date()));

			JobProviderJobCountObj.setProcessName(SettingsClass.dashboradFileName);

			JobProviderJobCountObj.setSources(MainWhiteLabelClass.jobProvidersJobCountMap);

			gson = new Gson().toJson(JobProviderJobCountObj);

			// Gson gson = new Gson();
			// JsonElement json = gson.toJsonTree(fromNameObjectLIst );
			// JsonObject jo = new JsonObject();
			// jo.add("Foo", json);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return gson;

	}

	// =========Send Message==================
	public static void sendTextSms(String data) {
		try {
			String phoneNumber = "8860547656,8699864636,9530667082,9855115882,9988736500,9464533885";
			data = URLEncoder.encode(data, "UTF-8");
			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";
			url = url + phoneNumber;
			String text = "&text=" + data;
			url = url + text;
			url = url + "&priority=ndnd&stype=normal";
			System.out.println("Url=" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// // =======Parsing==============================
	// public static String decodeString(String string) {
	// String decodedString = "";
	// try {
	// decodedString = URLDecoder.decode((html2text(string.replace("$",
	// "").replace("!", ""))), "UTF-8");
	//
	// } catch (Exception e) {
	// try {
	// decodedString = html2text(string.replace("$", "").replace("!", ""));
	// } catch (Exception e1) {
	// decodedString = string.replace("$", "").replace("!", "");
	// }
	// }
	// return decodedString;
	//
	// }
	//
	// public static boolean checkValidJob(Jobs job_data) {
	//
	// boolean isValidJob = false;
	//
	// if (job_data.getTitle() != null
	// && !job_data.getTitle().equalsIgnoreCase("null")
	// && !job_data.getTitle().equalsIgnoreCase("")
	// && job_data.getEmployer() != null
	// && !job_data.getEmployer().equalsIgnoreCase("null")
	// && !job_data.getEmployer().equalsIgnoreCase("")
	// && ((job_data.getCity() != null &&
	// !job_data.getCity().equalsIgnoreCase("null") &&
	// !job_data.getCity().equalsIgnoreCase("")) || (job_data.getState() != null
	// && !job_data.getState().equalsIgnoreCase("null") &&
	// !job_data.getState().equalsIgnoreCase("")))
	// && !MainWhiteLabelClass.companyBlockedString.toLowerCase().contains("'" +
	// job_data.getEmployer().toLowerCase() + "'")) {
	// isValidJob = true;
	// }
	//
	// return isValidJob;
	// }
	//
	// public static ArrayList<Jobs> CareerBlissApiParsing(String result, String
	// exactOrSimple) {
	//
	// // L.d("Parsing J2CApiParsing");
	// ArrayList<Jobs> careerList = new ArrayList<Jobs>();
	// JSONObject jObj = null;
	//
	// try {
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	//
	// if (jObj != null) {
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	//
	// JSONObject jSONObject = jobs_array.getJSONObject(temp);
	//
	// try {
	// job_data.setEmployer(decodeString(jSONObject.getString("company")));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jSONObject.getString("url"));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("CareerBlissApi");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.CareerBlissApi);
	//
	// job_data.setDisplaysource("CareerBlissApi.");
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// String cpc = jSONObject.getString("cpc");
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// if (splited.length > 1 && cpc != null && !cpc.equalsIgnoreCase("") &&
	// !cpc.equalsIgnoreCase("null")) {
	//
	// job_data.setSourcename("CareerBlissApi_" + cpc);
	//
	// if (cpc.equalsIgnoreCase("low")) {
	// job_data.setGross_cpc(splited[2]);
	// job_data.setCpc(splited[3]);
	// } else if (cpc.equalsIgnoreCase("med")) {
	// job_data.setGross_cpc(splited[4]);
	// job_data.setCpc(splited[5]);
	// } else if (cpc.equalsIgnoreCase("high")) {
	// job_data.setGross_cpc(splited[6]);
	// job_data.setCpc(splited[7]);
	// } else if (cpc.equalsIgnoreCase("lower")) {
	// job_data.setGross_cpc(splited[8]);
	// job_data.setCpc(splited[9]);
	// } else if (cpc.equalsIgnoreCase("lowest")) {
	// job_data.setGross_cpc(splited[10]);
	// job_data.setCpc(splited[11]);
	// } else {
	// job_data.setSourcename("CareerBlissApi");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// }
	// } else {
	// job_data.setGross_cpc("0");
	// job_data.setCpc("0");
	// }
	//
	// } catch (Exception e) {
	// }
	// try {
	// job_data.setTitle(decodeString(jSONObject.getString("title")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// String cityArray;
	// try {
	// cityArray = jSONObject.getString("location");
	// try {
	// job_data.setCity(cityArray.split(",")[0].replace("$", "").replace("!",
	// "").trim());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(cityArray.split(",")[1].replace("$", "").replace("!",
	// "").trim());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jSONObject.getString("date"))); //
	// change
	// // the
	// // format
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// careerList.add(job_data);
	// }
	//
	// }
	// }
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // System.out.println("\n J2CList api response : " + J2CList.size());
	//
	// return careerList;
	// }
	//
	// public static ArrayList<Jobs> getXmlDatafromResult(String xmlResult,
	// String exactOrSimple) {
	// // L.d("Parsing JUJU");
	//
	// // here we get the xml data from result...and then we will parse ..it..
	//
	// // Jobs _job=new Jobs();
	// ArrayList<Jobs> JuJuList = new ArrayList<Jobs>();
	//
	// try {
	//
	// DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	// DocumentBuilder builder = factory.newDocumentBuilder();
	// InputSource is = new InputSource(new StringReader(xmlResult));
	//
	// Document doc = builder.parse(is);
	//
	// doc.getDocumentElement().normalize();
	//
	// NodeList nList = doc.getElementsByTagName("item");
	//
	// for (int temp = 0; temp < nList.getLength(); temp++) {
	//
	// Node nNode = nList.item(temp);
	//
	// Jobs job_data = new Jobs();
	// if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	//
	// Element eElement = (Element) nNode;
	//
	// job_data.setCity(eElement.getElementsByTagName("city").item(0).getTextContent().replace("$",
	// "").replace("!", ""));
	// job_data.setEmployer(decodeString(eElement.getElementsByTagName("company").item(0).getTextContent()));
	//
	// job_data.setJoburl(eElement.getElementsByTagName("link").item(0).getTextContent());
	// job_data.setSourcename("Juju API");
	//
	// job_data.setDisplaysource("Juju.");
	//
	// if (exactOrSimple.equals(SettingsClass.EXACT)) {
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.JuJuExactApiName);
	// } else {
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.JuJuApiName);
	//
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// job_data.setTitle(decodeString(eElement.getElementsByTagName("title").item(0).getTextContent()));
	// job_data.setState(eElement.getElementsByTagName("state").item(0).getTextContent().replace("$",
	// "").replace("!", ""));
	// try {
	// job_data.setPostingdate(formatDate(eElement.getElementsByTagName("postdate").item(0).getTextContent()));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	//
	// if (checkValidJob(job_data)) {
	// JuJuList.add(job_data);
	// }
	// }
	//
	// }
	//
	// return JuJuList;
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// return JuJuList;
	//
	// }
	//
	// // new spi start
	// public static ArrayList<Jobs> zipRecuriterNTLSecParsing(String result,
	// String exactOrSimple) {
	//
	// ArrayList<Jobs> zipRecuriterNTLList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	// jObj = new JSONObject(result);
	//
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	//
	// JSONObject jsonObj = jobs_array.getJSONObject(temp);
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jsonObj.getString("city").replace("$", "").replace("!",
	// ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jsonObj.getString("url")); // check
	// // making
	// // job url (from 'jk' field)
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jsonObj.getJSONObject("hiring_company").getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setSourcename("Zip Recruiter NTL Sec API");
	//
	// // job_data.setSourcename("tieronejobs_2_site");
	//
	// if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest) {
	// job_data.setDisplaysource("Zip Recruiter NTL Sec API");
	// } else {
	// // job_data.setDisplaysource("Zip Recruiter.");
	// job_data.setDisplaysource("ZR.");
	//
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.zipRecuriterNTLSecApiName);
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setState(jsonObj.getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jsonObj.getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(jsonObj.getString("posted_time"));
	// } catch (JSONException e) {
	// job_data.setPostingdate("");
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	//
	// if (checkValidJob(job_data)) {
	//
	// zipRecuriterNTLList.add(job_data);
	//
	// }
	//
	// }
	//
	// return zipRecuriterNTLList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return zipRecuriterNTLList;
	// }
	//
	// public static ArrayList<Jobs> zipRecuriterNTLThirdParsing(String result,
	// String exactOrSimple) {
	//
	// ArrayList<Jobs> zipRecuriterNTLList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	// jObj = new JSONObject(result);
	//
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	//
	// JSONObject jsonObj = jobs_array.getJSONObject(temp);
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jsonObj.getString("city").replace("$", "").replace("!",
	// ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jsonObj.getString("url")); // check
	// // making
	// // job url (from 'jk' field)
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jsonObj.getJSONObject("hiring_company").getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setSourcename("Zip Recruiter NTL Third API");
	//
	// // job_data.setSourcename("tieronejobs_3_site");
	//
	// if (SettingsClass.runOrignalTest || SettingsClass.isThisTestRun) {
	// job_data.setDisplaysource("Zip Recruiter NTL Third API");
	// } else {
	// // job_data.setDisplaysource("Zip Recruiter.");
	// job_data.setDisplaysource("ZR.");
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.zipRecuriterNTLThirdApiName);
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setState(jsonObj.getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jsonObj.getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(jsonObj.getString("posted_time"));
	// } catch (JSONException e) {
	// job_data.setPostingdate("");
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	//
	// if (checkValidJob(job_data)) {
	//
	// zipRecuriterNTLList.add(job_data);
	//
	// }
	//
	// }
	//
	// return zipRecuriterNTLList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return zipRecuriterNTLList;
	// }
	//
	// public static ArrayList<Jobs> zipALlApiParsing(String result, String
	// exactOrSimple) {
	//
	// ArrayList<Jobs> zipAllApiList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	//
	// jObj = new JSONObject(result);
	//
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	//
	// JSONObject jsonObj = jobs_array.getJSONObject(temp);
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jsonObj.getString("city").replace("$", "").replace("!",
	// ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jsonObj.getString("url")); // check
	// // making
	// // job url (from 'jk' field)
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jsonObj.getJSONObject("hiring_company").getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setSourcename("ZIPALLAPI");
	// // job_data.setSourcename("tieronejobs_4_site Alerts");
	// if (SettingsClass.runOrignalTest || SettingsClass.isThisTestRun) {
	// job_data.setDisplaysource("ZIPALLAPI");
	// } else {
	// job_data.setDisplaysource("ZR.");
	// }
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.zipAllAPi);
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// // cpc_tier: "L" Low = 9 cents
	// // cpc_tier: "M" Med = 16 cents
	// // cpc_tier: "H" High = 30 cents
	// String cpc_tier = jsonObj.getString("cpc_tier");
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// if (splited.length > 1 && cpc_tier != null &&
	// !cpc_tier.equalsIgnoreCase("") && !cpc_tier.equalsIgnoreCase("null")) {
	//
	// job_data.setSourcename("ZIPALLAPI_" + cpc_tier);
	//
	// if (cpc_tier.equalsIgnoreCase("L")) {
	// job_data.setGross_cpc(splited[2]);
	// job_data.setCpc(splited[3]);
	// } else if (cpc_tier.equalsIgnoreCase("M")) {
	// job_data.setGross_cpc(splited[4]);
	// job_data.setCpc(splited[5]);
	// } else if (cpc_tier.equalsIgnoreCase("H")) {
	// job_data.setGross_cpc(splited[6]);
	// job_data.setCpc(splited[7]);
	// } else {
	// job_data.setSourcename("ZIPALLAPI");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// }
	// } else {
	// job_data.setGross_cpc("0");
	// job_data.setCpc("0");
	// }
	//
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	//
	// try {
	// job_data.setState(jsonObj.getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jsonObj.getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(jsonObj.getString("posted_time"));
	// } catch (JSONException e) {
	// job_data.setPostingdate("");
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	//
	// if (checkValidJob(job_data)) {
	//
	// zipAllApiList.add(job_data);
	// }
	//
	// }
	//
	// return zipAllApiList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return zipAllApiList;
	// }
	//
	// public static ArrayList<Jobs> J2CBooleanApiParsing(String result, String
	// exactOrSimple) {
	//
	// // L.d("Parsing J2CApiParsing");
	// ArrayList<Jobs> J2CList = new ArrayList<Jobs>();
	// JSONObject jObj = null;
	//
	// try {
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// /*
	// * try { String city =
	// * jobs_array.getJSONObject(temp).getString("city");
	// * job_data.setCity(city.substring(2, city.indexOf(","))); }
	// * catch (JSONException e) { e.printStackTrace(); }
	// */
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("J2C Boolean");
	//
	// if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest) {
	// job_data.setDisplaysource("Jobs2Careers Boolean API");
	// } else {
	// job_data.setDisplaysource("Jobs2Careers.");
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.J2CBoleanApiName);
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// JSONArray cityArray;
	//
	// try {
	// cityArray = jobs_array.getJSONObject(temp).getJSONArray("city");
	// try {
	// job_data.setCity(cityArray.getString(0).split(",")[0].replace("$",
	// "").replace("!", "").trim());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(cityArray.getString(0).split(",")[1].replace("$",
	// "").replace("!", "").trim());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // change
	// // the
	// // format
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	// /*
	// * try { job_data.setZipcode(location); } catch (Exception e) {
	// * e.printStackTrace(); }
	// */
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	//
	// J2CList.add(job_data);
	// }
	//
	// }
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// return J2CList;
	// }
	//
	// // new api end
	//
	// public static ArrayList<Jobs> J2CApiParsing(String result, String
	// exactOrSimple) {
	// ArrayList<Jobs> J2CList = new ArrayList<Jobs>();
	// JSONObject jObj = null;
	//
	// try {
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	// //
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	// //
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	//
	// } catch (JSONException e) {
	// //
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	// //
	// }
	// try {
	// job_data.setSourcename("Jobs2Careers API");
	//
	// job_data.setDisplaysource("Jobs2Careers.");
	// } catch (Exception e) {
	// //
	// }
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (JSONException e) {
	// //
	// }
	//
	// JSONArray cityArray;
	// try {
	// cityArray = jobs_array.getJSONObject(temp).getJSONArray("city");
	// try {
	// job_data.setCity(cityArray.getString(0).split(",")[0].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// }
	// try {
	// job_data.setState(cityArray.getString(0).split(",")[1].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// }
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // change
	// // the
	// // format
	// } catch (JSONException e) {
	// //
	// } catch (Exception e) {
	// //
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.J2CApiName);
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// J2CList.add(job_data);
	// }
	//
	// }
	//
	// } catch (Exception e) {
	//
	// }
	//
	// return J2CList;
	// }
	//
	// public static ArrayList<Jobs> J2CApiIDEParsing(String result, String
	// exactOrSimple) {
	//
	// ArrayList<Jobs> J2CList = new ArrayList<Jobs>();
	// JSONObject jObj = null;
	//
	// try {
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	//
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("J2C APIDE");
	//
	// if (SettingsClass.isThisTestRun) {
	// job_data.setDisplaysource("Jobs2Careers APIDE");
	// } else {
	// job_data.setDisplaysource("Jobs2Careers.");
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.J2CIDEApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// JSONArray cityArray;
	//
	// try {
	// cityArray = jobs_array.getJSONObject(temp).getJSONArray("city");
	// try {
	// job_data.setCity(cityArray.getString(0).split(",")[0].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(cityArray.getString(0).split(",")[1].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // change
	// // the
	// // format
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// J2CList.add(job_data);
	// }
	//
	// }
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // System.out.println("\n J2CList api response : " + J2CList.size());
	//
	// return J2CList;
	// }
	//
	// public static ArrayList<Jobs> jobApprovedApiParsing(String result, String
	// exactOrSimple) {
	// JSONObject jObj = null;
	// ArrayList<Jobs> jOBApprovedList = new ArrayList<Jobs>();
	// result = result.substring(result.indexOf("["));
	//
	// String new_result = result.substring(1, result.length() - 1);
	//
	// try {
	// jObj = new JSONObject(new_result);
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// JSONObject jObj_response = null;
	// try {
	// jObj_response = jObj.getJSONObject("response");
	// } catch (JSONException e2) {
	// //
	// e2.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = jObj_response.getJSONArray("results");
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setCity(jobs_array.getJSONObject(temp).getString("city").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(jobs_array.getJSONObject(temp).getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("JobApproved API");
	//
	// job_data.setDisplaysource("JobApproved.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.jobApprovedApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("jobtitle")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // parameter
	// // -
	// // date
	// // ,
	// // format
	// // in yy/mm/dddd
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// jOBApprovedList.add(job_data);
	// }
	//
	// }
	//
	// return jOBApprovedList;
	// }
	//
	// public static ArrayList<Jobs> myJobHelperApiParsing(String result, String
	// exactOrSimple) {
	// ArrayList<Jobs> myJobHelperList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// JSONObject queryResult = null;
	// try {
	// queryResult = jObj.getJSONObject("queryResult");
	// } catch (JSONException e2) {
	// //
	// e2.printStackTrace();
	// }
	// ;
	// JSONArray jobs_array = null;
	// try {
	//
	// jobs_array = queryResult.getJSONArray("jobs");
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setCity(jobs_array.getJSONObject(temp).getString("city").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(jobs_array.getJSONObject(temp).getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("My Job Helper API");
	//
	// job_data.setDisplaysource("My Job Helper.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.myJobHelperApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("postDate")));
	// // format
	// // date
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// /*
	// * try { job_data.setZipcode(location); } catch (Exception e) { //
	// * e.printStackTrace(); }
	// */
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// myJobHelperList.add(job_data);
	// }
	//
	// }
	//
	// return myJobHelperList;
	//
	// }
	//
	// public static ArrayList<Jobs> J2CTRUCKINGApiParsing(String result, String
	// exactOrSimple) {
	//
	// ArrayList<Jobs> J2CList = new ArrayList<Jobs>();
	// JSONObject jObj = null;
	//
	// try {
	//
	// try {
	// jObj = new JSONObject(result);
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	// //
	// e1.printStackTrace();
	// }
	//
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	//
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("J2C Trucking");
	//
	// if (SettingsClass.isThisTestRun) {
	// job_data.setDisplaysource("Jobs2Careers API");
	// } else {
	// job_data.setDisplaysource("Jobs2Careers.");
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.J2CTRUCKINGApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// JSONArray cityArray;
	//
	// try {
	// cityArray = jobs_array.getJSONObject(temp).getJSONArray("city");
	// try {
	// job_data.setCity(cityArray.getString(0).split(",")[0].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setState(cityArray.getString(0).split(",")[1].trim().replace("$",
	// "").replace("!", ""));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // change
	// // the
	// // format
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// J2CList.add(job_data);
	//
	// }
	//
	// }
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// return J2CList;
	// }
	//
	// public static ArrayList<Jobs> glassdoorJobParser(String result, String
	// exactOrSimple) {
	// ArrayList<Jobs> glassdoorList = new ArrayList<Jobs>();
	// try {
	// JSONObject jsonObject = new JSONObject(result);
	// JSONObject innerJson = jsonObject.getJSONObject("response");
	// JSONArray jsonArray = innerJson.getJSONArray("jobListings");
	// for (int i = 0; i < jsonArray.length(); i++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setTitle(decodeString(jsonArray.getJSONObject(i).getString("jobTitle")));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("Glassdoor API");
	//
	// job_data.setDisplaysource("Glassdoor.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.glassDoorApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setJoburl(jsonArray.getJSONObject(i).getString("jobViewUrl"));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// // spliting location into city and state
	// String location = jsonArray.getJSONObject(i).getString("location");
	// String[] stringArray = location.split(",");
	//
	// try {
	// job_data.setCity(stringArray[0].replace("$", "").replace("!", ""));
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setState(stringArray[1].replace("$", "").replace("!", ""));
	// } catch (Exception e) {
	//
	// }
	// } catch (Exception e) {
	//
	// }
	// try {
	// JSONObject employerObject = new
	// JSONObject(jsonArray.getJSONObject(i).getString("employer"));
	// job_data.setEmployer(decodeString(employerObject.getString("name")));
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	//
	// SimpleDateFormat srcformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// SimpleDateFormat destformat = new SimpleDateFormat("dd/MM/yyyy");
	//
	// Calendar c1 = Calendar.getInstance();
	// c1.setTime(srcformat.parse(jsonArray.getJSONObject(i).getString("date")));
	// String dest = destformat.format(c1.getTime());
	//
	// job_data.setPostingdate(dest);
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// // object.setPriority(priority);
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// glassdoorList.add(job_data);
	// }
	// }
	//
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// return glassdoorList;
	// }
	//
	// public static ArrayList<Jobs> SimplyHiredApiParsing(String result, String
	// keyword, String location, String exactOrSimple) {
	// ArrayList<Jobs> SimplyHiredList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// String new_result = result.substring(6, result.length() - 1);
	//
	// try {
	// jObj = new JSONObject(new_result);
	//
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("rs");
	// } catch (Exception e1) {
	// //
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jobs_array.getJSONObject(temp).getString("cty").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("cn")));
	//
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl("http://alumnirecruiter.com/index.php?jk=" +
	// jobs_array.getJSONObject(temp).getString("jk") + "&q=" + keyword + "&l="
	// + location); // check
	//
	// } catch (JSONException e) {
	// // isThisTestRun
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("SimplyHired API");
	//
	// // job_data.setSourcename("SH Site");
	//
	// job_data.setDisplaysource("SimplyHired.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.SimplyHiredApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setState(jobs_array.getJSONObject(temp).getString("st").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("jt")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("dp")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	// /*
	// * try { job_data.setZipcode(location); } catch (Exception
	// * e) { // e.printStackTrace(); }
	// */
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// SimplyHiredList.add(job_data);
	// }
	//
	// }
	//
	// return SimplyHiredList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return SimplyHiredList;
	// }
	//
	// public static ArrayList<Jobs> zipRecuriterParsing(String result, String
	// exactOrSimple) {
	// ArrayList<Jobs> zipRecuriterList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	// jObj = new JSONObject(result);
	//
	// } catch (JSONException e1) {
	// //
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jobs_array.getJSONObject(temp).getString("city").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("org_name")));
	//
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url")); //
	// check
	// // making
	// // job url (from 'jk' field)
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("Zip Recruiter API");
	//
	// // job_data.setDisplaysource("Zip Recruiter.");
	// job_data.setDisplaysource("ZR.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.zipRecuriterApiName);
	//
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setState(jobs_array.getJSONObject(temp).getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("title")));
	// } catch (JSONException e) {
	// //
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(jobs_array.getJSONObject(temp).getString("date"));
	// } catch (JSONException e) {
	// job_data.setPostingdate("");
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// zipRecuriterList.add(job_data);
	// }
	//
	// }
	//
	// return zipRecuriterList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return zipRecuriterList;
	// }
	//
	// public static ArrayList<Jobs> zipRecuriterNTLParsing(String result,
	// String exactOrSimple) {
	//
	// ArrayList<Jobs> zipRecuriterNTLList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	//
	// try {
	// jObj = new JSONObject(result);
	//
	// } catch (JSONException e1) {
	//
	// e1.printStackTrace();
	// }
	// JSONArray jobs_array = null;
	//
	// try {
	// jobs_array = jObj.getJSONArray("jobs");
	// } catch (Exception e1) {
	//
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (jobs_array != null && jobs_array.length() > 0) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	//
	// JSONObject jsonObj = jobs_array.getJSONObject(temp);
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(jsonObj.getString("city").replace("$", "").replace("!",
	// ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jsonObj.getString("url")); // check
	// // making
	// // job url (from 'jk' field)
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setEmployer(decodeString(jsonObj.getJSONObject("hiring_company").getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setSourcename("Zip Recruiter NTL API");
	//
	// // job_data.setSourcename("tieronejobs_site");
	//
	// if (SettingsClass.isThisTestRun) {
	// job_data.setDisplaysource("Zip Recruiter NTL API");
	// } else {
	// // job_data.setDisplaysource("Zip Recruiter.");
	//
	// job_data.setDisplaysource("ZR.");
	// }
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.zipRecuriterNTLApiName);
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setState(jsonObj.getString("state").replace("$",
	// "").replace("!", ""));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jsonObj.getString("name")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(jsonObj.getString("posted_time"));
	// } catch (JSONException e) {
	// job_data.setPostingdate("");
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// // job_data.setPriority(priority);
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	//
	// if (checkValidJob(job_data)) {
	//
	// zipRecuriterNTLList.add(job_data);
	// }
	//
	// }
	//
	// return zipRecuriterNTLList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return zipRecuriterNTLList;
	// }
	//
	// public static ArrayList<Jobs> Lead5Media_Api_Parsing(String result,
	// String exactOrSimple) {
	//
	// // L.d("Parsing J2CApiParsing");
	// ArrayList<Jobs> Lead5MediaArrayList = new ArrayList<Jobs>();
	//
	// try {
	// JSONArray mainArray = new JSONArray(result);
	//
	// JSONObject responeObject = null;
	// try {
	// JSONObject tempJsonObjct = mainArray.getJSONObject(0);
	//
	// responeObject = tempJsonObjct.getJSONObject("response");
	// } catch (Exception e2) {
	//
	// e2.printStackTrace();
	// }
	//
	// JSONArray jobs_array = null;
	// try {
	// jobs_array = responeObject.getJSONArray("results");
	// } catch (Exception e1) {
	//
	// e1.printStackTrace();
	// }
	// if (jobs_array != null) {
	// for (int temp = 0; temp < jobs_array.length(); temp++) {
	// Jobs job_data = new Jobs();
	// try {
	// job_data.setEmployer(decodeString(jobs_array.getJSONObject(temp).getString("company")));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// // job_data.setId(user_id);
	// try {
	// job_data.setJoburl(jobs_array.getJSONObject(temp).getString("url"));
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setSourcename("Lead 5 media Api");
	//
	// job_data.setDisplaysource("Lead 5 Media.");
	//
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.lead5media);
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	//
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	//
	// //
	// job_data.setGross_cpc(jobs_array.getJSONObject(temp).getString("cpc").trim());
	// // String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// // float sc1 = Float.valueOf(splited[12]);
	// // float sc2 = Float.valueOf(splited[13]);
	// // float cda = Float.valueOf(splited[14]);
	// // float dc = Float.valueOf(splited[15]);
	// // float gcpc = Float.valueOf(job_data.getGross_cpc());
	// // // Skip loop
	// // if (gcpc == 0 || gcpc == 0.0) {
	// // continue;
	// // }
	// // float cpc = gcpc - (gcpc * sc1 / 100) - (gcpc * sc2 / 100) - (gcpc *
	// cda / 100) - (gcpc * dc / 100);
	// // job_data.setCpc(String.format("%.2f", cpc));
	//
	// } catch (Exception e) {
	// }
	//
	// try {
	// job_data.setTitle(decodeString(jobs_array.getJSONObject(temp).getString("jobtitle")));
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// try {
	// job_data.setPostingdate(formatDate(jobs_array.getJSONObject(temp).getString("date")));
	// // change
	// // the
	// // format
	// } catch (JSONException e) {
	//
	// e.printStackTrace();
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	// try {
	// job_data.setZipcode(jobs_array.getJSONObject(temp).getString("zip"));
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setCity(jobs_array.getJSONObject(temp).getString("city").replace("$",
	// "").replace("!", "").trim());
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setState(jobs_array.getJSONObject(temp).getString("state").replace("$",
	// "").replace("!", "").trim());
	// } catch (Exception e) {
	//
	// }
	//
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// Lead5MediaArrayList.add(job_data);
	// }
	//
	// }
	// }
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// }
	//
	// return Lead5MediaArrayList;
	// }
	//
	// public static ArrayList<Jobs> simplyJobsApiParsing(String result, String
	// keyword, String location, String exactOrSimple) {
	//
	// ArrayList<Jobs> SimplyHiredList = new ArrayList<Jobs>();
	//
	// JSONObject jObj = null;
	// List<SimplyHiredApiParsingModel> simplyHiredList2 = null;
	// try {
	// jObj = new JSONObject(result);
	// SimplyHiredApiParsingModel[] arr = new
	// Gson().fromJson(jObj.getString("jobs"),
	// SimplyHiredApiParsingModel[].class);
	// simplyHiredList2 = Arrays.asList(arr);
	// } catch (Exception e1) {
	// //
	// // e1.printStackTrace();
	// }
	//
	// try {
	// if (simplyHiredList2 != null && simplyHiredList2.size() > 0) {
	// for (SimplyHiredApiParsingModel model : simplyHiredList2) {
	// Jobs job_data = new Jobs();
	//
	// try {
	// job_data.setCity(model.getCity().replace("$", "").replace("!", ""));
	// job_data.setEmployer(decodeString(model.getCompany()));
	// job_data.setJoburl(model.getUrl()); // check
	// job_data.setSourcename(MainWhiteLabelClass.SimplyJobsApi);
	// job_data.setDisplaysource("SimplyJobs.");
	// job_data.setSourceNameApiForInternalCheck(MainWhiteLabelClass.SimplyJobsApi);
	//
	// try {
	// String[] splited =
	// MainWhiteLabelClass.ApiCPCHashMap.get(job_data.getSourceNameApiForInternalCheck().toLowerCase()).split("\\|");
	// job_data.setGross_cpc(splited[0]);
	// job_data.setCpc(splited[1]);
	// } catch (Exception e) {
	// }
	//
	// job_data.setState(model.getState().replace("$", "").replace("!", ""));
	// job_data.setTitle(decodeString(model.getTitle()));
	// job_data.setPostingdate(formatDate(model.getDate()));
	// } catch (Exception e) {
	// //
	// e.printStackTrace();
	// }
	// // job_data.setPriority(priority);
	// /*
	// * try { job_data.setZipcode(location); } catch (Exception
	// * e) { // e.printStackTrace(); }
	// */
	// try {
	// job_data.setExactOrSynonym(exactOrSimple);
	//
	// } catch (Exception e) {
	//
	// }
	// try {
	// job_data.setApiOrCS(SettingsClass.API);
	// } catch (Exception e) {
	//
	// }
	// if (checkValidJob(job_data)) {
	// SimplyHiredList.add(job_data);
	// }
	//
	// }
	//
	// return SimplyHiredList;
	//
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//
	// }
	//
	// return SimplyHiredList;
	// }
	//
	// private static String formatDate(String date) {
	// String value = "";
	// if (date != null && !date.equalsIgnoreCase("null") && date != "") {
	//
	// if (date.contains("T")) {
	// String date_new[] = date.split("T");
	// date = date_new[0];
	// }
	// Date date1 = null; // 2015-05-29T00:00:00Z
	//
	// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	// try {
	// date1 = sdf.parse(date);
	// } catch (ParseException e) {
	// // new format come from api 05/03/15 so use this format
	// SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yy");
	// try {
	// date1 = sdf1.parse(date);
	// } catch (ParseException e1) {
	//
	// e1.printStackTrace();
	// }
	//
	// }
	// // SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	// value = sdf.format(date1);
	// }
	// return value;
	//
	// // return date1.toString();
	// }

	// ============================================new
	// code==============================================

	// =======================================for the job provider sourcecount
	// new ===========================

	// to write the job provider source count wise stats in the memcache
	public static String convert_JobDomainwiseProviderCountStats_ToString() {

		String result = "";

		for (String domain : MainWhiteLabelClass.domains) {

			for (String feed : SettingsClass.feedApiListForProviderWiseSourceCountStats) {

				String key = domain + "_" + feed.trim();

				// if(MainOakJobSearchEnigne.jobProvidersJobCountMap.containsKey(feed))

				if (SettingsClass.new_jobProviderCountDomainWise.get(key) != null) {

					if (result.equalsIgnoreCase("")) {

						result = SettingsClass.new_jobProviderCountDomainWise.get(key) + "";

					} else {

						result = result + key + ":" + SettingsClass.new_jobProviderCountDomainWise.get(key) + ",";
					}

				} else {
					result = result + key + ":" + 0 + ",";

				}

			}
		}

		return result;
	}

	public static void convert_JobDomainwiseProviderCount_ToHashMap(String jobsCount) {

		// initializing hash map from the memcache data

		String jobscount_arr[] = jobsCount.split(",");

		for (int i = 0; i < jobscount_arr.length; i++) {

			String[] jobsSourcePerFeed = jobscount_arr[i].split(":");

			//

			SettingsClass.new_jobProviderCountDomainWise.put(jobsSourcePerFeed[0], Integer.parseInt(jobsSourcePerFeed[1]));
		}

	}

	// for writing in the file we use this method
	public static String convert_jobproviderHashMapStats_toJason() {

		String gson = "";
		String date = "";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			date = formatter.format(new Date());
		} catch (Exception e) {
		}
		List<JobsDomain> finalJobsDomainList = new ArrayList<JobsDomain>();

		for (String domain : MainWhiteLabelClass.domains) {

			JobsDomain jobsDomainModel = new JobsDomain();
			jobsDomainModel.setName(domain);

			List<JobSourceCountModel> jobProviderJobCountDataList = new ArrayList<JobSourceCountModel>();

			for (String sourceName : SettingsClass.feedApiListForProviderWiseSourceCountStats) {

				JobSourceCountModel sourceCountModel = new JobSourceCountModel();

				String key = domain + "_" + sourceName;

				sourceCountModel.setSourceName(sourceName);
				sourceCountModel.setCount(SettingsClass.new_jobProviderCountDomainWise.get(key) + "");

				jobProviderJobCountDataList.add(sourceCountModel);

			}

			jobsDomainModel.setName(domain);
			jobsDomainModel.setJobSourceCountModelList(jobProviderJobCountDataList);
			jobsDomainModel.setDate(date);

			finalJobsDomainList.add(jobsDomainModel);

		}

		gson = new Gson().toJson(finalJobsDomainList);

		return gson;

	}

	// TODO FROM FILE
	public static void convert_JobproviderCountHashMapstatsToMap_Fromfile(String bufferedReader) {
		List<JobsDomain> JobsDomainList = new ArrayList<JobsDomain>();
		JobsDomainList = new Gson().fromJson(bufferedReader, new TypeToken<List<JobsDomain>>() {
		}.getType());

		for (JobsDomain jobDomain : JobsDomainList) {

			for (JobSourceCountModel sourceCount : jobDomain.getJobSourceCountModelList()) {

				try {

					String sourceCountValue = sourceCount.getCount();

					if (sourceCountValue == null || sourceCountValue.contains("null")) {
						sourceCount.setCount("0");
					}

					SettingsClass.new_jobProviderCountDomainWise.put(jobDomain.getName() + "_" + sourceCount.getSourceName(), Integer.parseInt(sourceCount.getCount()));

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}

	public static String getInstanceId() {
		AmazonEC2 ec2;
		AWSCredentials ec2AwsCredentials;
		ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2AwsAccessKey, SettingsClass.ec2AwsSecretKey);
		ec2 = new AmazonEC2Client(ec2AwsCredentials);

		String instanceId = EC2MetadataUtils.getInstanceId();
		return instanceId;

	}

	// =============================================end of new code goes here
	// ==============================================new methods implemented for
	// the group and job provider source count=====================
	// TODO new code used to put the stats of domain wise category stats to
	// memcache
	public static String convert_DomainCategoryGroupWiseStatsFromMap_ToString() {

		String result = "";
		String key = "";
		Integer value = 0;

		try {
			for (String provider : MainWhiteLabelClass.providersSet) {
				for (String domainKey : MainWhiteLabelClass.categoryDomainWiseGroupTemplateStatsKeys) {

					for (int i = 0; i < SettingsClass.groupObjectList.size(); i++) {

						for (int j = 0; j < SettingsClass.templateObjectList.size(); j++) {

							key = provider + "_" + domainKey + "_" + SettingsClass.groupObjectList.get(i).getGroupId() + "_" + SettingsClass.templateObjectList.get(j).getTemplateId();

							try {

								if (SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key) == null) {
									value = 0;
								} else {
									value = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);
								}

								result += key + ":" + value + ",";

							} catch (Exception e) {

								e.printStackTrace();
							}
						}
					}
				}
			}

			return result.substring(0, result.length() - 1);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return "";

	}

	// TODO new code initilizimng hash map from the string get from the memcache
	public static void convert_categoryDomainWiseGroupTemplateStats_ToMap(String group_template_count_string) {

		try {
			String group_template_count[] = group_template_count_string.split(",");

			for (int i = 0; i < group_template_count.length; i++) {
				try {
					SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(group_template_count[i].split(":")[0], Integer.parseInt(group_template_count[i].split(":")[1]));
				} catch (NumberFormatException e) {

					// e.printStackTrace();
				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	// TODO this method is used for the stats writing in the file as json
	public static String convert_DomainWiseSubjectLineStats_toJson() {
		String gson = "";
		List<Providers> providerList = new ArrayList<Providers>();

		// iterating hash map to make the json model which will write in the
		// file
		try {

			for (String provider : MainWhiteLabelClass.providersSet) {

				Providers providers = new Providers();
				ArrayList<Domains> finalDomainWiseList = new ArrayList<Domains>();

				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

				for (String domain : MainWhiteLabelClass.domains) {

					Domains domainModel = new Domains();
					domainModel.setDomainName(domain);

					ArrayList<Category> finalCategoryWiseList = new ArrayList<Category>();

					for (String category : MainWhiteLabelClass.categoriesList) {

						Category categoryModel = new Category();

						categoryModel.setCateGoryName(category);

						ArrayList<FinalGroupObject> finalGroupList = new ArrayList<FinalGroupObject>();

						for (int group_id = 0; group_id < SettingsClass.groupObjectList.size(); group_id++) {

							GroupObject currentGroupObject = SettingsClass.groupObjectList.get(group_id);

							FinalGroupObject finalGroupObject = new FinalGroupObject();

							int groupCount = 0;

							for (int temp_id = 0; temp_id < SettingsClass.templateObjectList.size(); temp_id++) {

								int currentTemplateId = SettingsClass.templateObjectList.get(temp_id).getTemplateId();

								String key = currentGroupObject.getGroupId() + "_" + currentTemplateId;

								key = provider + "_" + domain.trim() + "_" + category.replace(" ", "_").trim() + "_" + key;

								Integer tempValueObject = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);

								if (tempValueObject == null) {
									tempValueObject = 0;
								}

								// System.out.println("key value="+key+" "+tempValueObject);

								TemplateObject template = new TemplateObject();
								template.setTemplateId(currentTemplateId);
								template.setTemplateCount(tempValueObject);

								finalGroupObject.template.add(template);

								groupCount = groupCount + tempValueObject;

							}

							finalGroupObject.setGroup_id(currentGroupObject.getGroupId());
							finalGroupObject.setFrom_name(currentGroupObject.getFromName());
							finalGroupObject.setSubject(currentGroupObject.getSubject());
							finalGroupObject.setGroup_count(groupCount);
							finalGroupObject.setProcess_name(SettingsClass.dashboradFileName);
							finalGroupObject.setDate(df1.format(new Date()));

							finalGroupList.add(finalGroupObject);
						}

						categoryModel.setFinalTemplateGroupList(finalGroupList);
						finalCategoryWiseList.add(categoryModel);
					}

					domainModel.setCategories(finalCategoryWiseList);
					finalDomainWiseList.add(domainModel);
				}

				providers.setDomains(finalDomainWiseList);
				providers.setProviderName(provider);
				providerList.add(providers);

				gson = new Gson().toJson(providerList);

				// System.out.println(gson);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		return gson;

	}

	// TODO used to initialize the hashmap from file data
	public static void convert_CategoryWiseGroupTemplateStats_ToMap_FromFile(String bufferedReader) {
		// parsing the data from file to array model list

		List<Providers> providerList = new ArrayList<Providers>();
		providerList = new Gson().fromJson(bufferedReader, new TypeToken<List<Providers>>() {
		}.getType());

		// TODO added provider loop
		for (Providers provider : providerList) {

			List<Domains> finalDomainList = provider.getDomains();

			try {
				for (Domains domainModel : finalDomainList) {
					for (Category cateGoryModel : domainModel.getCategories()) {

						List<FinalGroupObject> finalGroupList = cateGoryModel.getFinalTemplateGroupList();

						for (int i = 0; i < finalGroupList.size(); i++) {
							for (int temp_id = 0; temp_id < finalGroupList.get(i).template.size(); temp_id++) {

								String key = provider.getProviderName().trim() + "_" + domainModel.getDomainName().trim() + "_" + cateGoryModel.getCateGoryName().replace(" ", "_").trim() + "_"
										+ finalGroupList.get(i).getGroup_id() + "_" + finalGroupList.get(i).template.get(temp_id).getTemplateId();

								SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, finalGroupList.get(i).template.get(temp_id).getTemplateCount());
							}
						}
					}
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}

	// =====================================//===============================================

	// ================================================//new code for subject
	// line to read all the subject
	// files==================================================
	public static String templateGsonResult_New_Method(String fileName, String child_key) {

		String gson = "";

		try {

			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
			ArrayList<FinalGroupObject> finalGroupList = new ArrayList<FinalGroupObject>();

			ArrayList<GroupObject> groupList = SettingsClass.child_Grouplist_map.get(child_key);

			for (int group_id = 0; group_id < groupList.size(); group_id++) {

				FinalGroupObject finalGroupObject = new FinalGroupObject();

				int groupCount = 0;

				for (int temp_id = 0; temp_id < SettingsClass.templateObjectList.size(); temp_id++) {

					// for temp use

					String key = fileName + "_" + groupList.get(group_id).getGroupId() + "_" + SettingsClass.templateObjectList.get(temp_id).getTemplateId();

					Integer tempValueObject = SettingsClass.templateCountHashMap.get(key);

					if (tempValueObject == null) {
						tempValueObject = 0;
					}

					TemplateObject template = new TemplateObject();
					template.setTemplateId(SettingsClass.templateObjectList.get(temp_id).getTemplateId());
					template.setTemplateCount(tempValueObject);

					finalGroupObject.template.add(template);

					groupCount = groupCount + tempValueObject;

				}

				finalGroupObject.setGroup_id(groupList.get(group_id).getGroupId());
				finalGroupObject.setFrom_name(groupList.get(group_id).getFromName());
				finalGroupObject.setSubject(groupList.get(group_id).getSubject());
				finalGroupObject.setGroup_count(groupCount);
				finalGroupObject.setProcess_name(SettingsClass.dashboradFileName);
				finalGroupObject.setDate(df1.format(new Date()));

				finalGroupList.add(finalGroupObject);

			}

			gson = new Gson().toJson(finalGroupList);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return gson;

	}

	public static void templateGsonToMap_New_Method(String bufferedReader, String fileName) {

		List<FinalGroupObject> finalGroupObjList = new ArrayList<FinalGroupObject>();
		finalGroupObjList = new Gson().fromJson(bufferedReader, new TypeToken<List<FinalGroupObject>>() {
		}.getType());

		try {

			for (int i = 0; i < finalGroupObjList.size(); i++) {

				for (int temp_id = 0; temp_id < finalGroupObjList.get(i).template.size(); temp_id++) {

					String key = fileName + "_" + finalGroupObjList.get(i).getGroup_id() + "_" + finalGroupObjList.get(i).template.get(temp_id).getTemplateId();

					SettingsClass.templateCountHashMap.put(key, finalGroupObjList.get(i).template.get(temp_id).getTemplateCount());

				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	// =========================================new code for subject line
	// change==========================
	public static void child_groupTemplateIds_toMap(String group_template_count_string) {
		try {
			String group_template_count[] = group_template_count_string.split(",");
			for (int i = 0; i < group_template_count.length; i++) {
				try {
					// initialize template hash map
					SettingsClass.templateCountHashMap.put(group_template_count[i].split(":")[0], Integer.parseInt(group_template_count[i].split(":")[1]));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String child_groupTemplateIds_toString(String fileName, String child_key) {
		String result = "";
		String key = "";
		Integer value = 0;
		ArrayList<GroupObject> groupObjectList = SettingsClass.child_Grouplist_map.get(child_key);
		try {
			for (int i = 0; i < groupObjectList.size(); i++) {
				for (int j = 0; j < SettingsClass.templateObjectList.size(); j++) {
					key = fileName + "_" + groupObjectList.get(i).getGroupId() + "_" + SettingsClass.templateObjectList.get(j).getTemplateId();
					try {
						if (SettingsClass.templateCountHashMap.get(key) == null) {
							value = 0;
						} else {
							value = SettingsClass.templateCountHashMap.get(key);
						}
						result += key + ":" + value + ",";
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return result.substring(0, result.length() - 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";

	}
	// ========================================================//==================================
}
