package whitelabels.model;

public class AmazonResponse {

   
    Hits hits;

   
    public Hits getHits() {
        return hits;
    }

    public void setHits(Hits hits) {
        this.hits = hits;
    }

    public AmazonResponse(Hits hits) {
        super();
        this.hits = hits;
    }

    public AmazonResponse() {
        super();
    }

}
