package whitelabels.model;

import java.util.ArrayList;

public class FinalGroupObject {

	public String group_id = "";
	public String from_name = "";
	public String subject = "";
	public int group_count = 0;
	public String process_name = "";
	public String date = "";

	/*
	 * public FinalGroupObject(String group_id, String from_name, String
	 * subject, int group_count, ArrayList<TemplateObject> template) { super();
	 * this.group_id = group_id; this.from_name = from_name; this.subject =
	 * subject; this.group_count = group_count; this.template = template; }
	 */

	public FinalGroupObject() {
		super();
	}

	public ArrayList<TemplateObject> template = new ArrayList<TemplateObject>();

	public String getProcess_name() {
		return process_name;
	}

	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getFrom_name() {
		return from_name;
	}

	public void setFrom_name(String from_name) {
		this.from_name = from_name;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getGroup_count() {
		return group_count;
	}

	public void setGroup_count(int group_count) {
		this.group_count = group_count;
	}

	public ArrayList<TemplateObject> getTemplate() {
		return template;
	}

	public void setTemplate(ArrayList<TemplateObject> template) {
		this.template = template;
	}

}
