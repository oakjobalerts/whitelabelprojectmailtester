package whitelabels.model;

public class GroupObject {

	private String subject;
	private String fromName;
	private String groupId;
	private String active;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public GroupObject(String group_id, String subjectName, String fromName) {
		this.groupId = group_id;
		this.fromName = fromName;
		this.subject = subjectName;
	
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}



}
