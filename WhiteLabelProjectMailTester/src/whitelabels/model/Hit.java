package whitelabels.model;



public class Hit {

    public String id="";
    public Jobs fields;

    public Hit(String id, Jobs fields) {
        super();
        this.id = id;
        this.fields = fields;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Jobs getFields() {
        return fields;
    }

    public void setFields(Jobs fields) {
        this.fields = fields;
    }

    public Hit() {
        super();
    }

}
