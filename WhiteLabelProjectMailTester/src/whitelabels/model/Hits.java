package whitelabels.model;

import java.util.ArrayList;
import java.util.List;

public class Hits {

	String found;
	String start;
	List<Hit> hit = new ArrayList<Hit>();

	public String getFound() {
		return found;
	}

	public void setFound(String found) {
		this.found = found;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public List<Hit> getHit() {
		return hit;
	}

	public void setHit(List<Hit> hit) {
		this.hit = hit;
	}

	public Hits(String found, String start, List<Hit> hit) {
		super();
		this.found = found;
		this.start = start;
		this.hit = hit;
	}

	public Hits() {
		super();
	}

}
