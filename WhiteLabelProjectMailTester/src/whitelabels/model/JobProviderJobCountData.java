package whitelabels.model;


import java.util.Map;

public class JobProviderJobCountData {

	


	public String processName = "";
	public String date = "";
	public Map<String,Integer>sources;


	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public Map<String, Integer> getSources() {
		return sources;
	}

	public void setSources(Map<String, Integer> sources) {
		this.sources = sources;
	}

	
}
