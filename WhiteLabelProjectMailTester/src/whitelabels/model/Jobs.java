package whitelabels.model;

public class Jobs implements Comparable<Jobs> {

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	String id = "";
	String title = "";
	String joburl = "";
	String employer = "";
	String city = "";
	String zipcode = "";
	String source = "";
	String sourcename = "";
	String displaysource = "";
	int priority = 100;
	String state = "";
	String postingdate = "";
	String userJobUrl = "";

	String exactOrSynonym = "";
	String ApiOrCS = "c";

	String cpc = "0.0";
	String gross_cpc = "0.0";

	String internal_cpc = "0.0";

	public String getInternal_cpc() {
		return internal_cpc;
	}

	public void setInternal_cpc(String internal_cpc) {
		this.internal_cpc = internal_cpc;
	}

	String apiSourceName = "";
	String sourceNameApiForInternalCheck = "defaultfortesing";

	public String getApiSourceName() {
		return apiSourceName;
	}

	public void setApiSourceName(String apiSourceName) {
		this.apiSourceName = apiSourceName;
	}

	public String getExactOrSynonym() {
		return exactOrSynonym;
	}

	public void setExactOrSynonym(String exactOrSynonym) {
		this.exactOrSynonym = exactOrSynonym;
	}

	public String getApiOrCS() {
		return ApiOrCS;
	}

	public void setApiOrCS(String apiOrCS) {
		ApiOrCS = apiOrCS;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getJoburl() {
		return joburl;
	}

	public void setJoburl(String joburl) {
		this.joburl = joburl;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getUserJobUrl() {
		return userJobUrl;
	}

	public void setUserJobUrl(String userJobUrl) {
		this.userJobUrl = userJobUrl;
	}

	public String getSourcename() {
		return sourcename;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public String getDisplaysource() {
		return displaysource;
	}

	public void setDisplaysource(String displaysource) {
		this.displaysource = displaysource;
	}

	public String getPostingdate() {
		return postingdate;
	}

	public void setPostingdate(String postingdate) {
		this.postingdate = postingdate;
	}

	public String getSourceNameApiForInternalCheck() {
		return sourceNameApiForInternalCheck;
	}

	public void setSourceNameApiForInternalCheck(String sourceNameApiForInternalCheck) {
		this.sourceNameApiForInternalCheck = sourceNameApiForInternalCheck;
	}

	public String getGross_cpc() {
		return gross_cpc;
	}

	public void setGross_cpc(String gross_cpc) {
		this.gross_cpc = gross_cpc;
	}

	public String getCpc() {
		return cpc;
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

	public int compareTo(Jobs o) {

		try {

			float feedOrder = Float.valueOf(this.getInternal_cpc());
			float oFeed = Float.valueOf(o.getInternal_cpc());
			if (feedOrder > oFeed) {
				return -1;
			} else if (feedOrder < oFeed) {
				return 1;
			} else
				return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	// @Override
	// public int compare(Object arg0, Object arg1) {
	// Jobs j1 = (Jobs) arg0;
	// Jobs j2 = (Jobs) arg1;
	// if (j1.getPriority() < j2.getPriority())
	// return -1;
	// else if (j1.getPriority() == j2.getPriority())
	// return 0;
	// else
	// return 1;
	// }

}
