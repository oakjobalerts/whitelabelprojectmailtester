package whitelabels.model;

public class ParameterModelClass {

	
	String RedirectionFileName = "";
	String whitelabel_name = "";
	boolean PowerInbox = false;
	boolean TopResume = false;
	String SendgridUsername = "";
	String JobTitleColorCode = "";
	String SubjectsFileName = "";
	String MemcacheKeyName = "";
	String LogoUrl = "";
	String QueueName = "";
	String DashboardName = "";
	String FromEmailAddress = "";
	String HostDomainName = "";
	String LearnMoreColorCode = "";
	String FeedOrderFileName = "";
	String BccMailPercentage = "";
	String PostalAddress = "";
	String SendGridCategoryName = "";
	String DomainUrl = "";
	boolean DisplaySource = false;
	String MailgunDomainName = "";
	String ThreadCount = "";
	String SendgridPassword = "";
	boolean isFlashAlertsActive = false;
	boolean isOakMobileAddActive = false;
	boolean isCylconSubProvider = false;
	String arbor_pixel = "";
	String image_postaladdress = "";

//	String EmailClient = "";
	// String LiveRampPixels = "";
	// String HtmlSourceName = "";
	// String ProviderName = "";
	// String LeadMediaChannel = "";
	// String JujuChannel = "";
	// String OpenPixelCategoryName = "";
	// String CampgianId = "";
	// String T2ValueJ2c = "";

	public String getImage_postaladdress() {
		return image_postaladdress;
	}

	public void setImage_postaladdress(String image_postaladdress) {
		this.image_postaladdress = image_postaladdress;
	}

	public String getWhitelabel_name () {
		return whitelabel_name;
	}

	public void setWhitelabel_name (String whitelabel_name) {
		this.whitelabel_name = whitelabel_name;
	}

	public String getSendGridCategoryName () {
		return SendGridCategoryName;
	}

	public void setSendGridCategoryName (String sendGridCategoryName) {
		SendGridCategoryName = sendGridCategoryName;
	}

	public String getArbor_pixel () {
		return arbor_pixel;
	}

	public void setArbor_pixel (String arbor_pixel) {
		this.arbor_pixel = arbor_pixel;
	}

	
	public boolean isCylconSubProvider () {
		return isCylconSubProvider;
	}

	public void setCylconSubProvider (boolean isCylconSubProvider) {
		this.isCylconSubProvider = isCylconSubProvider;
	}

	public boolean isOakMobileAddActive () {
		return isOakMobileAddActive;
	}

	public void setOakMobileAddActive (boolean isOakMobileAddActive) {
		this.isOakMobileAddActive = isOakMobileAddActive;
	}

	public boolean isFlashAlertsActive () {
		return isFlashAlertsActive;
	}

	public void setFlashAlertsActive (boolean isFlashAlertsActive) {
		this.isFlashAlertsActive = isFlashAlertsActive;
	}

	boolean isForEveningAlerts = false;

	public boolean isForEveningAlerts () {
		return isForEveningAlerts;
	}

	public void setForEveningAlerts (boolean isForEveningAlerts) {
		this.isForEveningAlerts = isForEveningAlerts;
	}

	public String getSendgridPassword () {
		return SendgridPassword;
	}

	public void setSendgridPassword (String sendgridPassword) {
		SendgridPassword = sendgridPassword;
	}

	public String getRedirectionFileName () {
		return RedirectionFileName;
	}

	public void setRedirectionFileName (String redirectionFileName) {
		RedirectionFileName = redirectionFileName;
	}

	public boolean isPowerInbox () {
		return PowerInbox;
	}

	public void setPowerInbox (boolean powerInbox) {
		PowerInbox = powerInbox;
	}

	public boolean isTopResume () {
		return TopResume;
	}

	public void setTopResume (boolean topResume) {
		TopResume = topResume;
	}

	//
	// public String getHtmlSourceName() {
	// return HtmlSourceName;
	// }
	//
	// public void setHtmlSourceName(String htmlSourceName) {
	// HtmlSourceName = htmlSourceName;
	// }
	//
	public String getSendgridUsername () {
		return SendgridUsername;
	}

	public void setSendgridUsername (String sendgridUsername) {
		SendgridUsername = sendgridUsername;
	}

	//
	// public String getProviderName() {
	// return ProviderName;
	// }
	//
	// public void setProviderName(String providerName) {
	// ProviderName = providerName;
	// }
	//
	// public String getLeadMediaChannel() {
	// return LeadMediaChannel;
	// }
	//
	// public void setLeadMediaChannel(String leadMediaChannel) {
	// LeadMediaChannel = leadMediaChannel;
	// }

//	public String getEmailClient () {
//		return EmailClient;
//	}
//
//	public void setEmailClient (String emailClient) {
//		EmailClient = emailClient;
//	}

	public String getJobTitleColorCode () {
		return JobTitleColorCode;
	}

	public void setJobTitleColorCode (String jobTitleColorCode) {
		JobTitleColorCode = jobTitleColorCode;
	}

	public String getSubjectsFileName () {
		return SubjectsFileName;
	}

	public void setSubjectsFileName (String subjectsFileName) {
		SubjectsFileName = subjectsFileName;
	}

	public String getMemcacheKeyName () {
		return MemcacheKeyName;
	}

	public void setMemcacheKeyName (String memcacheKeyName) {
		MemcacheKeyName = memcacheKeyName;
	}

	// public String getJujuChannel() {
	// return JujuChannel;
	// }
	//
	// public void setJujuChannel(String jujuChannel) {
	// JujuChannel = jujuChannel;
	// }
	//
	// public String getOpenPixelCategoryName() {
	// return OpenPixelCategoryName;
	// }
	//
	// public void setOpenPixelCategoryName(String openPixelCategoryName) {
	// OpenPixelCategoryName = openPixelCategoryName;
	// }
	//
	// public String getCampgianId() {
	// return CampgianId;
	// }
	//
	// public void setCampgianId(String campgianId) {
	// CampgianId = campgianId;
	// }

	public String getLogoUrl () {
		return LogoUrl;
	}

	public void setLogoUrl (String logoUrl) {
		LogoUrl = logoUrl;
	}

	public String getQueueName () {
		return QueueName;
	}

	public void setQueueName (String queueName) {
		QueueName = queueName;
	}

	// public String getT2ValueJ2c() {
	// return T2ValueJ2c;
	// }
	//
	// public void setT2ValueJ2c(String t2ValueJ2c) {
	// T2ValueJ2c = t2ValueJ2c;
	// }

	public String getDashboardName () {
		return DashboardName;
	}

	public void setDashboardName (String dashboardName) {
		DashboardName = dashboardName;
	}

	public String getFromEmailAddress () {
		return FromEmailAddress;
	}

	public void setFromEmailAddress (String fromEmailAddress) {
		FromEmailAddress = fromEmailAddress;
	}

	public String getHostDomainName () {
		return HostDomainName;
	}

	public void setHostDomainName (String hostDomainName) {
		HostDomainName = hostDomainName;
	}

	public String getLearnMoreColorCode () {
		return LearnMoreColorCode;
	}

	public void setLearnMoreColorCode (String learnMoreColorCode) {
		LearnMoreColorCode = learnMoreColorCode;
	}

	public String getFeedOrderFileName () {
		return FeedOrderFileName;
	}

	public void setFeedOrderFileName (String feedOrderFileName) {
		FeedOrderFileName = feedOrderFileName;
	}

	public String getBccMailPercentage () {
		return BccMailPercentage;
	}

	public void setBccMailPercentage (String bccMailPercentage) {
		BccMailPercentage = bccMailPercentage;
	}

	public String getPostalAddress () {
		return PostalAddress;
	}

	public void setPostalAddress (String postalAddress) {
		PostalAddress = postalAddress;
	}

	// public String getSendGridCategoryName() {
	// return SendGridCategoryName;
	// }
	//
	// public void setSendGridCategoryName(String sendGridCategoryName) {
	// SendGridCategoryName = sendGridCategoryName;
	// }

	public String getDomainUrl () {
		return DomainUrl;
	}

	public void setDomainUrl (String domainUrl) {
		DomainUrl = domainUrl;
	}

	public boolean isDisplaySource () {
		return DisplaySource;
	}

	public void setDisplaySource (boolean displaySource) {
		DisplaySource = displaySource;
	}

	public String getMailgunDomainName () {
		return MailgunDomainName;
	}

	public void setMailgunDomainName (String mailgunDomainName) {
		MailgunDomainName = mailgunDomainName;
	}

	// public String getLiveRampPixels() {
	// return LiveRampPixels;
	// }
	//
	// public void setLiveRampPixels(String liveRampPixels) {
	// LiveRampPixels = liveRampPixels;
	// }

	public String getThreadCount () {
		return ThreadCount;
	}

	public void setThreadCount (String threadCount) {
		ThreadCount = threadCount;
	}

}
