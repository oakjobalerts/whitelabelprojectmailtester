package whitelabels.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ReadFeedOrderModel {

	public String rotateCpcScore = "1";
	public List<String> alwaysOnTopsourceCloudSearchQueryList = new ArrayList<String>();
	public List<String> sourceCloudSearchQueryList = new ArrayList<String>();
	public List<String> sourceCloudSearchQueryListForCollegeRec = new ArrayList<String>();
	public String alwaysOnTopFeedsString = "";
	// public ArrayList<String> feedOrderList = new ArrayList<String>();
	public HashMap<String, String> ApiCPCHashMap = new HashMap<String, String>();
	public String searchEndpoint = "";
	public HashMap<String, Integer> maxJobsHashmap = new LinkedHashMap<String, Integer>();
	public String zipRecuiterStartTimeHours[] = null;
	public LinkedHashMap<String, ApiStatusModel> apiStatusHashmap = new LinkedHashMap<String, ApiStatusModel>();
	public LinkedHashMap<String, ScoreBaseFeedModel> scoreBaseFeedModelList = new LinkedHashMap<String, ScoreBaseFeedModel>();
}
